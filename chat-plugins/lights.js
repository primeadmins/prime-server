/**
 * Custom code added or created by Lights
 * Please do not edit this file, but feel free to create your own
 *
 * These commands will follow no particular theme
 * and the code is likely going to be a mess because i have no idea what im doing
 *
 * If you're reading this and you aren't a Prime Server developer, I hope you get scurvy for obtaining this file.
 * Fuck you.
 */

'use strict';

exports.commands = {

        deroomauthall: function (target, room, user) {
                if (!this.can('root', null, room)) return false;
                if (!room.auth) return this.errorReply("Room does not have any roomauth to delete.");
                if (!target) {
                    user.lastCommand = '/deroomauthall';
                    this.errorReply("ALL ROOMAUTH WILL BE GONE, ARE YOU SURE?");
                    this.errorReply("To confirm, use: /deroomauthall confirm");
                    return;
                }
                if (user.lastCommand !== '/deroomauthall' || target !== 'confirm') {
                    return this.parse('/help deroomauthall');
                }
                let count = 0;
                for (let userid in room.auth) {
                    if (room.auth[userid] === '+', '$', '%', '@', '&', '#') {
                        delete room.auth[userid];
                        if (userid in room.users) room.users[userid].updateIdentity(room.id);
                        count++;
                    }
                }
                if (!count) {
                    return this.sendReply("(This room has no roomauth)");
                }
                if (room.chatRoomData) {
                    Rooms.global.writeChatRoomData();
                }
                this.addModCommand("All " + count + " roomauth has been cleared by " + user.name + ".");
            },
    deroomauthallhelp: ["/deroomauthall - Deauths all roomauthed users. Requires: ~"],
report: 'reportuser',
    reportuser: function (target, room, user, connection) {
        if (!target) return this.parse('/help reportuser');
        if (!this.canTalk()) return this.errorReply("You cannot do this while unable to talk. If you're trying to report staff, use /reportstaff!");
 
        if (target.length > MAX_REASON_LENGTH) {
            return this.errorReply("The report is too long. It cannot exceed " + MAX_REASON_LENGTH + " characters.");
        }
        if (!this.can('minigame', null, room)) return false;
        var targetRoom = Rooms.get('staff');
        targetRoom.add('[User Report Monitor] ' + (room ? '(' + room + ') ' : '') + Chat.escapeHTML(user.name) + ' has reported that "' + target + '"').update();
        return this.sendReply("Your report has been received. Thank you for your time!")
    },
    reportuserhelp: ["/reportuser [note + pastebin] - Reports something to Staff. [False reports are ZERO tolerance] Viewed by: % @ & ~"],
        userid: function(target, room, user) {
                if (!target) return this.sendReply('You must specify a user');
                target = this.splitTarget(target);
                var targetUser = this.targetUser;
                this.sendReply('The userid of this.targetUser is ' + targetUser);
        },
        alert: function(target, room, user) {
                if (user.can('rangeban')) {
                        target = this.splitTarget(target);
                        var targetUser = this.targetUser;
                        if (!targetUser) return this.sendReply('You need to specify a user.');
                        if (!target) return this.sendReply('You need to specify a message to be sent.');
                        targetUser.popup(target);
                        this.sendReply(targetUser.name + ' successfully recieved the message: ' + target);
                        targetUser.send(user.name + ' sent you a popup alert.');
                        this.addModCommand(user.name + ' sent a popup alert to ' + targetUser.name);
                }
        },
        forcelogout: function(target, room, user) {
                if (user.can('hotpatch')) {
                        if (!target) return this.sendReply('You must specify a target.');
                        target = this.splitTarget(target);
                        var targetUser = this.targetUser;
                        if (targetUser.can('hotpatch')) return this.sendReply('You cannot logout an Administrator.');
                        this.addModCommand(targetUser + ' was forcibly logged out by ' + user.name + '.');
                        targetUser.resetName();
                }
        },
        roomlist: function (target, room, user) {
		if(!this.can('lock')) return;
		let totalUsers = 0;
		for (let u of Users.users) {
			u = u[1];
			if (Users(u).connected) {
				totalUsers++;
			}
		}
		let rooms = Object.keys(Rooms.rooms),
			len = rooms.length,
			header = ['<b><font color="#777777" size="2">Total users connected: ' + totalUsers + '</font></b><br />'],
			official = ['<b><font color="#777777" size="2"><u>Official chat rooms:</u></font></b><br />'],
			nonOfficial = ['<hr><b><u><font color="#777777" size="2">Public chat rooms:</font></u></b><br />'],
			privateRoom = ['<hr><b><u><font color="#777777" size="2">Private chat rooms:</font></u></b><br />'],
			groupChats = ['<hr><b><u><font color="#777777" size="2">Group Chats:</font></u></b><br />'],
			battleRooms = ['<hr><b><u><font color="#777777" size="2">Battle Rooms:</font></u></b><br />'];
		while (len--) {
			let _room = Rooms.rooms[rooms[(rooms.length - len) - 1]];
			if (_room.type === 'battle') {
				battleRooms.push('<a href="/' + _room.id + '" class="ilink">' + _room.title + '</a> (' + _room.userCount + ')');
			}
			if (_room.type === 'chat') {
				if (_room.isPersonal) {
					groupChats.push('<a href="/' + _room.id + '" class="ilink">' + _room.id + '</a> (' + _room.userCount + ')');
					continue;
				}
				if (_room.isOfficial) {
					official.push('<a href="/' + toId(_room.title) + '" class="ilink">' + _room.title + '</a> (' + _room.userCount + ')');
					continue;
				}
				if (_room.isPrivate) {
					privateRoom.push('<a href="/' + toId(_room.title) + '" class="ilink">' + _room.title + '</a> (' + _room.userCount + ')');
					continue;
				}
			}
			if (_room.type !== 'battle' && _room.id !== 'global') nonOfficial.push('<a href="/' + toId(_room.title) + '" class="ilink">' + _room.title + '</a> (' + _room.userCount + ')');
		}
		this.sendReplyBox(header + official.join(' ') + nonOfficial.join(' ') + privateRoom.join(' ') + (groupChats.length > 1 ? groupChats.join(' ') : '') + (battleRooms.length > 1 ? battleRooms.join(' ') : ''));
},
        clearall: function (target, room, user) {
                if (!this.can('ban')) return;
		        if (room.battle) return this.sendReply('You cannot do it on battle rooms.');

		                let len = room.log.length,
                        users = [];
                                while (len--) {
                                        room.log[len] = '';
                                }
		                        for (var user in room.users) {
			                                users.push(user);
			                                Users.get(user).leaveRoom(room, Users.get(user).connections[0]);
		                        }
		                len = users.length;
		                        setTimeout(function () {
		                                	while (len--) {
				                                Users.get(users[len]).joinRoom(room, Users.get(users[len]).connections[0]);
			                                }
		                        }, 1000);
},
};
