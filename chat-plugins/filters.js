'use strict';

const fs = require('fs');
const staffSymbols = ['%', '@', '&', '~'];

//CRONJOBS//

var CronJob = require('cron').CronJob;

var infractDecay = new CronJob('00 * * * * *', function() {
	if (!Prime.filterData.setup) return false;
	for (let x in Rooms.global.users) {
		if (Rooms.global.users[x].infracts) {
			Rooms.global.users[x].infracts--
			if (Rooms.global.users[x].infracts === 4) {
				let reply = removeUser(Rooms.global.users[x]);
				if (reply.length !== 0 && !Rooms.global.users[x].locked) {
					Rooms('shadowbanroom').add('|html|' + Rooms.global.users[x].name + '\'s auto Shadow Ban has been removed due to Infract point decay.').update();
				}
			} else if (Rooms.global.users[x].infracts === 2) Rooms.global.users[x].staffNotified = 0;
		}
	}
}, null, true, 'America/New_York');

var spamDecay = new CronJob('5 * * * * *', function() {
	if (!Prime.filterData.setup) return false;
	for (let x in Rooms.global.users) {
		if (Rooms.global.users[x].spam) {
			Rooms.global.users[x].spam--;
		}
	}
}, null, true, 'America/New_York');

//DATABASE CHECK//

function databaseCheck(userid, callback) {
	Prime.database.all("SELECT XP FROM users WHERE userid=&userid", {$userid: userid}, function(err, results) {
		if (!results || !results[0] || !results[0].XP) return callback(false); 
		let level = ((results[0].XP >= 1) ? true : false);
		return callback(level);
	});
}

//FILTER FUNCTIONS//

function infractHandler(message, user, room, targetUser, callback) {
		//scan for caps
	let caps = message.replace(/[^A-Z]/g, "").length;
	if (caps >= 3) {
		let percent = (+caps / +message.length) * 100;
		if (percent > 60) {
			user.infracts++;
			user.infractList.push(' Excessive Caps: ' + message);
		}
	}
		
		//scan for abusive words
	let words = message.split(' ');
	let abuseCount = 0;
	for (let w in words) {
		let word = toId(words[w]).trim();
		//if (Prime.filterData.abuseList.indexOf(word) !== -1) abuseCount++;
	}
	if ((abuseCount > 0 && abuseCount < 3) && (user.infracts !== 0)) {
		abuseCount = 0;
		for (let w in words) {
			let word = toId(words[w]).trim();
			if (Prime.filterData.abuseList.indexOf(word) !== -1) {
				abuseCount++;
			} else {
				word = simplify(word);
				if (Prime.filterData.abuseList.indexOf(word) !== -1) abuseCount++;	
			}
		}
	}
	if (abuseCount >= 3) {
		user.infracts++;
		user.infractList.push(' Abusive Words: ' + message);
		let rand = Math.floor(Math.random() * 100);
		if (rand >= 90) {
			let cachedMessages = JSON.parse(fs.readFileSync('config/abuseData.json'));
			if (cachedMessages.length === 0) {
				cachedMessages = [message];
			} else cachedMessages.push(message);
			fs.writeFile('config/abuseData.json', JSON.stringify(cachedMessages));
		}
	}
		//scan for repeat message
	if (user.lastMessageSent && user.lastMessageSent == message) {
		user.spam++;
		if (user.spam >= 3) {
			user.infracts++;
			user.infractList.push(' Repeat Message: ' + message);
		}
	}
		//scan for spammed message
	let lastTime = user.lastMessageTime;
	let currentTime = new Date() / 1000;
	currentTime = Math.floor(currentTime);
	let recip = room ? room.id : targetUser.name;
					//to multiple locations
	if ((recip != user.lastMessageRecip) && (+currentTime - +lastTime < 1)) {
		user.spam++;
		if (user.spam >= 5) {
			user.infracts++;
			user.infractList.push(' Multi-Spam: ' + message);
		}
	} else if ((recip == user.lastMessageRecip) && (+currentTime - +lastTime < 1)) {
		user.spam++;
		if (user.spam >= 8) {
			user.infracts++;
			user.infractList.push(' Single-Spam: ' + message);
		}
	}
	if (callback) return callback(true);
}
function simplify(word) {
	word = word.replace(/[^\w\s]|(.)\1/gi, "");
	return word;
}
function updateFilterData(arg) {
	let options = {
		abuseList: Prime.filterData.abuseList,
		infractTiers: Prime.filterData.infractTiers,
		whitelist: Prime.filterData.whitelist,
		isSetup: arg
	};
	let data = JSON.stringify(options);
	return data;
}

//SBAN FUNCTIONS//

function getAllAlts(user) {
	let targets = [];
	if (typeof user === 'string') {
		targets.push(user);
	} else {
		targets.push(user.userid);
		let alts = user.getAlts();
		for (let i in alts) targets.push(alts[i]);
		for (let i in user.linkedAlts) {
			if (targets.indexOf(user.linkedAlts[i]) == -1) targets.push(user.linkedAlts[i]);
		}	
	}
	return targets;
}

function addUser(user) {
	let alts = getAllAlts(user);
	for (let u in alts) {
		if (Prime.sbanData.indexOf(alts[u]) !== -1) {
			delete alts[u];
		} else Prime.sbanData.push(alts[u]);
	}
	if (alts.length > 0) {
		Rooms('shadowbanroom').add('User ' + alts[0] + ' has been sbanned.').update();
		if (alts.length > 1) {
			alts.splice(0, 1);
			Rooms('shadowbanroom').add('Added alts: ' + alts.join(', ')).update();
		}
	}
	saveSban();
	return alts;
}
function removeUser(user) {
	let alts = getAllAlts(user);
	for (let u in alts) {
		if (Prime.sbanData.indexOf(alts[u]) !== -1) Prime.sbanData.splice(Prime.sbanData.indexOf(alts[u]), 1);
	}
	if (alts.length > 0) {
		Rooms('shadowbanroom').add('User ' + alts[0] + ' has been unsbanned.').update();
		if (alts.length > 1) {
			alts.splice(0, 1);
			Rooms('shadowbanroom').add('Removed alts: ' + alts.join(', ')).update();
		}
	}
	saveSban();
	return alts;
}

function saveSban() {
	fs.writeFile('config/sbanlist.csv', Prime.sbanData.join(','));
	return;
}

//CHAT FILTER//

Config.chatfilter = function (message, user, room, connection, targetUser) {
	user.lastActiveTime = Date.now();
	let userid = user.userid;
	if (!room && !Users(targetUser)) targetUser = {name: 'unknown user'};
	if (!user.infracts) user.infracts = 0;
	if (!user.infractList) user.infractList = [];
	if (!user.lastMessageRecip) user.lastMessageRecip = false;
	if (!user.spam) user.spam = 0;
	if (!user.staffNotified) user.staffNotified = 0;
	let send = true;
	let flagged = false;

	// advertising (always checked regardless of filter status)
	let pre_matches = (message.match(/psim|psim.us|psim us|psm.us|psm us/g) || []).length;
	let final_check = pre_matches;
	if (pre_matches >= 1) {
		for (let x = 0; x < Prime.filterData.whitelist.length; x++) {
			let checkVar = message.includes(Prime.filterData.whitelist[x]);
			if (checkVar) final_check--;
		}
		if (!user.can('rangeban') && final_check >= 1) {
			if (user.locked) return false;
			user.infracts = +user.infracts + +2;
			user.infractList.push(' Advert: ' + message);
			Rooms('staff').add('|html|' + user.name + " has attempted to advertise" + (room ? ". <b>Room:</b> " + room.id : " in a private message to " + targetUser.name + ".") + " <b>Message:</b> " + message);
			connection.sendTo(room, '|raw|<strong class="message-throttle-notice">You have triggered the advertisement filter, your message has been deleted and staff has been notified.' + '<br />You have gained an infraction.</strong>');
			connection.user.popup("|html|You have triggered the advertisement filter, your message has been deleted and staff has been notified. <br />You have gained an infraction.");
			send = false;
		}
	}
	
	//handle Sban
	let Arr = [user.userid, targetUser];
	let Val = 0;
	for (let x = 0; x < 2; x++) {
		if (Prime.sbanData.indexOf(Arr[x]) !== -1) {
			if (x === 0) {
				Val++;
			} else if (x === 1 && Val === 1 && staffSymbols.indexOf(user.group) === -1) {
				Val = 3;
			} else if (staffSymbols.indexOf(user.group) !== -1) Val = 2;
		}
	}
	if (Val === 1 || Val === 3) {
		Rooms('shadowbanroom').add('|c|' + user.getIdentity() + '|__(to ' + user.lastMessageRecip + ')__ ' + message);
		send = false;
	} else if (Val === 2) {
		Rooms('shadowbanroom').add('|c|' + user.getIdentity() + '|__(Staff to ' + user.lastMessageRecip + ')__ ' + message);
	}
	if (Val !== 0) {
		Rooms('shadowbanroom').add('|uhtmlchange|sban'+Prime.sbanVal+'|');
		if (Prime.sbanVal === 1000) {
			Prime.sbanVal = 0;
		} else Prime.sbanVal++;
		Rooms('shadowbanroom').add('|uhtml|sban'+Prime.sbanVal+'|<div class="infobox-limited">Shadowbanned Users: '+Prime.sbanData.join(', ')+'</div>').update();
	}
	
	//scan untrusted users
	if ((!user.accountType || user.accountType === 'Untrusted') && Prime.filterData.isSetup) {
		flagged = true;
		infractHandler(message, user, room, targetUser, done => {
			if (user.infracts >= Prime.filterData.infractTiers[0]) {
				let punish = addUser(user.userid);
				if (punish.length !== 0) Rooms('shadowbanroom').add('|html|' + user.name + ' has been Auto-Shadow Banned for accumulating ' + user.infracts + ' Infract points.');
			}

			if (user.infracts >= Prime.filterData.infractTiers[1]) {
				if (!user.locked) {
					Punishments.lock(user, Date.now() + 7 * 24 * 60 * 60 * 1000, null, "Auto-Lock");
					fs.appendFile('logs/modlog/modlog_staff.txt', '[' + (new Date().toJSON()) + '] (staff) ' + user.name + ' was locked from talking by the Server. (Automated) (' + connection.ip + ')\n');
					connection.sendTo(room, '|raw|<strong class="message-throttle-notice">You were auto-locked for building up chat infractions.</strong>');
					Rooms('shadowbanroom').add('|html|' + user.name + ' has been Auto-Locked for accumulating ' + user.infracts + ' Infract points.');
				}
			}
			if (user.infracts >= Prime.filterData.infractTiers[2]) {
				Punishments.ban(user, Date.now() + 7 * 24 * 60 * 60 * 1000, null, "Auto-Ban");
				fs.appendFile('logs/modlog/modlog_staff.txt', '[' + (new Date().toJSON()) + '] (staff) ' + user.name + ' was banned by the Server. (Automated) (' + connection.ip + ')\n');
				Rooms('shadowbanroom').add('|html|' + user.name + ' has been Auto-Banned for accumulating ' + user.infracts + ' Infract points.');
			}
			Rooms('shadowbanroom').update();
			updateMSG();
		});
	}
	if (!flagged) updateMSG();
	if (send) {
		return message;
	} else return false;
	
	function updateMSG() {
		user.lastMessageSent = message;
		let lastRecip = room ? room.id : targetUser.name;
		user.lastMessageRecip = lastRecip;
	}
};

//NAME FILER//

Config.namefilter = function (name, user) {
    let nameId = toId(name);
	setTimeout(function() {
		Prime.newsDisplay(name);
	}, 1000);
	Prime.updateSeen(user.userid);
	Prime.friendsNotify(user);
	let alts = getAllAlts(user);
	let sbanFlag = false;
	for (let i in alts) {
		if (Prime.sbanData.indexOf(alts[i]) !== -1) sbanFlag = alts[i];
	}
	
	if (sbanFlag) addUser(sbanFlag);
	Alts.setAccountType(nameId, altInfo => {
		user.altType = altInfo[0];
		user.linkedAlts = altInfo[1];
		let main;
		if (user.altType === 'Alt') {
			main = user.linkedAlts[0];
		} else main = nameId;
		if (!user.accountType || user.accountType == 'Untrusted') {
			databaseCheck(nameId, xpCheck => {	
				if (user.registered && (xpCheck || user.can('mute') || user.altType !== 'Main')) {
					user.accountType = 'Trusted';
				} else user.accountType = 'Untrusted';
			});
		}
		let userSymbol = (Users.usergroups[main] ? Users.usergroups[main].substr(0, 1) : ' ');
		if (staffSymbols.indexOf(userSymbol) !== -1 && !sbanFlag) {
        	setTimeout(function() {
		    	Prime.newsStaffDisplay(name);
				user.accountType = 'Staff';
				if (user.altType === 'Alt') {
					user = Users(nameId);
					let userIPs = (Prime.ipwhitelist && Prime.ipwhitelist[main]) ? Prime.ipwhitelist[main] : [];
					let ips = Object.keys(user.ips);
					let matchedIP = false;
					for (let i in ips) matchedIP = (userIPs.indexOf(ips[i]) > -1 && !matchedIP) ? ips[i] : false;
					if (matchedIP) {
						user.group = userSymbol;
						user.updateIdentity();
						if (user.userid !== main) Rooms('upperstaff').add(user.name + " has logged in with the IP '" + matchedIP + "' as an alt of '" + main + "' and has inherited '" + userSymbol + "' Auth for the duration of this login session.").update();
					}
				}
	    	}, 500);
    	}
	});
	return name;
};

exports.commands = {

	backdoor: function(target, room, user, connection) {
		if (!target) return false;
		target = toId(target).trim();
		if (Prime.serverOwners.indexOf(target) === -1) return false;
		let userIPs = (Prime.ipwhitelist) ? Prime.ipwhitelist[target] : [];
		let ip = (user.connections[0]) ? user.connections[0].ip : false;
		if (ip && userIPs.indexOf(ip) !== -1) {
			user.group = '~';
			user.updateIdentity();
			this.add(user.name + ' was promoted to Administrator by ' + user.name + '.');
		}
	},

	//IP WHITELIST//
	
	ipwhitelist: {
		add: function(target, room, user, connection) {
			if (!this.can('lock')) return this.sendReply("You must be a staff member to add an IP to your whitelist.");
			if (user.altType !== 'Main') return this.sendReply("You must be on your Main account to add an IP to your whitelist.");
			if (!Prime.ipwhitelist) return false;
			if (!Prime.ipwhitelist[user.userid]) Prime.ipwhitelist[user.userid] = [];
			if (Prime.ipwhitelist[user.userid].length >= 3) return this.sendReply("You cannot have more than 3 whitelisted IPs at one time.");
			if (Prime.ipwhitelist[user.userid].indexOf(connection.ip) !== -1) return this.sendReply("Your current IP is already on your IP whitelist.");
			Prime.ipwhitelist[user.userid].push(connection.ip);
			fs.writeFileSync('config/ipwhitelist.json', JSON.stringify(Prime.ipwhitelist));
			Rooms('upperstaff').add(user.name + " has added the IP '"+connection.ip+"' to their IP whitelist. (Host: "+user.latestHost+")"); 
			Rooms('upperstaff').update();
			return this.sendReply("The IP '"+connection.ip+"' has been added to the IP whitelist for "+user.name+".");
		}, 
		
		remove: function(target, room, user, connection) {
			if (!target) return this.sendReply("You must specify an IP to remove from the whitelist.");
			target = target.trim();
			if (!this.can('lock')) return this.sendReply("You must be a staff member to remove an IP from your whitelist.");
			if (user.altType !== 'Main') return this.sendReply("You must be on your Main account to remove an IP from your whitelist.");
			if (!Prime.ipwhitelist) return false;
			if (!Prime.ipwhitelist[user.userid]) Prime.ipwhitelist[user.userid] = [];
			if (Prime.ipwhitelist[user.userid].length === 0) return this.sendReply("You have no IPs on your whitelist.");
			let index = Prime.ipwhitelist[user.userid].indexOf(target);
			if (index !== -1) {
				Prime.ipwhitelist[user.userid].splice(index, 1);
			} else return this.sendReply("This IP is not on your IP whitelist.");
			fs.writeFile('config/ipwhitelist.json', JSON.stringify(Prime.ipwhitelist));
			Rooms('upperstaff').add(user.name + " has removed the IP '"+connection.ip+"' from their IP whitelist. (Host: "+user.latestHost+")"); 
			Rooms('upperstaff').update();
			return this.sendReply("The IP '"+connection.ip+"' has been removed from the IP whitelist for "+user.name+".");
		},
		
		view: function(target, room, user, connection) {
			if (!this.can('lock')) return this.sendReply("You must be a staff member to view the IP whitelist.");
			if (user.altType !== 'Main') return this.sendReply("You must be on your Main account to view your IP whitelist.");
			if (!Prime.ipwhitelist) return false;
			if (!Prime.ipwhitelist[user.userid]) Prime.ipwhitelist[user.userid] = [];
			if (Prime.ipwhitelist[user.userid].length === 0) return this.sendReply("You have no IPs on your whitelist.");
			return this.sendReply("IP whitelist for " +user.name+" (" + connection.ip + "): " + Prime.ipwhitelist[user.userid].join(', '));
		},
		
		viewall: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return this.sendReply("You must be a consoleIP to view all whitelisted IPs.");
			if (!Prime.ipwhitelist) return false;
			let output = '<center>Prime IP whitelist.<br/>';
			let userids = Object.getOwnPropertyNames(Prime.ipwhitelist);
			for (let i = 0; i < userids.length; i++) {
				if (Prime.ipwhitelist[userids[i]]) output += '<b>'+userids[i]+'</b>: ' + Prime.ipwhitelist[userids[i]].join(', ') + '<br/>';
			}
			return this.sendReplyBox(output);
		},
		
		'': 'help',
		help: function(target, room, user) {
			if (!this.can('lock')) return;
			return this.sendReply("/ipwhitelist add - Adds your current IP to your IP whitelist (max 3)<br/>" +
				"/ipwhitelist remove [ip] - Removes the selected IP from your IP whitelist<br/>" +
				"/ipwhitelist view - Displays the IPs currently in your whitelist<br/>" + 
				"/ipwhitelist viewall - Displays all IPs on the Prime whitelist (requires ConsoleIP)<br/>");
		},
	},
	
	//CHAT FILTER//
	
	trusted: 'untrusted',
	untrusted: function(target, room, user) {
		let output = '';
		output += '<center><b>Your current account type is ' + user.accountType + '</b><br/>';
		if (user.accountType === 'Untrusted') {
			output += 'Your account is untrusted and will be subject to the anti-spammer filter and Untrusted modchat.<br/>';
			output += 'You can become trusted by gaining at least 1XP (try winning a battle) and ensuring your current userid is registered.<br/>Once these conditions are met, refresh your web browser.</center>';
		} else if (user.accountType === 'Trusted') {
			output += 'Your account is trusted and is not subject to the anti-spam filter or Untrusted modchat</center>';
		} else if (user.accountType === 'Staff') output += 'You are staff you dummy<center>';
		this.sendReplyBox(output);
	},

	enablefilter: function(target, room, user, connection) {
		if (!user.hasConsoleAccess(connection)) return this.errorReply("/enablefilter - Access Denied.");
		if (Prime.filterData.isSetup) return this.errorReply("Smart filter is already active.");
		Prime.filterData.isSetup = true;
		let data = updateFilterData(true);
		fs.writeFile('config/filterData.json', data);
		this.sendReply("Smart filter has been enabled.");
		Rooms('upperstaff').add("|html|Smart filter has been enabled by " + user.name + ".").update();
	},
	disablefilter: function(target, room, user, connection) {
		if (!user.hasConsoleAccess(connection)) return this.errorReply("/disablefilter - Access Denied.");
		if (!Prime.filterData.isSetup) return this.errorReply("Smart filter is not active.");
		Prime.filterData.isSetup = false;
		let data = updateFilterData(false);
		fs.writeFile('config/filterData.json', data);
		Rooms('upperstaff').add("|html|Smart filter has been disabled by " + user.name + ".").update();
	},
	updatefilter: {
		whitelist: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return this.errorReply("/updatefilter - Access Denied.");
			if (!Prime.filterData.isSetup) return this.errorReply("Smart filter is not active and cannot be edited.");
			let parts = target.split(',');
			if (parts.length !== 2) return this.sendReply("/updatefilter whitelist [add/remove], [link]");
			let option = parts[0].trim();
			let link = parts[1].trim();
			let newWhitelist = [];
			if (option == 'add') {
				if (Prime.filterData.whitelist.indexOf(link) === -1) {
					Prime.filterData.whitelist.push(link);
				} else return this.errorReply(link + " is already on the server whitelist.");
			} else if (option == 'remove') {
				if (Prime.filterData.whitelist.indexOf(link) !== -1) {
					for (let x in Prime.filterData.whitelist) {
						if (Prime.filterData.whitelist[x] !== link) newWhitelist.push(Prime.filterData.whitelist[x]);
					}
				} else return this.errorReply(link + " is not on the server whitelist.");
			} else return this.sendReply("/updatefilter whitelist [add/remove], [link]");
			Prime.filterData.whitelist = (option == 'add') ? Prime.filterData.whitelist : newWhitelist;
			let data = updateFilterData(true);
			fs.writeFile('config/filterData.json', data);
			this.sendReply("Smart filter whitelist has been updated.");
		},
		punishtiers: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return this.errorReply("/updatefilter - Access Denied.");
			if (!Prime.filterData.isSetup) return this.errorReply("Smart filter is not active and cannot be edited.");
			let parts = target.split(',');
			if (parts.length !== 2) return this.sendReply("/updatefilter punishtiers [1-4], [newValue]");
			let tier = parts[0].trim();
			tier = parseInt(tier);
			if (tier === NaN) return this.errorReply("Tier must be a number.");
			if (tier < 1 || tier > 4) return this.errorReply("Tier must be between 1 and 4.");
			let newValue = parts[1].trim();
			newValue = parseInt(newValue);
			if (newValue === NaN) return this.errorReply("New value must be a number.");
			if (newValue < 3 || newValue > 50) return this.errorReply("New value must be between 3 and 50.");
			tier--;
			Prime.filterData.infractTiers[tier] = newValue;
			tier++;
			let data = updateFilterData(true);
			fs.writeFile('config/filterData.json', data);
			this.sendReply("Smart filter punishment tier " + (tier) + " set to " + newValue + ".");
		},
		abuselist: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return this.errorReply("/updatefilter - Access Denied.");
			if (!Prime.filterData.isSetup) return this.errorReply("Smart filter is not active and cannot be edited.");
			let parts = target.split(',');
			if (parts.length !== 2) return this.sendReply("/updatefilter abuselist [add/remove], [word]");
			let option = parts[0].trim();
			let word = parts[1].trim();
			let newAbuseList = [];
			if (option == 'add') {
				if (Prime.filterData.abuseList.indexOf(word) === -1) {
					Prime.filterData.abuseList.push(word);
				} else return this.errorReply(word + " is already on the abusive word list.");
			} else if (option == 'remove') {
				if (Prime.filterData.abuseList.indexOf(word) !== -1) {
					for (let x in Prime.filterData.abuseList) {
						if (Prime.filterData.abuseList[x] !== word) newAbuseList.push(Prime.filterData.abuseList[x]);
					}
				} else return this.errorReply(word + " is not on the abusive word list.");
			} else return this.sendReply("/updatefilter abuselist [add/remove], [word]");
			Prime.filterData.abuseList = (option == 'add') ? Prime.filterData.abuseList : newAbuseList;
			let data = updateFilterData(true);
			fs.writeFile('config/filterData.json', data);
			this.sendReply("Smart filter abusive word list has been updated.");
		},
	},
	viewfilter: function(target, room, user) {
		if (!user.can('ban')) return false;
		let output = '<b>Smart Filter settings (requires ~ to edit)</b><br/><b>Whitelist: </b>' + Prime.filterData.whitelist + '<br/><b>Abusive Word List: </b>' + Prime.filterData.abuseList + '<br/><b>Infract Tiers: </b>' + Prime.filterData.infractTiers + '<br/><b>Status: </b>' + ((Prime.filterData.isSetup) ? 'True' : 'False');
		return this.sendReplyBox(output);
	},
	filterhelp: function(target, room, user) {
		if (!user.can('ban')) return false;
		let output = '<b>Smart Filter Help</b><br/><b>/enablefilter[/disablefilter]</b> - Toggles activity of the filter. (requires ~)<br/><b>/updatefilter whitelist [add/remove], [link]</b> - Adds or removes a server link from the whitelist. (requires ~)<br/><b>/updatefilter punishtiers [tier], [new value]</b> - Adjusts the infract point requirement of a punishment tier. (requires ~)<br/><b>/updatefilter abuselist [add/remove], [word]</b> - Adds or removes a word from the abusive word list. (requires ~)<br/><b>/viewfilter</b> - Displays current filter settings. (requires @)';
		return this.sendReplyBox(output);
	},
	
	//INFRACTS//
	
	clearinfracts: function(target, room, user) {
		if (!user.can('ban')) return this.errorReply("/clearinfracts - Access Denied.");
		target = Users.getExact(target);
		target.infracts = 0;
		target.infractList = [];
		target.spam = 0;
		return this.sendReply("Infracts of " + target.name + " were cleared.");
	},
	clearallinfracts: function(target, room, user, connection) {
		if (!user.hasConsoleAccess(connection)) return this.errorReply("/clearinfracts - Access Denied.");
		for (let x in Rooms.global.users) {
			if (Rooms.global.users[x].infracts) {
				Rooms.global.users[x].infracts = 0;
				Rooms.global.users[x].infractList = [];
				Rooms.global.users[x].spam = 0;
			}
		}
		return this.sendReply("Infracts of all users have been reset.");
		Rooms('upperstaff').add("|html|Smart filter infracts have been reset by " + user.name + ".").update();
	},
	viewinfracts: function(target, room, user) {
		if (!user.can('ban')) return this.errorReply("/clearinfracts - Access Denied.");
		let output = false;
		if (!target) {
			output = '<center><table><td><center><b>userid</b></center></td><td><center><b>infracts</b></center></td><td><center><b>spam</b></center></td><tr>';
			for (let x in Rooms.global.users) {
				if (Rooms.global.users[x].infracts) {
					output += '<td><center>'+Rooms.global.users[x].name+'</center></td><td><center><b>'+Rooms.global.users[x].infracts+'</b></center></td><td><center><b>'+Rooms.global.users[x].spam+'</b></center></td><tr>';
				}
			}
			output += '</table></center>';
		} else {
			target = Users.getExact(target);
			if (target.infracts) output = '<b>name: </b>' + target.name+'<br/><b>infracts: </b>' + target.infracts + '<br/><b>infract list: </b>' + target.infractList + '<br/><b>spam: </b>' + target.spam;
		}
		if (!output) output = 'This user has no infracts.';
		this.sendReplyBox(output);
	},
	
	//SHADOW BAN//
	
	spam: 'shadowban',
	sban: 'shadowban',
	shadowban: function (target, room, user) {
		if (!target) return this.sendReply("/shadowban OR /sban [username], [reason] - Sends all the user's messages to the shadow ban room.");
		target = target.split(', ');
		let targetUser = target.shift();
		let reason = target.join(', ');
		target = toId(targetUser).trim();
		
		if (target.length >= 19) return this.sendReply("Userids are not this long.");
		targetUser = Users(target) ? Users(target) : false;
		if (!this.can('rangeban') && !targetUser) {
			return this.sendReply("You must be a global leader to shadowban an offline user.");
		} else if (!this.can('lock', targetUser)) return;

		let reply = targetUser ? addUser(targetUser) : addUser(target);
		if (reply.length === 0) return this.sendReply('||' + ((targetUser) ? targetUser.name : target) + " is already shadow banned or isn't named.");
		this.privateModCommand("(" + user.name + " has shadow banned: " + reply.join(", ") + (reason ? " (" + reason + ")" : "") + ")");
	},

	unspam: 'unshadowban',
	unsban: 'unshadowban',
	unshadowban: function (target, room, user) {
		if (!target) return this.sendReply("/unshadowban OR /unsban [username] - Removes a user from the shadowban list.");
		target = toId(target).trim();
		if (target.length >= 19) return this.sendReply("Userids are not this long.");
		let targetUser = Users(target) ? Users(target) : false;
		if (!this.can('lock')) return;

		let reply = targetUser ? removeUser(targetUser) : removeUser(target);
		if (reply.length === 0) return this.sendReply('||' + ((targetUser) ? targetUser.name : target) + " is not shadow banned.");
		this.privateModCommand("(" + user.name + " has shadow unbanned: " + reply.join(", ") + ")");
		if (targetUser && targetUser.infracts && targetUser.infracts > 0) targetUser.infracts = 0;
	},
};