'use strict'

/*********************************************
* Interactive multi-shop interface by Lights *
*********************************************/

/************
* Shop help *
*************
*  This interactive shop is designed to be used with multiple shops; hence the menus and sub menus;
*  if your server doesn't use multiple shops; this isn't for you.
*
*  If you want to add/remove items and shop menus, you'll need to edit shopData.json. A list of
*  properties and what they do is listed below. If you are adding a shop item, you'll also
*  need to add a case for it into the redeem command. Adding menus, obviously, requires no such effort. 
*
**************
* Properties *
**************
*
* closed - can be included as a property of shopData; if this is true, the shop and redeem commands will be rendered unusable. 
*
* display - The name that will be displayed in the shop for this item/menu
*
* info - this can be set to a string of text that will be added into the info-bar marquee at the bottom of the shop. typically used to provide info about the item or menu the user is on
*
* items - a property for menus that allows you to add shop items or more sub-menus for navigation. sub menus and shop items should not be contained in the same object
*
* price - the value that is required to be paid in the set currency when this item is redeemed. should be an integer value.
*
* currency - the type of currency required. this is used for the shop UI as well as the redeem command
*
* img - a property for shop items that allows you to specify an example image of what the user is buying.
*
* multibuy - a property for shop items that can be bought multiple times in succession.
*
* onbuy - a property for shop items that displays on-purchase instructions
*
************/

const fs = require('fs');
const size = require('request-image-size');

if (!Prime.shopData.closed) buildShop(Prime.shopData);

function buildShop(shopData) {
    let properties = Object.getOwnPropertyNames(shopData);
    let arr = [];
    for (let i in properties) {
        let name = properties[i];
        arr.push(shopData[name].items);
    }
    for (let i = 0; i < arr.length; i++) {
        getItems(arr[i], properties[i]);
    }
}
function getItems(obj, objname){
    let properties = Object.getOwnPropertyNames(obj);
    for (let i in properties) {
        let name = properties[i];
        if (obj[name].items) {
            getItems(obj[name].items, name);
        } else {
            let shopItem = {
                name: name,
                display: obj[name].display,
                price: obj[name].price,
                multibuy: obj[name].multibuy,
                currency: obj[name].currency,
                onbuy: obj[name].onbuy
            };
            Prime.itemList.push(shopItem);
            if (Prime.currencies.indexOf(shopItem.currency) === -1) Prime.currencies.push(shopItem.currency);
        }
    }
}      

function drawMain(output) {
    let shopData = Prime.shopData
    output += '<div><center><table border=0>';
    let arr = Object.getOwnPropertyNames(shopData);
    let outputArr = [];
    for (let x in arr) {
        let item = arr[x];
        outputArr.push(shopData[item]);
    }
    for (let x = 0; x < outputArr.length; x++) {
        let name = outputArr[x].display;
        output += '<td style="padding: 8px;"><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+arr[x]+'|1">'+name+'</button></td>';
        if ((x === 2 || x === 5 || x === 8) && x != (+outputArr.length - +1)) output += '</table><br/><table border=0>';
    }
    output += '</table></center><br/></div>';
    return output;
}

function assembleOutput(output, marquee, shop, selectionType, back, update, user, room, cache) {
    output += '<br/><div>';
    output += '<div style="padding: 5px; border: 2px solid #6B2AC9; background: black; color: #fff; text-shadow: 0px -1px 0px #000; border-radius: 6px; margin: 10px"><div style="float: left; background-color: black; position: absolute; z-index: 10;">' + ((selectionType !== 'main' && selectionType !== 'exit') ? '<button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop main|1">Main Menu</button> <button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop '+back+'|1">Back</button> ' : '') + '<button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop exit|1">Exit</button></div><marquee style="text-align: center; width: 100%" direction="left" speed="fast">'+marquee+'</marquee></div>';
    shop = '<div style="padding: 5px; border: 2px solid #6B2AC9; background: black; color: #fff; text-shadow: 0px -1px 0px #000; border-radius: 6px; margin: 10px"><center><font size=4><i><b>Prime Server Shop</b></i></font></center></div><br/><br/>' + output;
    if (selectionType == 'exit') shop = '<center>Shop closed - click <button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop main|1">here</button> to reopen the shop.</center>';
	if (cache) user.shopCache = shop;
    display(update, user, room, shop);
}

function display(update, user, room, output) {
    let uhtml = update ? '|uhtmlchange|'+user.userid+'shop|' : '|uhtml|'+user.userid+'shop|';
	Users.get(user.userid).connections[0].sendTo(room.id, uhtml + output);
}

function runTransaction(money, currency, item, user){
	if (money < item.price) return false;
    switch(currency) {
        case 'points':
	        Economy.writeMoney(user.userid, item.price * -1);
            break;
        case 'credits':
            Prime.writeCredits(user.userid, item.price * -1);
            break;
    }   
	let receiptId = genReceiptId();
	let remaining = (+money - +item.price);
	let receipt = user.userid + "|" + new Date().toUTCString() + "|" + item.display + "|" + item.price + "|" + item.currency + "|" + receiptId + "|" + remaining + "|0";
	logReceipt(receipt);
	return receipt;
}

function logReceipt(receipt) {
	fs.appendFile('logs/transactions.log', receipt + '\n');
}

function failedTransaction(user, item, money, room) {
	if (!user.shopCache) return false;
	let cache = user.shopCache;
    if (cache == '') return false;
	let parts = cache.split('<!-- split -->');
	let remaining = (+item.price - +money);
	let output = parts[0] + '<br/><br/><center><b>You do not have enough ' + item.currency + '<br/>' + remaining + ' more ' + item.currency + ' requied.</b></center>' + parts[2];
	
	Users.get(user.userid).connections[0].sendTo(room.id, '|uhtmlchange|'+user.userid+'shop|' + output);
}

function successfulTransaction(item, receipt, user, room) {
    receipt = receipt.split('|');
	let output = '<div style="padding: 5px; border: 2px solid #6B2AC9; background: black; color: #fff; text-shadow: 0px -1px 0px #000; border-radius: 6px; margin: 10px; text-align: center"><font size=4><i><b>Prime Server Shop</b></i></font></div><br/><br/>';
	output += '<center><b>Your purchase was successful</b><br/><br/>' + item.onbuy + '<br/></center><div style="width: 70%;margin: 0 auto;"><b>Name: </b>' + user.name + '<br/><b>Date: </b>' + receipt[1] + '<br/><b>Item: </b>' + receipt[2] + '<br/><b>Price: </b>' + receipt[3] + ' ' + receipt[4] + '<br/><b>Remaining ' + receipt[4] + ': </b>' + receipt[6] + '<br/><b>ReceiptID: </b>' + receipt[5] + '<br/><br/><center>This receipt is stored as your proof of purchase in the event of an error.<br/>Use /receipts to see your recent receipts.<br/><br/>';
	output += '<button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop main|1">Main Menu</button> ' + ((item.multibuy && (receipt[3] < receipt[4])) ? '<button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/rebuy ' + item.name + '">Buy Another</button>' : '');
	output += '<br/></center></div><div style="padding: 5px; border: 2px solid #6B2AC9; background: black; color: #fff; text-shadow: 0px -1px 0px #000; border-radius: 6px; margin: 10px"><div style="float: left; background-color: black; position: absolute; z-index: 10;"><button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop main|1">Main Menu</button> <button style="border: 2px solid #4506a3 ; border-radius: 6px; background: black ; color: white;" name="send" value="/shop exit|1">Exit</button></div><marquee style="text-align: center; width: 100%" direction="left" speed="fast>Thank you for your purchase!</marquee></div>';
	
	Users.get(user.userid).connections[0].sendTo(room.id, '|uhtmlchange|'+user.userid+'shop|' + output);
}

function genReceiptId(){
	let receipt;
	let lines = fs.readFileSync('logs/transactions.log', 'utf8').split('\n').reverse();
	let existing = [];
	for (let i in lines) {
		let parts = lines[i].split(',');
		existing.push(lines[5]);
	}
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	do {
		receipt = "";
		for (let i = 0; i < 8; i++) {
			receipt += possible.charAt(Math.floor(Math.random() * possible.length));
		}
	} while (existing.indexOf(receipt) !== -1);
    return receipt;
}

//add different currencies as args
function setupPrice(item, points, credits){
    let money = [];
	if (item.currency === 'points') {
        money.push(points);
    } else if (item.currency === 'credits') {
        money.push(credits);
    }
    money.push(item.currency);
    return money;
}

function scaleImage(url, callback) {
    if (url) {
        size(url, function(err, dimensions, length) {
            let width = dimensions.width;
            let height = dimensions.height;
            if (height >= 120) {
                let scaleFactor = +height / +120;
                height = +height / +scaleFactor;
                width = +width / +scaleFactor;
            }
            let arr = [url, width, height];
            return callback(arr);
        });
    } else return callback(false);
}

function buyPack(user, packId) {
    if (!Users.userPacks[user.userid]) Users.userPacks[user.userid] = [];
    Users.userPacks[user.userid].push(packId);
}

exports.commands = {

    creditshop: 'shop',
    packshop: 'shop',
    pokemart: 'shop',
    shop: function(target, room, user) {
        let shopData = Prime.shopData;
        if (Prime.shopData.closed) return this.sendReply("The shop is currently closed; check back later.");
        let shop;
        let output = '';
        let update = false;
        let selectionType = false;
		let selection = false;
        let isUpdate;
        let marquee = 'Welcome to the Prime Server Shop! This is still an experimental version of the shop; any feedback is welcome.';
		if (target) {
            if (target.trim() === 'help') {
                return this.sendReplyBox("/shop - opens shop UI<br/>/receipts - lists your recent receipts<br/>/receipt [receipt ID] - displays the details of the specified receipt if you are the owner<br/>/receipts view, [user] - lists receipts of the specified user (requires @)<br/>Multi-Shop overhaul by Lights");
            } else if (target.trim() === 'credit' || target.trim() === 'credits') {
                return this.sendReply("Multi-Shop overhaul by Lights");
            }
			target = target.split('|');
			if (target.length !== 2) {
				return this.parse('/shop');
			} else {
				selection = target[0].trim();
				isUpdate = target[1].trim();
				if (isUpdate === '1'|| isUpdate === '2') update = true;
			}
            if (update) {
                let parts = false;
                if (selection.includes('.')) {
                    parts = selection.split('.');
                }
                if (!parts) {
                    if (selection == 'main') {
                        output = drawMain(output);
                        selectionType = 'main';
                        assembleOutput(output, marquee, shop, selectionType, false, update, user, room, false);
                    } else if (selection == 'exit') {
                        selectionType = 'exit';
                        assembleOutput(output, marquee, shop, selectionType, false, update, user, room, false);
                    } else {
                        let arr = Object.getOwnPropertyNames(shopData);
                        let match = false;
                        for (let x in arr) {
                            if (selection == arr[x]) match = arr[x];
                        }
                        if (match) {
                            selection = shopData[match].items;
                            arr = Object.getOwnPropertyNames(selection);
                            let test = arr[0];
                            selectionType = selection[test].items ? '1' : '2';
                            let outputArr = [];
                            for (let y in arr) {
                                let item = arr[y];
                                outputArr.push(selection[item]);
                            }
                            marquee = shopData[match].info;
                            if (selectionType == '1') {
                                output += '<center><table border=0>'
                                for (let x = 0; x < outputArr.length; x++) {
                                    let name = outputArr[x].display;
                                    let button = match+'.'+arr[x];
                                    output += '<td style="padding: 8px;"><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+button+'|1">'+name+'</button></td>';
                                    if ((x === 2 || x === 5 | x=== 8) && x != (+outputArr.length - +1)) output += '</table><br/><table border=0>';
                                }
                                output += '</table></center><br/></div>';
                                assembleOutput(output, marquee, shop, selectionType, 'main', update, user, room, false);
                            } else {
                                //item selection
                                output += '<div style="position: relative;"><div style="width: 60%; max-height: 300px; min-height: 250px; overflow-y: scroll;"><center><table border=0>';
                                for (let x = 0; x < outputArr.length; x++) {
                                    let name = outputArr[x].display;
                                    let button = match+'.'+arr[x];
                                    output += '<td style="padding: 5px;"><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+button+'|2">'+name+'</button></td><tr>';
                                }
                                output += '</table></center><br/></div>';
                                //item info empty
                                output += '<div style="position: absolute; top: 0px;right: 0px;bottom: 0px; width: 39%; max-height: 300px; min-height: 250px; background-color: rgba(204,102,255,0.4);border: 2px solid; border-radius: 5px;"><br/><br/><center><b>Select an Item for more info.</b></center></div></div>';
                                assembleOutput(output, marquee, shop, selectionType, 'main', update, user, room, false);
                            }
                        } else {
                            output = drawMain(output);
                            selectionType = 'main';
                            assembleOutput(output, marquee, shop, selectionType, update, user, room, false);
                        }
                    }
                } else {
                    let length = parts.length;
                    let prog = 0;
                    let arr = Object.getOwnPropertyNames(shopData);
                    let path = [];
                    let match = false;
                    let container = false;
                    let containerArr = false;
                    for (let z = 0; z < length; z++) {
                        for (let x in arr) {
                            if (parts[z] == arr[x]) {
                                match = arr[x];
                                path.push(match);
                                prog++;
                            }
                        }
                        if (match) {
                            if (z == 0) {
                                selection = shopData[match].items;
                            } else if (z > 0 && z != (+length - +1)) {
                                selection = selection[match].items;
                            } else if (z == (+length - +1) && !selection[match].items) {
                                container = selection;
                                marquee = container.info;
                                selection = selection[match];
                            } else {
                                marquee = selection[match].info;
                                selection = selection[match].items;
                            }
                            arr = Object.getOwnPropertyNames(selection);
                            if (container) containerArr = Object.getOwnPropertyNames(container);
                            let test = arr[0];
                            selectionType = selection[test].items ? '1' : '2';
                            if (selectionType == '2' && prog !== length) {
                                match = false;
                            }
                        }
                    }
                    if (match && prog == length && length == path.length) {
                        if (isUpdate == '1') {
                            if (selectionType == '1') {
                                output += '<center><table border=0>'
                                for (let x = 0; x < outputArr.length; x++) {
                                    let last = '';
                                    let back = '';
                                    if (path.length >= 2) {
                                        for (let q = 0; q < path.length; q++) {
                                            last += path[q];
                                            if (q !== (+path.length - +1)) {
                                                last += '.';
                                                back += path[q];
                                                if (q !== (+path.length - +2)) back += '.';
                                            }
                                        }
                                    } else {
                                        last = (+path.length - +1);
                                        last = path[last];
                                        back = 'main'; 
                                    }
                                    let name = outputArr[x].display;
                                    let button = last+'.'+arr[x];
                                    output += '<td style="padding: 8px;"><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+button+'|1">'+name+'</button></td>';
                                    if ((x === 2 || x === 5 | x=== 8) && x != (+outputArr.length - +1)) output += '</table><br/><table border=0>';
                                }
                                output += '</table></center><br/></div>';
                                assembleOutput(output, marquee, shop, selectionType, back, update, user, room, false);
                            } else {
                                let outputArr = [];
                                for (let y in arr) {
                                    let item = arr[y];
                                    outputArr.push(selection[item]);
                                }
                                output += '<div style="position: relative;"><div style="width: 60%; max-height: 300px; min-height: 250px; overflow-y: scroll;"><center><table border=0>';
                                let back = '';
                                for (let x = 0; x < outputArr.length; x++) {
                                    let last = '';
                                    if (path.length >= 2) {
                                        for (let q = 0; q < path.length; q++) {
                                            last += path[q];
                                            if (q !== (+path.length - +1)) {
                                                last += '.';
                                                if (x == 0) back += path[q];
                                                if (q !== (+path.length - +2) && x == 0) back += '.';
                                            }
                                        }
                                    } else {
                                        last = (+path.length - +1);
                                        last = path[last];
                                        back = 'main';
                                    }
                                    let name = outputArr[x].display;
                                    let button = last+'.'+arr[x];
                                    output += '<td style="padding: 5px;"><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+button+'|2">'+name+'</button></td><tr>';
                                }
                                output += '</table></center><br/></div>';
                                 //item info empty
                                output += '<div style="position: absolute; top: 0px;right: 0px;bottom: 0px; width: 39%; max-height: 300px; min-height: 250px; background-color: rgba(204,102,255,0.4);border: 2px solid; border-radius: 5px;"><br/><br/><center><b>Select an Item for more info.</b></center></div></div>';
                                assembleOutput(output, marquee, shop, selectionType, back, update, user, room, false);
                            }
                        } else {
                            //item selection
                            let outputArr = [];
                            for (let y in containerArr) {
                                let item = containerArr[y];
                                outputArr.push(container[item]);
                            }
                            output += '<div style="position: relative;"><div style="width: 60%; max-height: 300px; min-height: 250px; overflow-y: scroll;"><center><table border=0>';
                            let back = '';
                            for (let x = 0; x < outputArr.length; x++) {
                                let last = '';
                                if (path.length >= 3) {
                                    for (let q = 0; q < (+path.length - +1); q++) {
                                        last += path[q];
                                        if (q !== (+path.length - +2)) {
                                            last += '.';
                                            if (x == 0) back += path[q];
                                            if (q !== (+path.length - +3) && x == 0) back += '.';
                                        }
                                    }
                                } else {
                                    last = (+path.length - +2);
                                    last = path[last];
                                    back = 'main';
                                }
                                let name = outputArr[x].display;
                                let button = last+'.'+containerArr[x];
                                output += '<td style="padding: 5px;"><button style="border: 2px solid ' + ((containerArr[x] == match) ? '#ffffff' : '#4506a3') + ' ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/shop '+button+'|2">'+name+'</button></td><tr>';
                            }
                            
                            let itemId = +path.length - +1;
                            itemId = path[itemId];
                            output += '</table></center><br/></div>';
                            scaleImage(selection.img, image => {
                                output += '<div style="position: absolute; top: 0px;right: 0px;bottom: 0px;width: 39%; max-height: 300px; min-height: 250px; background-color: rgba(204,102,255,0.4);border: 2px solid; border-radius: 5px;"><!-- split --><br/><center><b>' + selection.display + '</b><br/><br/>' + ((image) ? '<img src="'+image[0]+'" width='+image[1]+' height='+image[2]+'><br/>' : '' ) + '<br/><b>Price: </b>' + selection.price + ' ' + selection.currency + '<br/><br/><button style="border: 2px solid #4506a3 ; border-radius: 15px 0px ; background: black ; color: white;" name="send" value="/redeem '+itemId+'">Confirm Purchase</button></center><!-- split --></div></div>';
                                marquee = selection.info;
                                assembleOutput(output, marquee, shop, selectionType, back, update, user, room, true);
                            });
                        }
                    } else {
                        selectionType = 'main';
                        assembleOutput(output, marquee, shop, selectionType, false, update, user, room, false);
                    }
                }
            }
		} else {
            output = drawMain(output);
            selectionType = 'main';
            assembleOutput(output, marquee, shop, selectionType, false, update, user, room, false);
        }
    },

    redeem: function(target, room, user) {
        let shopData = Prime.shopData;
        let err = "Invalid target; this command is not meant to be used manually.";
        if (!target) return this.errorReply(err);
        target = toId(target).trim();
        let match = false;
        let money;
        let success = false;
        let userPacks;
        for (let i in Prime.itemList) {
            if (Prime.itemList[i].name == target) match = Prime.itemList[i];
        }
        if (!match) return this.errorReply(err);
        Economy.readMoney(user.userid, points => {
            Prime.readCredits(user.userid, credits => {
                switch(match.name) {
                    case 'customsymbol':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    user.canCustomSymbol = true;
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
			    	    break;
                    case 'customavatar':
			    	    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    inventory.addItem(user.userid, 'avatartoken');
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
			    	    break;
			        case 'customcolor':
		    		    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
                            inventory.addItem(user.userid, 'colortoken');
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
			    	    break;
                    case 'icon':
		    		    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    inventory.addItem(user.userid, 'icontoken');
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
			    	    break;
                    case 'fix':
		    		    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    inventory.addItem(user.userid, 'fixtoken');
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
			    	    break;
		    	    case 'room':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
	    				if (success) {
	    					Prime.messageSeniorStaff(user.name + " has purchased a room. Please contact this user to setup their room.");
	    					successfulTransaction(match, success, user, room);
	    				} else {
	    					failedTransaction(user, match, money[0], room);
	    				}
	    				user.shopCache = false;
		    	    	break;
	    	    	case 'infobox':
	    		    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
		    				Prime.messageSeniorStaff(user.name + " has purchased an infobox. Please contact this user to setup their infobox. (Please note that they must provide the HTML; you are not obligated to make it for them.");
		    				successfulTransaction(match, success, user, room);
		    			} else {
		    				failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
			        	break;
	    		    case 'league':
	    				money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
			    		if (success) {
			    			Prime.messageSeniorStaff(user.name + " has purchased a League System. Please contact this user to setup their League. (They must be the room founder of the room they wish to add the League System to.");
			    			successfulTransaction(match, success, user, room);
			    		} else {
			    			failedTransaction(user, match, money[0], room);
				    	}
		    			user.shopCache = false;
			        	break;
		    			/* Keep this for later
			        	targetRoom.update();
		    	    	targetRoom.shop = {};
		    		    targetRoom.shopList = [];
		        		targetRoom.chatRoomData.shop = targetRoom.shop;
		        		targetRoom.chatRoomData.shopList = targetRoom.shopList;
		        		Rooms.global.writeChatRoomData();
					*/ 
		        	case 'doublexp':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
			    		if (success) {
			    			user.doublexp = Math.floor(new Date() / 1000);
			    			successfulTransaction(match, success, user, room);
			    		} else {
			    			failedTransaction(user, match, money[0], room);
			    		}
			    		user.shopCache = false;
		        		break;
			    	case 'rbybaseset':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
			    		if (success) {
                            buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
				    	} else {
				    		failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
    				case 'rbyjungle':
    					money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
    						buyPack(user, match.name);
    						successfulTransaction(match, success, user, room);
	    				} else {
	    					failedTransaction(user, match, money[0], room);
	    				}
	    				user.shopCache = false;
                        break;
			    	case 'rbyfossil':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
	    				if (success) {
		    				buyPack(user, match.name);
			    			successfulTransaction(match, success, user, room);
				    	} else {
				    		failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		        		break;
		    		case 'rbyteamrocket':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
		    				buyPack(user, match.name);
		    				successfulTransaction(match, success, user, room);
			    		} else {
				    		failedTransaction(user, match, money[0], room);
					    }
    					user.shopCache = false;
	    	    		break;
		    		case 'rbygymheroes':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
			    		if (success) {
				    		buyPack(user, match.name);
				    		successfulTransaction(match, success, user[0], room);
					    } else {
					    	failedTransaction(user, match, money, room);
					    }
    					user.shopCache = false;
    		    		break;
    				case 'rbygymchallenge':
    					money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
	    				if (success) {
		    				buyPack(user, match.name);
			    			successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'gscneogenesis':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'gscneodiscovery':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'gscneorevelation':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user[0], room);
					    } else {
						    failedTransaction(user, match, money, room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'gscneodestiny':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'gscexpedition':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
                    case 'gscaquapolis':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
                        if (success) {
                            buyPack(user, match.name);
                            successfulTransaction(match, success, user, room);
                        } else {
                            failedTransaction(user, match, money[0], room);
                        }
                        user.shopCache = false;
                        break;
				    case 'rseexteamrocketreturns':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
				    case 'dppbaseset':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
						    buyPack(user, match.name);
						    successfulTransaction(match, success, user, room);
					    } else {
						    failedTransaction(user, match, money[0], room);
					    }
					    user.shopCache = false;
		    		    break;
                    case 'hgssbase':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'hgsscalloflegends':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'hgsstriumphant':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'hgssundaunted':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'hgssunleashed':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
				    case 'bwlegendarytreasures':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
					    if (success) {
					    	buyPack(user, match.name);
					    	successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'bwplasmablast':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
				    		buyPack(user, match.name);
			    			successfulTransaction(match, success, user, room);
			    		} else {
			    			failedTransaction(user, match, money[0], room);
			    		}
			    		user.shopCache = false;
		        		break;
			    	case 'bwplasmafreeze':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
			    		if (success) {
			    			buyPack(user, match.name);
			    			successfulTransaction(match, success, user, room);
			    		} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
	    	    		break;
                    case 'bwexalteddragons':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwdarkexplorers':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwboundaries':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwplasmastorm':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwexpack':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwnextdestinies':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwnoblevictories':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwemergingpowers':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwbase':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
                    case 'bwdragonvault':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
		    		case 'xybase':
		    			money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
		    			if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
			    			failedTransaction(user, match, money[0], room);
		    			}
		    			user.shopCache = false;
		        		break;
	    			case 'xyflasefire':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
	    				if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
	    					failedTransaction(user, match, money[0], room);
	    				}
	    				user.shopCache = false;
	    	    		break;
	    			case 'xyfuriousfists':
	    				money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
	    				if (success) {
	    					buyPack(user, match.name);
	    					successfulTransaction(match, success, user, room);
	    				} else {
	    					failedTransaction(user, match, money[0], room);
	    				}
		    			user.shopCache = false;
		        		break;
				    case 'xyphantomforces':
					    money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
					    	buyPack(user, match.name);
					    	successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'xyprimalclash':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
					    	buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
				    	} else {
				    		failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
				    case 'xyroaringskies':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
						    buyPack(user, match.name);
					    	successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'xyancientorigins':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
					    	buyPack(user, match.name);
					    	successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'xygenerations':
			    		money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
			    		if (success) {
				    		buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
					    } else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
				    case 'xyfatescollide':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
				    		buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
				    	} else {
					    	failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'xybreakpoint':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
				    		buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
				    	} else {
				    		failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
				    case 'xybreakthrough':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
						    buyPack(user, match.name);
					    	successfulTransaction(match, success, user, room);
				    	} else {
				    		failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			    	case 'xysteamsiege':
				    	money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);
				    	if (success) {
				    		buyPack(user, match.name);
				    		successfulTransaction(match, success, user, room);
				    	} else {
						    failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
                    case 'roseticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
					        Economy.writeMoney(user.userid, 5);
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'redticket':
				        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
					        if (user.claimPacks) {
					            user.claimPacks += 1;
                            } else user.claimPacks = 1;
					        Prime.claimPackPopup(user, "Choose a pack for your Red Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'cyanticket':
				        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
					        Economy.writeMoney(user.userid, 15);
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'blueticket':
				        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
                            if (user.claimPacks) {
					            user.claimPacks += 2;
                            } else user.claimPacks = 2;
					        Prime.claimPackPopup(user, "Choose the first pack for your Blue Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'orangeticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Orange Ticket.**");
						    Prime.addTicket(user.name, "Orange Ticket");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'silverticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 20);
                            if (user.claimPacks) {
					            user.claimPacks += 1;
                            } else user.claimPacks = 1;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Silver Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'violetticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 20);
                            if (user.claimPacks) {
					            user.claimPacks += 1;
                            } else user.claimPacks = 1;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Violet Ticket:");
                            Prime.addTicket(user.name, "Violet Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Violet Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'yellowticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
                            if (user.claimPacks) {
					            user.claimPacks += 5;
                            } else user.claimPacks = 5;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Yellow Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'whiteticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 50);
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'greenticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 30);
                            if (user.claimPacks) {
					            user.claimPacks += 2;
                            } else user.claimPacks = 2;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Green Ticket:");
                            Prime.addTicket(user.name, "Green Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Green Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'crystalticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
                            Prime.addTicket(user.name, "Crystal Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Crystal Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'goldticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 50);
                            if (user.claimPacks) {
					            user.claimPacks += 2;
                            } else user.claimPacks = 2;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Gold Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'blackticket':
				        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 100);
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'rubyticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 50);
                            if (user.claimPacks) {
					            user.claimPacks += 5;
                            } else user.claimPacks = 5;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Ruby Ticket:");
                            Prime.addTicket(user.name, "Ruby Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Ruby Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'sapphireticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 100);
                            if (user.claimPacks) {
					            user.claimPacks += 7;
                            } else user.claimPacks = 7;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Sapphire Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'magentaticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
                            Prime.addTicket(user.name, "Magenta Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Magenta Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'rainbowticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 200);
                            if (user.claimPacks) {
					            user.claimPacks += 10;
                            } else user.claimPacks = 10;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Rainbow Ticket:");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
			        case 'emeraldticket':
                        money = setupPrice(match, points, credits);
					    success = runTransaction(money[0], money[1], match, user);;
				    	if (success) {
                            successfulTransaction(match, success, user, room);
				            Economy.writeMoney(user.userid, 100);
                            if (user.claimPacks) {
					            user.claimPacks += 5;
                            } else user.claimPacks = 5;
						    Prime.claimPackPopup(user, "Choose which pack you'd like from the Emerald Ticket:");
                            Prime.addTicket(user.name, "Emerald Ticket");
				            Rooms.get('marketplacestaff').add('|c|~Credit Shop Alert|**' + user.name + " has purchased a Emerald Ticket.**");
                        } else {
                            failedTransaction(user, match, money[0], room);
				    	}
				    	user.shopCache = false;
		    	    	break;
                }
            });
        });    
    },
    rebuy: function(target, room, user) {
        if (!target) return false;
        target = target.trim();
        user.shopCache = '';
        return this.parse('/redeem ' + target);
    },
    loadshop: function(target, room, user, connection) {
        if (!user.hasConsoleAccess(connection)) return this.errorReply("/dev - Access Denied.");
        loadShops();
    },
    
    receipt: 'receipts',
    receipts: function(target, room, user) {
        let options;
        let receipts = fs.readFileSync('logs/transactions.log', 'utf8').split('\n').reverse();
        if (!target) {
            options = 1;
        } else {
            target = target.split(',');
            if (target.length > 1) {
                options = 2;
                for (let t in target) target[t] = toId(target[t]).trim();
            } else {
                target = target[0].trim();
                if (target === 'help') return this.parse('/shop help');
                options = 3;
            }
        }
        if (options === 1) {
            let ownedReceipts = [];
            for (let i in receipts) {
                let parts = receipts[i].split('|');
                if (toId(parts[0]) === user.userid) ownedReceipts.push(parts[5]);
            }
            if (ownedReceipts.length > 0) {
                return this.sendReply("Your receipts: " + ownedReceipts.join(', ') + ". Use /receipt [receiptID] to view details.");
            } else {
                return this.sendReply("You do not have any recent receipts (receipts are deleted after one month).");
            }
        } else if (options === 2) {
            if (target[0] !== 'view' || target.length !== 2) this.sendReply("/receipts view, [user]");
            if (!this.can('ban')) this.errorReply("Access Denied.");
            let name = Users.get(target[1]);
            if (!name) {
                name = target[1];
            } else {
                name = name.name;
            }
            let ownedReceipts = [];
            for (let i in receipts) {
                let parts = receipts[i].split('|');
                if (toId(parts[0]) === target[1]) ownedReceipts.push(parts[5]);
            }
            if (ownedReceipts.length > 0) {
                return this.sendReply("Receipts of " + name + ": " + ownedReceipts.join(', ') + ". Use /receipt [receiptID] to view details.");
            } else {
                return this.sendReply(name + " does not have any recent receipts (receipts are deleted after one month).");
            }
        } else if (options === 3) {
            let receipt = false;
            for (let i in receipts) {
                let parts = receipts[i].split('|');
                if (parts[5] == target) receipt = parts;
            }
            if (!receipt) {
                return this.errorReply("Invalid receipt ID");
            } else {
                if (receipt[0] !== user.userid && !this.can('ban')) return this.sendReply("You can only view your own receipts.");
                let expire = parseInt(receipt[7]);
                expire = +30 - +expire;
                expire = expire.toString();
                return this.sendReplyBox('<center><b>Receipt ' + receipt[5] + '</b></center><br/><b>Name: </b>' + receipt[0] + '<br/><b>Date: </b>' + receipt[1] + '<br/><b>Item: </b>' + receipt[2] + '<br/><b>Price: </b>' + receipt[3] + '<br/><b>Currency: </b>' + receipt[4] + '<br/><b>Remaining ' + receipt[4] + ': </b>' + receipt[6] + '<br/><b>Days until expiry: </b>' + expire);
            }
        } else {
            return this.parse("/shop help");
        }
    },
};