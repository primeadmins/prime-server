'use strict'

/***********************************************
* Inventory.js; User item management by Lights *
***********************************************/

const fs = require('fs');

function loadItemData() {
    try {
        Prime.itemData = JSON.parse(fs.readFileSync('config/itemData.json', 'utf8'));
    } catch (e) {
        Prime.itemData = {};
    }
}
loadItemData();

function sortItemType(property, add) {
	let preList = Object.getOwnPropertyNames(Prime.itemData);
	let itemList = [];
	if (property == 'all') property = 'id';
	for (let i = 0; i < preList.length; i++) {
		let item = Prime.itemData[preList[i]];
		if (item[property] && add) {
			itemList.push(preList[i]);
		} else if (!item[property] && !add) itemList.push(preList[i]);
	}
	return itemList;
}

function checkUser(userid, callback) {
	userid = Alts.getMain(toId(userid));
    Prime.database.all("SELECT * FROM inventory WHERE userid=$userid", {$userid: userid}, function (err, results) {
        if (!results || !results[0]) {
            Prime.database.run("INSERT INTO inventory(userid) VALUES ($userid)", {$userid: userid}, function (err) {
                if (err) return console.log(err);
				return callback(true);
            });
        } else return callback (true);
    });
}

function displayInventory(items, user, self) {
	let itemIds = Object.getOwnPropertyNames(Prime.itemData);
	let ownedItems = [];
	let itemsAmt = {};
	let output = '';
	for (let i in items) {
		if (itemIds.indexOf(items[i].name) != -1) {
			for (let x in itemIds) {
				if (items[i].name == itemIds[x]) {
					ownedItems.push(Prime.itemData[itemIds[x]]);
					if (items[i].num > 1) itemsAmt[itemIds[x]] = toString(items[i].num);
				}
			}
		}
	}
	output += '<center><font size=3><b>Inventory of ' + Prime.nameColor(user, true) + '</b></font></center><br/><center><table border=0>';
	for (let i = 0; i < ownedItems.length; i++) {
		output += '<td><button style="border: 1px #BA85FF solid; border-radius: 3px; box-shadow: 1px 1px 5px;" name="send" value="/viewitem ' + ownedItems[i].id + '"><table border=0><td style="padding: 1px"><center><img src="' + ownedItems[i].image + '" height=24 width=24></center></td><tr>' +
		'<td><center><b>' + ownedItems[i].display + '</b></center></td>';
		if (itemsAmt[ownedItems[i]]) output += '<tr><td><center>' + itemsAmt[ownedItems[i]] + 'x</center></td>';
		output += '</table></button></td>';
		if (i % 8 == 0 && i !== 0) {
			output += '</table><br/><table border=0>';
		}
	}
	return self.sendReplyBox('<div class="infobox-limited">' + output + '</div>');
}

function displayItem(item, self) {
	let itemIds = Object.getOwnPropertyNames(Prime.itemData);
	let foundItem = false;
	let output = '';
	if (itemIds.indexOf(item) !== -1) {
		for (let i in itemIds) {
			if (item == itemIds[i]) foundItem = Prime.itemData[itemIds[i]];
		}
	}
	if (!foundItem) return self.sendReply("Invalid item name.");
	let propertyIds = ['usable', 'consumable', 'holdable', 'key', 'tradeable', 'sellable'];
	let properties = [];
	for (let x in propertyIds) {
		if (foundItem[propertyIds[x]]) properties.push(propertyIds[x]);
	}
	output += '<img src="'+foundItem.image+'" height=24 width=24><br/>' +
	'<b>Name: </b>'+foundItem.display+'<br/>' +
	'<b>Description: </b>'+foundItem.info+'<br/>' +
	'<b>Properties: </b>'+properties.join(', ')+'<br/>';
	if (foundItem.by) output += '<b>Commissioned by: </b>' + foundItem.by;
	return self.sendReplyBox(output);
}

function errReply(item) {
	let output = "You do not own a";
	let vowel = ['a', 'e', 'i', 'o', 'u'];
	if (vowel.indexOf(item.display.charAt(0).toLowerCase) !== -1) output += "n";
	output += " " + item.display + ".";
	return output;
}

let inventory = global.inventory = {
	addItem(userid, item, callback){
		userid = Alts.getMain(toId(userid));
		checkUser(userid, done => {
			Prime.database.all("SELECT "+item+" FROM inventory WHERE userid=$userid", {$userid: userid}, function(err, results) {
				let data = results[0];
				if (Boolean(data) != false) {
					data = +data[item] + +1;
				} else {
					data = 1;
				}
				Prime.database.run("UPDATE inventory SET "+item+"=$data WHERE userid=$userid", {$data: data, $userid: userid}, function(err, done) {
					if (callback) return callback(true)
				});
			});
		});
	},
	removeItem(userid, item, num, callback){
		userid = Alts.getMain(toId(userid));
		checkUser(userid, done => {
			Prime.database.all("SELECT "+item+" FROM inventory WHERE userid=$userid", {$userid: userid}, function(err, results) {
				let data = results[0];
				if (Boolean(data) != false) {
					if (data[item] >= 1) {
						data = (data[item] >= num) ? +data[item] - +num : 0;
					} else data = 0;
				} else data = 0;
				Prime.database.run("UPDATE inventory SET "+item+"=$data WHERE userid=$userid", {$data: data, $userid: userid}, function(err, done) {
					if (callback) return callback(true)
				});
			});
		});
	},
	checkItem(userid, item, callback){
		userid = Alts.getMain(toId(userid));
		checkUser(userid, done => {
        	Prime.database.all("SELECT "+item+" FROM inventory WHERE userid=$userid", {$userid: userid}, function (err, results) {
            	let data = results[0];
            	if (Boolean(data) == false) {
                	return callback(false);
            	} else if (data[item] < 1) {
                	return callback(false);
            	} else {
                	return callback(true);
            	}	
			});
		});
	},
	
};

exports.commands = {
	
	use: function(target, room, user) {
        if (!target) return this.sendReply("/use [itemID], [arg]* - Some items may require an additional piece of information to be used, for example an Avatartoken will require an image to be submitted.");
		let itemList = sortItemType('usable', true);
        let parts = target.split(',');
        for (let i in parts) parts[i] = parts[i].trim();
		parts[0] = Chat.escapeHTML(parts[0]);
        if (itemList.indexOf(parts[0]) === -1) return this.sendReply("Invalid item ID");
        let self = this;
        switch (parts[0]) {
            case 'avatartoken':
                if (!parts[1]) return self.sendReply("You must specify an image"); 
				if (parts.length > 2) return self.sendReply("/use avatartoken, image url");
                inventory.checkItem(user.userid, parts[0], pass => {
					if (pass) {
						Prime.messageSeniorStaff("/raw <center><h4>Custom Avatar</h4>" + user.name + " has redeemed a custom avatar. <img src=" + parts[1] + "><br/><br/><button name='send' value='/ca add "+user.userid+","+parts[1]+"'>Apply</button> <button name='send' value='/declineitem "+user.userid+",avatar'>Decline</button><br/><br/><hr/></center>");
						inventory.removeItem(user.userid, parts[0], 1);
						return self.sendReply("You have redeemed your Avatar Token, a member of staff will apply it shortly!");
					} else return self.sendReply(errReply(Prime.itemData[parts[0]]));
				});
				break;
            case 'icontoken':
                if (!parts[1]) return self.sendReply("You must specify an image"); 
				if (parts.length > 2) return self.sendReply("/use icontoken, image url");
                inventory.checkItem(user.userid, parts[0], pass => {
					if (pass) {
						Prime.messageSeniorStaff("/raw <center><h4>Custom Icon</h4>" + user.name + " has redeemed a Userlist Icon. <img src=" + parts[1] + "><br/><br/><button name='send' value='/icon "+user.userid+","+parts[1]+"'>Apply</button> <button name='send' value='/declineitem "+user.userid+",icon'>Decline</button><br/><br/><hr/></center>");
						inventory.removeItem(user.userid, parts[0], 1);
						return self.sendReply("You have redeemed your Icon Token, a member of staff will apply it shortly!");
					} else return self.sendReply(errReply(Prime.itemData[parts[0]]));
				});
                break;
            case 'colortoken':
                if (!parts[1]) return self.sendReply("You must specify a color"); 
				if (parts.length > 2) return self.sendReply("/use colortoken, hex color");
                inventory.checkItem(user.userid, parts[0], pass => {
					if (pass) {
						Prime.messageSeniorStaff("/raw <center><h4>Custom Color</h4>" + user.name + " has redeemed a Custom name color. <font color='"+parts[1]+"'><b>"+user.name+"</b></font><br/><br/><button name='send' value='/customcolor set "+user.userid+","+parts[1]+"'>Apply</button> <button name='send' value='/declineitem "+user.userid+",color'>Decline</button><br/><br/><hr/></center>");
						inventory.removeItem(user.userid, parts[0], 1);
						return self.sendReply("You have redeemed your Color Token, a member of staff will apply it shortly!");
					} else return self.sendReply(errReply(Prime.itemData[parts[0]]));
				});
                break;
        }
    },
	
	declineitem: function(target, room, user) {
		if (!this.can('rangeban')) return false;
		if (!target) return this.sendReply("You must specify a user.");
		target = target.split(',');
		if (target.length !== 2) return this.sendReply("/declineitem userid, item");
		for (let i in target) target[i] = toId(target[i]).trim();
		if (target[0].length > 19) return this.sendReply("Usernames are not this long.");
		let validItems = ['avatar', 'icon', 'color', 'fix'];
		if (validItems.indexOf(target[1]) === -1) return this.sendReply("Invalid item. Valid items: " + validItems.join(', '));
		Prime.messageSeniorStaff("/raw <center>" + user.name + " has declined " + target[0] + "'s redeemed " + target[1].charAt(0).toUpperCase() + target[1].slice(1) + "<br/><br/><hr/></center>");
		return Users(target[0]).send('|pm|~Server|~|Your ' + target[1].charAt(0).toUpperCase() + target[1].slice(1) + ' was declined.'); 
	},
	
	throw: 'toss',
	toss: function(target, room, user) {
		if (!target) return this.sendReply("/toss [item ID], [quantity] - If no quantity is specified, only 1 will be removed");
		let parts = target.split(',');
		for (let p in parts) parts[p] = toId(parts[p]).trim();
		if (parts.length > 2) return this.sendReply("/toss [item ID], [quantity] - If no quantity is specified, only 1 will be removed");
		let itemList = sortItemType('key', false);
		let item = Chat.escapeHTML(parts[0]);
		let num = (parts.length == 2 && parseInt(parts[1]) != NaN && parseInt(parts[1]) >= 1) ? parseInt(parts[1]) : 1;
		if (itemList.indexOf(item) === -1) return this.sendReply("Invalid item ID");
		let self = this;
		inventory.checkItem(user.userid, item, pass => {
			if (pass) {
				inventory.removeItem(user.userid, item, num);
				return self.sendReply("You have tossed " + toString(num) + " '" + item + "'" + ((num > 1) ? "s." : "."));
			} else return self.sendReply(errReply(Prime.itemData[item]));
		});
	},
	
	transfer: {
		item: function(target, room, user) {
			if (!target) return this.sendReply("/transfer [item] [itemID], [target user]");
			let parts = target.split(',');
			let item = toId(parts[0]).trim();
			item = Chat.escapeHTML(item);
			let targetUser = toId(parts[1]).trim();
			let itemList = sortItemType('tradeable', true);
        	if (itemList.indexOf(item) == -1) return this.sendReply("Invalid item ID");
			inventory.removeItem(user.userid, item, 1);
			inventory.addItem(targetUser, item);
			let name = Prime.itemData[item].display;
			return this.sendReply("You have transferred 1 " + item + " to " + Prime.nameColor(Users.get(targetUser).userid, true) + ".");
		},
		card: function(target, room, user) {
			if (!target) return this.sendReply("/transfer [card] [cardID], [target user]");
			let parts = target.split(',');
			let card = toId(parts[0]).trim();
			let targetUser = toId(parts[1]).trim();
			let canTransfer = removeCard(card, user.userid);
			if (!canTransfer) return this.sendReply("Invalid card ID");
			addCard(targetUser, card);
			Db('points').set(targetUser, getPointTotal(targetUser));
			Db('points').set(user.userid, getPointTotal(user.userid));
			return this.sendReply("You have transferred your " + card + " to " + Prime.nameColor(Users.get(targetUser).userid, true) + ".");
		},/*
		points: function(target, room, user) {
			if (!target) return this.sendReply("/transfer [points] [amount], [target user]");
			let parts = target.split(',');
			let amount = parts[0].trim();
			amount = Math.round(Number(amount));
			let targetUser = toId(parts[1]).trim();
			if (isNaN(amount)) return this.sendReply("Amount must be a number.");
			let self = this;
			Economy.readMoney(user.userid, points => {
				if (amount > points) return self.sendReply("You cannot transfer more points than you currently have.");
				Economy.writeMoney(user.userid, -amount, () => {
					Economy.writeMoney(targetUser, amount, () => {
						return self.sendReply("You have transferred " + amount + " points to " + Prime.nameColor(Users.get(targetUser).userid, true) + ".");
					});
				});
			});
		},*/
		'': 'help',
		help: function(target, room, user) {
			return this.sendReplyBox("/transfer item [itemID], [target user]<br/>" +
			"/transfer card [cardID], [target user]<br/>" +
			"/transfer points [amount], [target user]");
		},
		
	},
	
	give: function(target, room, user) {
	
	},
	
	sell: function(target, room, user) {
		if (!target) return this.sendReply("/sell [item/card]");
	},
	
	trade: function(target, room, user) {
		
	},
    
    inventory: function(target, room, user) {
		if (!this.runBroadcast()) return;
        if (!target) target = user.userid;
        target = toId(target).trim();
        target = Chat.escapeHTML(target);
        let self = this;
        let items = [];
        checkUser(target, done => {
            Prime.database.all("SELECT * FROM inventory WHERE userid=$userid", {$userid: target}, function (err, results) {
                let data = results[0];
                let itemIds = Object.getOwnPropertyNames(results[0]);
                for (let i in itemIds) {
                    if (itemIds[i] !== 'userid' && Boolean(data[itemIds[i]]) != false) items.push({name: itemIds[i], num: data[itemIds[i]]});
                }
                if (items.length >= 1) {
					displayInventory(items, target, self);
					if (this.broadcasting) room.update();
            	} else {
					self.sendReply("This user has no items in their inventory.");
            	}
            });
		});
    },
	
	viewitem: function(target, room, user) {
		if (!this.runBroadcast()) return;
        if (!target || target == 'userid') return this.errorReply("You must specify an item to view.");
		target = toId(target).trim();
        target = Chat.escapeHTML(target);
		let self = this;
		displayItem(target, self);
		if (this.broadcasting) room.update();
	},
	
	spawn: {
		card: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/spawn card [user], [cardID]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/spawn card [user], [cardID]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let card = Chat.escapeHTML(parts[1].trim());
			let cards = require('../config/card-data.js');
			if (!cards.hasOwnProperty(card)) return this.errorReply("Invalid card ID.");
			card = cards[card];
			PrimePSGO.addCard(targetUser, card.title);
			user.popup("You have successfully given " + card.name + " to " + targetUser + ".");
			this.logModCommand(user.name + " gave the card '" + card.name + "' to " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has spawned the card '" + card.name + "' in your inventory.");
		},
		item: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/spawn item [user], [itemID]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/spawn item [user], [itemID]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let item = Chat.escapeHTML(parts[1].trim());
			let itemList = sortItemType('all', true);
			if (itemList.indexOf(item) === -1) return this.errorReply("Invalid item ID");
			inventory.addItem(targetUser, item);
			user.popup("You have successfully given " + item + " to " + targetUser + ".");
			this.logModCommand(user.name + " gave the card '" + item + "' to " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has spawned the card '" + item + "' in your inventory.");
		},
		points: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/spawn points [user], [amount], [reason]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 3) return this.errorReply("/spawn points [user], [amount], [reason]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let amount = parseInt(parts[1].trim());
			if (isNaN(amount)) return this.errorReply("Amount must be a number.");
			let reason = parts[2];
			if (reason.length > 100) return this.errorReply("Reason may not be longer than 100 characters.");
			if (toId(reason).length < 1) return this.errorReply("Please specify a reason to give points.");
			Economy.writeMoney(targetUser, amount, () => {});
			user.popup("You have successfully given " + amount + " points to " + targetUser + ".");
			this.logModCommand(user.name + " gave " + amount + " points to " + targetUser + ".");
			Economy.logTransaction(user.name + " has given " + amount + ((amount === 1) ? " point " : " points ") + " to " + targetUser + ". (Reason: " + reason + ") They now have " + newAmount + (newAmount === 1 ? " buck." : " bucks."));
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has added " + amount + " points to your account.");
		},
		xp: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return false;
			if (!target) return this.errorReply("/spawn xp [user], [amount]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/spawn xp [user], [amount]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let amount = parseInt(parts[1].trim());
			if (isNaN(amount)) return this.errorReply("Amount must be a number.");
			glevel.hardAddXp(targetUser, amount);
			user.popup("You have successfully given " + amount + "XP to " + targetUser + ".");
			this.logModCommand(user.name + " gave " + amount + "XP to " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has added " + amount + "XP to your account.");
		},
	},
	
	remove: {
		card: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/remove card [user], [cardID]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/remove card [user], [cardID]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let card = Chat.escapeHTML(parts[1].trim());
			let cards = require('../config/card-data.js');
			if (!cards.hasOwnProperty(card)) return this.errorReply("Invalid card ID.");
			card = cards[card];
			PrimePSGO.removeCard(targetUser, card.title);
			user.popup("You have successfully taken " + card.name + " from " + targetUser + ".");
			this.logModCommand(user.name + " took the card '" + card.name + "' from " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has taken the card '" + card.name + "' from your inventory.");
		},
		item: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/remove item [user], [itemID]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/remove item [user], [itemID]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let item = Chat.escapeHTML(parts[1].trim());
			let itemList = sortItemType('all', true);
			if (itemList.indexOf(item) === -1) return this.errorReply("Invalid item ID");
			let self = this;
			inventory.checkItem(targetUser, item, results => {
				if (results) {
					inventory.removeItem(item, targetUser);
					user.popup("You have successfully taken " + item + " from " + targetUser + ".");
					self.logModCommand(user.name + " took the card '" + item + "' from " + targetUser + ".");
					if (Users(targetUser)) Users(targetUser).popup(user.name + " has taken the card '" + item + "' from your inventory.");
				} else self.sendReply(targetUser + " does not have this item.");
			});
		},
		points: function(target, room, user) {
			if (!user.can('rangeban')) return false;
			if (!target) return this.errorReply("/remove points [user], [amount]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/remove points [user], [amount]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let amount = parseInt(parts[1].trim());
			if (isNaN(amount)) return this.errorReply("Amount must be a number.");
			Economy.writeMoney(targetUser, -amount, () => {});
			user.popup("You have successfully taken " + amount + " points from " + targetUser + ".");
			this.logModCommand(user.name + " took " + amount + " points from " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has taken " + amount + " points from your account.");
		},
		xp: function(target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return false;
			if (!target) return this.errorReply("/remove xp [user], [amount]");
			let parts = target.split(",").map(p => toId(p));
			if (parts.length !== 2) return this.errorReply("/remove xp [user], [amount]");
			let targetUser = Chat.escapeHTML(parts[0].trim());
			let amount = parseInt(parts[1].trim());
			if (isNaN(amount)) return this.errorReply("Amount must be a number.");
			glevel.hardAddXp(targetUser, -amount);
			user.popup("You have successfully taken " + amount + "XP from " + targetUser + ".");
			this.logModCommand(user.name + " took " + amount + "XP from " + targetUser + ".");
			if (Users(targetUser)) Users(targetUser).popup(user.name + " has taken " + amount + "XP from your account.");
		},
	},
	
	sell: {
		card: function(target, room, user) {
		
		},
		item: function(target, room, user) {
		
		},
	},
};