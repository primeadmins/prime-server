'use strict'

exports.commands = {
	whatsnew: 'serverupdates',
	announcements: 'serverupdates',
	serverupdates: {
        staff: {
			show: 'view',
			view: function (target, room, user) {
				if (!this.runBroadcast()) return;
				if (!this.can('lock')) return this.errorReply("/serverupdates staff - Access Denied");
				if (toId(room.title) !== 'staff') return this.errorReply("This command can only be used in the staff room.");
				if (!Rooms('staff') || !Rooms('staff').news) return this.errorReply("Theres nothing here.");
				if (!Rooms('staff').news && Rooms('staff')) Rooms('staff').news = {};
				let news = Rooms('staff').news;
				if (Object.keys(news).length === 0) return this.sendReply("Theres nothing new going on.");
				return this.sendReplyBox(
					"<center><b>Staff News:</b></center>" +
						Prime.generateNews().join('<hr>')
				);
			},
			delete: function (target, room, user, connection) {
				if (!user.hasConsoleAccess(connection)) return this.errorReply("/serverupdates staff delete - Access Denied");
				if (!target) return this.parse('/help serverupdates');
				if (!Rooms('staff').news) Rooms('staff').news = {};
				let news = Rooms('staff').news;
				if (!news[target]) return this.errorReply("The update you\'re looking for doesn\'t exist.");
				delete news[target];
				Rooms('staff').news = news;
				Rooms('staff').chatRoomData.news = Rooms('staff').news;
				Rooms.global.writeChatRoomData();
				this.privateModCommand(`(${user.name} deleted server announcement titled: ${target}.)`);
			},
			add: function (target, room, user, connection) {
				if (!user.hasConsoleAccess(connection)) return this.errorReply("/serverupdates staff add - Access Denied");
				if (!target) return this.parse('/help serverupdates');
				target = target.split('|');
				for (let u in target) target[u] = target[u].trim();
				if (!target[1]) return this.errorReply("Usage: /serverupdates staff add Title| Message");
				if (!Rooms('staff').news) Rooms('staff').news = {};
				let news = Rooms('staff').news;
				news[target[0]] = {
					desc: target[1],
					posted: Date.now(),
					by: user.name
				};
				Rooms('staff').news = news;
				Rooms('staff').chatRoomData.news = Rooms('staff').news;
				Rooms.global.writeChatRoomData();
				this.privateModCommand(`(${user.name} added server announcement: ${target[1]})`);
			},
			'': 'help',
			help: function (target, room, user) {
				if (!this.runBroadcast()) return;
				let output = "/serverupdates staff - Displays recent server updates. Requires % <br/>" + 
					"/serverupdates staff add [Title] | [Content] - Adds update to list. Requires ~ <br/>" +
					"/serverupdates delete [Title] - Deletes update from recent updates. Requires ~ ";
				if (toId(room.title) !== 'staff') return this.errorReply("This command can only be used in the staff room.");
				this.sendReplyBox(output);
			},
        },
		show: 'view',
		view: function (target, room, user) {
			if (!Rooms('lobby') || !Rooms('lobby').news) return this.errorReply("Theres nothing here. Either something is borkd or theres nothing new. Either way it means Prime sucks.");
			if (!Rooms('lobby').news && Rooms('lobby')) Rooms('lobby').news = {};
			let news = Rooms('lobby').news;
			if (Object.keys(news).length === 0) return this.sendReply("Theres nothing new going on. This place sucks.");
			return this.sendReplyBox(
				"<center><b>Whats New?:</b></center>" +
					Prime.generateNews().join('<hr>')
			);
		},
		delete: function (target, room, user) {
			if (!this.can('rangeban')) return false;
			if (!target) return this.parse('/help serverupdates');
			if (!Rooms('lobby').news) Rooms('lobby').news = {};
			let news = Rooms('lobby').news;
			if (!news[target]) return this.errorReply("The update you\'re looking for doesn\'t exist.");
			delete news[target];
			Rooms('lobby').news = news;
			Rooms('lobby').chatRoomData.news = Rooms('lobby').news;
			Rooms.global.writeChatRoomData();
			this.privateModCommand(`(${user.name} deleted server announcement titled: ${target}.)`);
		},
		add: function (target, room, user) {
			if (!this.can('rangeban')) return false;
			if (!target) return this.parse('/help serverupdates');
			target = target.split('|');
			for (let u in target) target[u] = target[u].trim();
			if (!target[2]) return this.errorReply("Usage: /whatsnew add Title| Message| Type");
            target[2] = toId(target[2]);
            target[2] = target[2].charAt(0).toUpperCase() + target[2].slice(1);
            let typeArr = ['Development', 'Event', 'General', 'Emergency'];
            if (typeArr.indexOf(target[2]) === -1) return this.errorReply("Please specify a valid update type: Development, Event, General, Emergency");
			if (!Rooms('lobby').news) Rooms('lobby').news = {};
			let news = Rooms('lobby').news;
			news[target[0]] = {
				desc: target[1],
				posted: Date.now(),
                type: target[2]
			};
			Rooms('lobby').news = news;
			Rooms('lobby').chatRoomData.news = Rooms('lobby').news;
			Rooms.global.writeChatRoomData();
			this.privateModCommand(`(${user.name} added server announcement: ${target[1]})`);
		},
        enable: 'disable',
		disable: function (target, room, user) {
			let reply = Prime.checkNews(user.userid);
			this.sendReply(`You have ${reply ? 'disabled' : 'enabled'} server updates.`);
		},
		'': 'help',
        help: function (target, room, user) {
            let output = "/serverupdates - Displays recent server updates.<br/>/serverupdates [enable/disable] - Enables/Disables the displaying of updates on connection to Prime.";
            if (this.can('rangeban')) {
                output += "<br/>/serverupdates add [Title] | [Content] | [Type] - Adds update to list.<br/>" +
                "/serverupdates delete [Title] - Deletes update from recent updates.";
            }
            this.sendReplyBox(output);
        },
	},
};