/* Symbol Color Plugin
 * Refactor'd Version
  of jds Colour! */
'use strict';

const fs = require('fs');

function load() {
	fs.readFile('config/staffsymbol.json', 'utf8', function (err, file) {
		if (err) return;
		Prime.staffSymbol = JSON.parse(file);
	});
}
setInterval(function () {
	load();
}, 500);

function updateColor() {
	fs.writeFileSync('config/staffsymbol.json', JSON.stringify(Prime.staffSymbol));

	let newCss = '/* SYMBOLS START */\n';

	for (let name in Prime.staffSymbol) {
		newCss += generateCSS(name, Prime.staffSymbol[name]);
	}
	newCss += '/* SYMBOLS END */\n';

	let file = fs.readFileSync('config/custom.css', 'utf8').split('\n');
	if (~file.indexOf('/* COLORS START */')) file.splice(file.indexOf('/* SYMBOLS START */'), (file.indexOf('/* SYMBOLS END */') - file.indexOf('/* COLORS START */')) + 1);
	fs.writeFileSync('config/custom.css', file.join('\n') + newCss);
	Prime.reloadCSS();
}

function generateCSS(name, color) {
	let css = '';
	let rooms = [];
	name = toId(name);
	for (let room in Rooms.rooms) {
		if (Rooms.rooms[room].id === 'global' || Rooms.rooms[room].type !== 'chat' || Rooms.rooms[room].isPersonal) continue;
		if (!isNaN(Number(Rooms.rooms[room].id.charAt(0)))) continue;
		rooms.push('#' + Rooms.rooms[room].id + '-userlist-user-' + name + '  em.group');
	}
	css = rooms.join(', ');
	css += '{\ncolor: ' + color + ' !important;\n}\n';
	css += 'color: ' + color + ' !important;\n}\n';
	return css;
}

exports.commands = {
	staffsymbol: function (target, room, user) {
		if (!this.can('rangeban')) return false;
		target = target.split(',');
		for (let u in target) target[u] = target[u].trim();
		if (!target[1]) return this.parse('/help staffsymbol');
		if (toId(target[0]).length > 19) return this.errorReply("Usernames are not this long...");
		if (target[1] === 'disable'|| target[1] === 'enable') {
            if (target[1] === 'disable') {
			    if (!Prime.staffSymbol[toId(target[0])]) return this.errorReply('/staff symbol - ' + target[0] + ' does not have a staff symbol.');
			    delete Prime.staffSymbol[toId(target[0])];
			    updateColor();
			    this.sendReply("You removed " + target[0] + "'s staff symbol.");
			    Rooms('staff').add(user.name + " removed " + target[0] + "'s staff symbol.").update();
			    this.privateModCommand("(" + target[0] + "'s staff symbol was removed by " + user.name + ".)");
			    if (Users(target[0]) && Users(target[0]).connected) Users(target[0]).popup(user.name + " removed your staff symbol.");
           }
           if (target[1] === 'enable') {
                this.sendReply("|raw|You have given <b>" + Tools.escapeHTML(target[0]) + "</b> a staff symbol.");
			    Rooms('staff').add(user.name + " gave " + target[0] + " a staff symbol.").update();
		        this.privateModCommand("(" + target[0] + " has recieved a staff symbol from " + user.name + ".)");
		        Prime.staffSymbol[toId(target[0])] = '#cc0033';
		        updateColor();
           }
		} else {
           this.parse('/help staffsymbol'); 
        }
        
	},
	staffsymbolhelp: ["Commands Include:",
				"/staffsymbol [user], enable - Gives [user] a staff symbol",
				"/staffsymbol [user], disable - Deletes a user's staff symbol"],
};