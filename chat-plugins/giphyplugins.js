'use strict';

let http = require('http');

exports.commands = {

// Animal command by RoboPhill
	animal: 'animals',
	animals: function(target, room, user) {
		if (!target) return this.parse('/help animals')
		let tarId = toId(target);
		let validTargets = ['cat', 'otter', 'dog', 'bunny', 'pokemon', 'kitten', 'puppy'];
		if (room.id === 'lobby') return this.errorReply("This command cannot be broadcasted in the Lobby.");
		if (!validTargets.includes(tarId)) return this.parse('/help animals');
		let self = this;
		let reqOpt = {
			hostname: 'api.giphy.com', // Do not change this
			path: '/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=' + tarId,
			method: 'GET',
		};
		let req = http.request(reqOpt, function(res) {
			res.on('data', function(chunk) {
				try {
					let data = JSON.parse(chunk);
					let output = '<center><img src="' + data.data["image_url"] + '" width="50%"></center>';
					if (!self.runBroadcast()) return;
					if (data.data["image_url"] === undefined) {
						self.errorReply("ERROR CODE 404: No images found!");
						return room.update();
					} else {
						self.sendReplyBox(output);
						return room.update();
					}
				} catch (e) {
					self.errorReply("ERROR CODE 503: Giphy is unavaliable right now. Try again later.");
					return room.update();
				}
			});
		});
		req.end();
	},
	animalshelp: ['|html|<div class="infobox">Animals Plugin by <font color="hotpink">DarkNightSkies</font> & <font color="deeppink">Kyv.n(♥)</font>' +
		'<ul><li>/animals cat - Displays a cat.</li>' +
		'<li>/animals kitten - Displays a kitten.</li>' +
		'<li>/animals dog - Displays a dog.</li>' +
		'<li>/animals puppy - Displays a puppy.</li>' +
		'<li>/animals bunny - Displays a bunny.</li>' +
		'<li>/animals otter - Displays an otter.</li>' +
		'<li>/animals pokemon - Displays a pokemon.</li>' +
		'<li>/animals help - Displays this help box.</li></ul></div>'
],
	gif: 'giphy',
	giphy: function(target, room, user) {
		if (!target) return this.parse('/help giphy')
		let tarId = toId(target);
		if (room.id === 'lobby') return this.errorReply("This command cannot be broadcasted in the Lobby.");
		let self = this;
		let reqOpt = {
			hostname: 'api.giphy.com', // Do not change this
			path: '/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=' + tarId,
			method: 'GET',
		};
		let req = http.request(reqOpt, function(res) {
			res.on('data', function(chunk) {
				try {
					let data = JSON.parse(chunk);
					let output = '<center><img src="' + data.data["image_url"] + '" width="50%"></center>';
					if (!self.runBroadcast()) return;
					if (data.data["image_url"] === undefined) {
						self.errorReply("ERROR CODE 404: No images found!");
						return room.update();
					} else {
						self.sendReplyBox(output);
						return room.update();
					}
				} catch (e) {
					self.errorReply("ERROR CODE 503: Giphy is unavaliable right now. Try again later.");
					return room.update();
				}
			});
		});
		req.end();
	},
	giphyhelp: ['|html|<div class="infobox">Giphy Plugin by <font color="deeppink">Kyv.n(♥)</font><br>' +
		'- /giphy [word] - Displays a random Gif with your chosen keyword.</div>'
],
};