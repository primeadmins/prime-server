'use strict';

const fs = require('fs');
const moment = require('moment');

let serverIp = '149.56.131.9';

let Economy = global.Economy = {
	readMoney: function (userid, callback) {
		if (!callback) return false;
		userid = Alts.getMain(toId(userid));
		Prime.database.all("SELECT * FROM users WHERE userid=$userid", {$userid: userid}, function (err, rows) {
			let points = ((rows[0] && rows[0].bucks) ? rows[0].bucks : 0);
			callback(points);
		});
	},
	writeMoney: function (userid, amount, callback) {
		userid = Alts.getMain(toId(userid));
		Prime.database.all("SELECT * FROM users WHERE userid=$userid", {$userid: userid}, function (err, rows) {
			if (rows.length < 1) {
				Prime.database.run("INSERT INTO users(userid, bucks) VALUES ($userid, $amount)", {$userid: userid, $amount: amount}, function (err) {
					if (callback) return callback();
				});
			} else {
				amount += rows[0].bucks;
				let points = amount;
				Prime.database.run("UPDATE users SET bucks=$amount WHERE userid=$userid", {$amount: points, $userid: userid}, function (err) {
					if (callback) return callback();
				});
			}
		});
	},
	
	logTransaction: function (message) {
		if (!message) return false;
		fs.appendFile('logs/point.log', '[' + new Date().toUTCString() + '] ' + message + '\n');
	},

	logDice: function (message) {
		if (!message) return false;
		fs.appendFile('logs/dice.log', '[' + new Date().toUTCString() + '] ' + message + '\n');
	},
};

exports.commands = {
	
    wallet: 'viewpoints',
    atm: 'viewpoints',
    checkpoints: 'viewpoints',
	viewpoints: function (target, room, user) {
		if (!target) target = user.name;
		if (!this.runBroadcast()) return;
		let userid = toId(target);
		if (userid.length < 1) return this.sendReply("/points - Please specify a user.");
		if (userid.length > 19) return this.sendReply("/points - [user] can't be longer than 19 characters.");

		Economy.readMoney(userid, money => {
			this.sendReplyBox(Chat.escapeHTML(target) + " has " + money + ((money === 1) ? " point." : " points."));
			if (this.broadcasting) room.update();
		});
	},

	gp: 'givepoints',
	givepoints: function (target, room, user) {
		if (!user.can('rangeban')) return false;
		if (!target) return this.sendReply("Usage: /givepoints [user], [amount]");
		let splitTarget = target.split(',');
		if (!splitTarget[1]) return this.sendReply("Usage: /givepoints [user], [amount]");
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/givepoints - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/givepoints - [user] can't be longer than 19 characters");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/givepoints - [amount] must be a number.");
		if (amount > 100) return this.sendReply("/givepoints - You can't give more than 100 points at a time.");
		if (amount < 1) return this.sendReply("/givepoints - You can't give less than one point.");

		Economy.writeMoney(targetUser, amount);
		this.sendReply(Chat.escapeHTML(targetUser) + " has received " + amount + ((amount === 1) ? " point." : " points."));
		Economy.logTransaction(user.name + " has given " + amount + ((amount === 1) ? " point " : " points ") + " to " + targetUser);
	},

	takepoints: function (target, room, user) {
		if (!user.can('rangeban')) return false;
		if (!target) return this.sendReply("Usage: /takepoints [user], [amount]");
		let splitTarget = target.split(',');
		if (!splitTarget[1]) return this.sendReply("Usage: /takepoints [user], [amount]");
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/takepoints - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/takepoints - [user] can't be longer than 19 characters");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/takepoints - [amount] must be a number.");
		if (amount > 100) return this.sendReply("/takepoints - You can't take more than 100 points at a time.");
		if (amount < 1) return this.sendReply("/takepoints - You can't take less than one points.");

		Economy.writeMoney(targetUser, -amount);
		this.sendReply("You removed " + amount + ((amount === 1) ? " point " : " points ") + " from " + Chat.escapeHTML(targetUser));
		Economy.logTransaction(user.name + " has taken " + amount + ((amount === 1) ? " point " : " points ") + " from " + targetUser);
	},

	transferpoints: function (target, room, user) {
		if (!target) return this.sendReply("Usage: /transferpoints [user], [amount]");
		let splitTarget = target.split(',');
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();
		if (!splitTarget[1]) return this.sendReply("Usage: /transferpoints [user], [amount]");

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/transferpoints - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/transferpoints - [user] can't be longer than 19 characters.");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/transferpoints - [amount] must be a number.");
		if (amount > 100) return this.sendReply("/transferpoints - You can't transfer more than 100 points at a time.");
		if (amount < 1) return this.sendReply("/transferpoints - You can't transfer less than one point.");

		Economy.readMoney(user.userid, money => {
			if (money < amount) return this.sendReply("/transferpoints - You can't transfer more points than you have.");
			Economy.writeMoney(user.userid, -amount, () => {
				Economy.writeMoney(targetUser, amount, () => {
					this.sendReply("You've sent " + amount + ((amount === 1) ? " point " : " points ") + " to " + targetUser);
					Economy.logTransaction(user.name + " has transfered " + amount + ((amount === 1) ? " point " : " points ") + " to " + targetUser);
					if (Users.getExact(targetUser) && Users.getExact(targetUser)) Users.getExact(targetUser).popup(user.name + " has sent you " + amount + ((amount === 1) ? " point." : " points."));
				});
			});
		});
	},

	pointlog: function (target, room, user) {
		//if (!this.can('bucks')) return false;
		if (!target) return this.sendReply("Usage: /pointlog [number] to view the last [number] lines OR /pointlog [text] to search for the last 50 lines containint [text].");
		let word = false;
		if (isNaN(Number(target))) word = true;
		let lines = fs.readFileSync('logs/point.log', 'utf8').split('\n').reverse();
		let output = '';
		let count = 0;
		let regex = new RegExp(target.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "gi");

		if (word) {
			output += 'Displaying last 50 lines containing "' + target + '":\n';
			for (let line in lines) {
				if (count >= 50) break;
				if (!~lines[line].search(regex)) continue;
				output += lines[line] + '\n';
				count++;
			}
		} else {
			if (target > 100) target = 100;
			output = lines.slice(0, (lines.length > target ? target : lines.length));
			output.unshift("Displaying the last " + (lines.length > target ? target : lines.length) + " lines:");
			output = output.join('\n');
		}
		user.popup("|wide|" + output);
	},

	roomshop: 'leagueshop',
	leagueshop: function (target, room, user) {
		if (!room.isLeague) return this.sendReply('/leagueshop - This room is not a league.');
		if (!room.shop) return this.sendReply('/leagueshop - This room does not have a shop, purchase one with /redeem leagueshop, ' + room.title);
		if (!room.founder) return this.sendReply('/leagueshop - league shops require a room founder.');
		if (!room.shopList) room.shopList = [];
		if (!target) target = '';

		let cmdParts = target.split(' ');
		let cmd = cmdParts.shift().trim().toLowerCase();
		let params = cmdParts.join(' ').split(',').map(function (param) { return param.trim(); });
		let item = params[0];

		switch (cmd) {
		case 'list':
		case 'view':
		default:
			if (!this.runBroadcast()) return;
			if (room.shopList.length < 1) return this.sendReplyBox('<center><b><u>This shop has no items!</u></b></center>');
			let output = '<center><h4><b><u><font color="#24678d">' + Chat.escapeHTML(room.title) + '\'s Shop</font></u></b></h4><table border="1" cellspacing ="0" cellpadding="3"><tr><th>Item</th><th>Description</th><th>Price</th></tr>';
			for (let u in room.shopList) {
				if (!room.shop[room.shopList[u]] || !room.shop[room.shopList[u]].name || !room.shop[room.shopList[u]].description || !room.shop[room.shopList[u]].price) continue;
				output += '<tr><td><button name="send" value="/leagueshop redeem ' + Chat.escapeHTML(room.shopList[u]) + '" >' + Chat.escapeHTML(room.shop[room.shopList[u]].name) +
				'</button></td><td>' + Chat.escapeHTML(room.shop[room.shopList[u]].description.toString()) + '</td><td>' + room.shop[room.shopList[u]].price + '</td></tr>';
			}
			output += '</center><br />';
			this.sendReplyBox(output);
			break;
		case 'add':
			if (!user.can('roommod', null, room)) return this.sendReply('/leagueshop - Access denied.');
			if (params.length < 3) return this.sendReply('Usage: /leagueshop add [item name], [description], [price]');
			if (!room.shopList) room.shopList = [];
			let name = params.shift();
			let description = params.shift();
			let price = Number(params.shift());
			if (isNaN(price)) return this.sendReply('Usage: /leagueshop add [item name], [description], [price]');
			room.shop[toId(name)] = {};
			room.shop[toId(name)].name = name;
			room.shop[toId(name)].description = description;
			room.shop[toId(name)].price = price;
			room.shopList.push(toId(name));
			room.chatRoomData.shop = room.shop;
			room.chatRoomData.shopList = room.shopList;
			Rooms.global.writeChatRoomData();
			this.sendReply('Added "' + name + '" to the league shop for ' + price + ' ' + ((price === 1) ? " point." : " points.") + '.');
			break;
		case 'remove':
		case 'rem':
		case 'del':
		case 'delete':
			if (!user.can('roommod', null, room)) return this.sendReply('/leagueshop - Access denied.');
			if (params.length < 1) return this.sendReply('Usage: /leagueshop delete [item name]');
			if (!room.shop[toId(item)]) return this.sendReply('/leagueshop - Item "' + item + '" not found.');
			delete room.shop[toId(item)];
			delete room.chatRoomData.shop[toId(item)];
			let index = room.shopList.indexOf(toId(item));
			if (index > -1) {
				room.shopList.splice(index, 1);
				room.chatRoomData.shopList.splice(index, 1);
			}
			Rooms.global.writeChatRoomData();
			this.sendReply('Removed "' + item + '" from the league shop.');
			break;
		case 'redeem':
			if (params.length < 1) return this.sendReply('Usage: /leagueshop redeem [item name]');
			if (!room.shop[toId(item)]) return this.sendReply('/leagueshop - Item "' + item + '" not found.');
			Economy.readMoney(user.userid, money => {
				if (money < room.shop[toId(item)].price) return this.sendReply('You don\'t have enough points to purchase a ' + item + '. You need ' + ((money - room.shop[toId(item)].price) * -1) + ' more points.');
				if (!room.shopBank) room.shopBank = room.founder;
				this.parse('/transferpoints ' + room.shopBank + ',' + room.shop[toId(item)].price);
				fs.appendFile('logs/leagueshop_' + room.id + '.txt', '[' + new Date().toJSON() + '] ' + user.name + ' has purchased a ' +
					room.shop[toId(item)].name + ' for ' + room.shop[toId(item)].price + ' ' + ((room.shop[toId(item)].price === 1) ? " point." : " points.") + '.\n'
				);
				room.add(user.name + ' has purchased a ' + room.shop[toId(item)].name + ' for ' + room.shop[toId(item)].price + ' ' + ((room.shop[toId(item)].price === 1) ? " point." : " points.") + '.');
				room.update();
			});
			break;
		case 'help':
			if (!this.runBroadcast()) return;
			this.sendReplyBox('The following is a list of league shop commands: <br />' +
				'/leagueshop view/list - Shows a complete list of shop items.`<br />' +
				'/leagueshop add [item name], [description], [price] - Adds an item to the shop.<br />' +
				'/leagueshop delete/remove [item name] - Removes an item from the shop.<br />' +
				'/leagueshop redeem [item name] - Redeems an item from the shop.<br />' +
				'/leagueshop viewlog [number of lines OR word to search for] - Views the last 15 lines in the shop log.<br />' +
				'/leagueshop bank [username] - Sets the room bank to [username]. The room bank receives all points from purchases in the shop.'
			);
			break;
		case 'setbank':
		case 'bank':
			if (user.userid !== room.founder && !user.can('rangeban')) return this.sendReply('/leagueshop - Access denied.');
			if (params.length < 1) return this.sendReply('Usage: /leagueshop bank [username]');
			let bank = params.shift();
			room.shopBank = toId(bank);
			room.chatRoomData.shopBank = room.shopBank;
			Rooms.global.writeChatRoomData();
			this.sendReply('The room bank is now set to ' + room.shopBank);
			break;
		case 'log':
		case 'viewlog':
			if (!user.can('roommod', null, room)) return this.sendReply('/leagueshop - Access denied.');
			target = params.shift();
			let lines = 0;
			if (!target.match('[^0-9]')) {
				lines = parseInt(target || 15);
				if (lines > 100) lines = 100;
			}
			let filename = 'logs/leagueshop_' + room.id + '.txt';
			let command = 'tail -' + lines + ' ' + filename;
			let grepLimit = 100;
			if (!lines || lines < 0) { // searching for a word instead
				if (target.match(/^["'].+["']$/)) target = target.substring(1, target.length - 1);
				command = "awk '{print NR,$0}' " + filename + " | sort -nr | cut -d' ' -f2- | grep -m" + grepLimit +
					" -i '" + target.replace(/\\/g, '\\\\\\\\').replace(/["'`]/g, '\'\\$&\'').replace(/[\{\}\[\]\(\)\$\^\.\?\+\-\*]/g, '[$&]') + "'";
			}

			require('child_process').exec(command, function (error, stdout, stderr) {
				if (error && stderr) {
					user.popup('/leagueshop viewlog erred - the shop log does not support Windows');
					console.log('/leagueshop viewlog error: ' + error);
					return false;
				}
				if (lines) {
					if (!stdout) {
						user.popup('The log is empty.');
					} else {
						user.popup('Displaying the last ' + lines + ' lines of shop purchases:\n\n' + stdout);
					}
				} else {
					if (!stdout) {
						user.popup('No purchases containing "' + target + '" were found.');
					} else {
						user.popup('Displaying the last ' + grepLimit + ' logged purchases containing "' + target + '":\n\n' + stdout);
					}
				}
			});
			break;
		}
	},

	points: function (target, room, user) {
		if (!this.runBroadcast()) return;

		Prime.database.all("SELECT SUM(bucks) FROM users;", (err, rows) => {
			let totalBucks = rows[0]['SUM(bucks)'];
			Prime.database.all("SELECT userid, SUM(bucks) AS total FROM users GROUP BY bucks HAVING TOTAL > 0;", (err, rows) => {
				let userCount = rows.length;
				Prime.database.all("SELECT * FROM users ORDER BY bucks DESC LIMIT 1;", (err, rows) => {
					let richestUser = rows[0].userid;
					let richestUserMoney = rows[0].bucks;
					if (Users.getExact(richestUser)) richestUser = Users.getExact(richestUser).name;
					Prime.database.all("SELECT AVG(bucks) FROM users WHERE bucks > 0;", (err, rows) => {
						let averageBucks = rows[0]['AVG(bucks)'];

						this.sendReplyBox("The richest user is currently <b><font color=#24678d>" + Chat.escapeHTML(richestUser) + "</font></b> with <b><font color=#24678d>" +
							richestUserMoney + "</font></b> points.</font></b><br />There is a total of <b><font color=#24678d>" +
							userCount + "</font></b> users with at least one point.<br /> The average user has " +
							"<b><font color=#24678d>" + Math.round(averageBucks) + "</font></b> points.<br /> There is a total of <b><font color=#24678d>" +
							totalBucks + "</font></b> points in the economy."
						);
						room.update();
					});
				});
			});
		});
	},

	customsymbol: function (target, room, user) {
		let bannedSymbols = ['!', '|', '‽', '\u2030', '\u534D', '\u5350', '\u223C'];
		for (let u in Config.groups) if (Config.groups[u].symbol) bannedSymbols.push(Config.groups[u].symbol);
		let userid = user.userid;
		glevel.readXp(userid, xp => {
			let levelPass = 0;
			if (xp >= 770) levelPass = 1;
			if (!user.canCustomSymbol && !user.can('vip') && levelPass === 0) return this.sendReply('You need to redeem this item from the points shop to use.');
			if (!target || target.length > 1) return this.sendReply('/customsymbol [symbol] - changes your symbol (usergroup) to the specified symbol. The symbol can only be one character in length.');
			if (target.match(/([a-zA-Z ^0-9])/g) || bannedSymbols.indexOf(target) >= 0) {
				return this.sendReply('This symbol is banned.');
			}
			user.customSymbol = target;
			user.updateIdentity();
			user.canCustomSymbol = false;
			this.sendReply('Your symbol is now ' + target + '. It will be saved until you log off for more than an hour, or the server restarts. You can remove it with /resetsymbol');
		});
	},

	removesymbol: 'resetsymbol',
	resetsymbol: function (target, room, user) {
		//if (!user.hasCustomSymbol) return this.sendReply("You don't have a custom symbol!");
		delete user.customSymbol;
		user.updateIdentity();
		this.sendReply('Your symbol has been removed.');
	},
};