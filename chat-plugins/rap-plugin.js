'use strict';

exports.commands = {
    rap: {
    	    timer: function (target, room, user) {
            if (room.id !== 'serverevents') return this.errorReply('This command can only be used in the Server Events Room.');
	        if (!this.runBroadcast()) return;
	        if (!target) return  this.parse('/help rap');
	        room.add('|c|~Rap Battle Manager|' + target + ' You have 7 minutes remaining to prepare your Bars!!');
	        let self = room;
	        setTimeout(function() {
	            room.update();
	            self.add('|c|~Rap Battle Manager|' + target + ' Your time is Up!!! Present your bars or Choke!!');
	            
	        }, 420000);
    	        
    	    },
    	    
    	    topic: function (target, room, user) {
            if (room.id !== 'serverevents') return this.errorReply('This command can only be used in the Server Events Room.');
	        if (!this.runBroadcast()) return;
	        let topic = ['Filler',
	                    'Heat - Bring the heat with sick rhymes and a tight flow.',
	                    'Bars - Spit those illest bars and wackest of rhymes.',
	                    'Pokemon - Battle it out in a pokemon themed rap off.',
	                    'Roast - Go all out on your opponent and make \'em choke.'];
	       let random = ~~(Math.random() * 5);
	       let chosenTop = topic[random];
	        room.add('|c|~Rap Battle Manager|**The topic of todays battle is:** ``' + chosenTop + '`` **Good Luck!**');
    	        
    	    },
    	    
    	   pokemon: function (target, room, user) {
           if (room.id !== 'serverevents') return this.errorReply('This command can only be used in the Server Events Room.');
	       if (!this.runBroadcast()) return;
	       let p1random = ~~(Math.random() * 720);
	       let p2random = ~~(Math.random() * 720);
	       
	       let self = room;
	       let Self = this;
	        setTimeout(function() {
	            room.update();
	            self.add('|c|~Rap Battle Manager|**Player 1\'s pokemon is:**');
	            Self.parse('!data ' + p1random);
	            
	        }, 5000);
	        
	        setTimeout(function() {
	            room.update();
	            self.add('|c|~Rap Battle Manager|**Player 2\'s pokemon is:**');
	            Self.parse('!data ' + p2random);
	            
	        }, 5000);
    	        
    	    },
    	    
    	   help: function (target, room, user) {
           if (room.id !== 'serverevents') return this.errorReply('This command can only be used in the Server Events Room.');
	       if (!this.runBroadcast()) return;
	       this.parse('/help rap');
    	   },
    },
    raphelp: ['|html|Rap Plugin for Prime by Murdering<br>' +
             '- /rap timer [user] - Adds a choke timer for users when writing raps.<br>' +
             '- /rap topic - Picks a topic for the rap battle.Battle.<br>' + 
             '- /rap pokemon - Chooses random pokemon for the players for the battle. [Pokemon only].<br>' + 
             '- /rap help - Displays this.']
};