'use strict'
/***************************************************
 * Profile Level VIP System by Lights and RoboPhill*
 **************************************************/
 
function levelPack(userid, room) {
	let n = Math.floor(Math.random() * Prime.claimPacks.length);
	let randPack = Prime.claimPacks[n];
	if (!Users.userPacks[userid]) Users.userPacks[userid] = [];
	Users.userPacks[userid].push(toId(randPack));
	Users.get(userid).connections[0].sendTo(room.id,
		'|raw| You have receieved a ' + randPack + ' pack from levelling up. You have until the server restarts to open your pack. ' +
		'Use <button name="send" value="/openpack ' + randPack + '"><b>/openpack ' + randPack + '</b></button> to open your pack.');
}

 
let glevel = global.glevel = {

    writeXp: function(userid, xp) {
        userid = Alts.getMain(toId(userid));
        Prime.database.run('UPDATE users SET XP ="'+xp+'" WHERE userid=$userid', {$userid: userid}, function(err){
            if (err) return //console.log(err);
        });
	},
    hardAddXp: function(userid, xp) {
        userid = Alts.getMain(toId(userid));
        glevel.readXp(userid, current => {
            xp = (+xp + +current);
            Prime.database.run('UPDATE users SET XP ="'+xp+'" WHERE userid=$userid', {$userid: userid}, function(err){
                if (err) return //console.log(err);
            });
        });
    },
    readXp: function(userid, callback) {
        userid = Alts.getMain(toId(userid));
        Prime.database.all('SELECT * FROM users WHERE userid=$userid', {$userid: userid}, function(err, results){
            if (results.length < 1) {
                Prime.database.run("INSERT INTO users(userid) VALUES ($userid)", {$userid: userid}, function(err){
                    if (err) return //console.log(err);
                });
            }
        });
        Prime.database.all('SELECT XP FROM users WHERE userid=$userid', {$userid: userid}, function(err, results){
            if(err) return //console.log(err);
            let xp;
            if (typeof results[0] === 'undefined') {
                xp = 0;
            } else {
                xp = results[0].XP
            }
            return callback(xp);
        });
    },
    
    addXp: function(userid, xp, xpAdd, callback) {
        userid = Users.get(userid);
    //get modifiers
        glevel.getLevel(xp, level => {
            let levelMod;
            if (level < 5) {
                levelMod = 1;
            } else if (level >= 5 && level < 10) {
                levelMod = 1.6;
            } else if (level >= 10 && level < 15) {
                levelMod = 2.2;
            } else if (level >= 15 && level < 20) {
                levelMod = 2.9;
            } else if (level >= 20 && level < 25) {
                levelMod = 3.7;
            } else if (level >= 25 && level < 30) {
                levelMod = 4.6;
            } else if (level >= 30 && level < 35) {
                levelMod = 5.6;
            } else if (level >= 35 && level < 40) {
                levelMod = 6.7;
            } else {
                levelMod = 0;
            }
            let double = false;
            inventory.checkItem(userid, 'mysteryflute', flute => {
                if (userid.doublexp) {
                    let current = new Date() / 1000;
                    current = Math.floor(current);
                    if ((+current - +userid.doublexp) >= 86400) {
                        userid.doublexp = false;
                    } else double = true;
                }
    //get random xp added
                let rand = Math.floor(Math.random() * 15);
                let posNeg = Math.random();
                posNeg = ((Math.round(posNeg) === 1) ? -1 : 1);
                let xpRand1 = Math.floor((rand * posNeg) * .1 );
                let xpRand = (+xpRand1 + +xpAdd);
    //apply modifiers
                if (Prime.xpmod !== 1) xpRand = (+xpRand * +Prime.xpmod);
                if (double) xpRand = (xpRand * 2);
                if (flute) xpRand = (xpRand * 1.1);
                xpRand = (xpRand * levelMod);
                if (xpRand < 1) xpRand = 1;
    //add XP and return needed values
                xp = Math.floor(xp + xpRand);
                xpRand = Math.floor(xpRand);
                let xpGroup = [xp, xpRand, double, flute];
                return callback(xpGroup);
            });
        });
    },
    
    updateXp: function(userid, xpAdd, room, self) {
        userid = Alts.getMain(toId(userid));
	    var targetUser = Users.getExact(userid) ? Users.getExact(userid).name : userid;
	    var getUser = Users.get(targetUser);
        glevel.readXp(userid, xp => {
            let oldXp = xp;
            glevel.addXp(userid, xp, xpAdd, xpGroup => {
                xp = xpGroup[0];
                let xpRand = xpGroup[1];
                glevel.writeXp(userid, xp);
                if (typeof Users.get(userid).connections[0] !== 'undefined') {
                    glevel.getLevel(xp, level => {
                        Users.get(userid).connections[0].sendTo(room.id, 'You have gained ' + ((Prime.xpmod !== 1) ? ' [Serverwide ' + Prime.xpmod + 'x XP' + ((Prime.xpmodreason) ? ' (' + Prime.xpmodreason + ')]' : ']') : '') + xpRand + ' XP.' + ((xpGroup[2]) ? ' [Double XP Voucher]' : '') + ((xpGroup[3]) ? ' [Mystery Flute Bonus]' : '')); 
                        glevel.getLevel(oldXp, oldLevel => {
                            if (oldLevel < level) {
                                let rewardLevel;
                                if (level === 1) {
                                    rewardLevel = '-Immunity to the anti-spammer filter.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 3) {
                                    rewardLevel = 'Customize `About Me` on profile with /profile edit.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 5) {
                                    rewardLevel = '-Unlimited access to /customsymbol.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 8) {
                                    rewardLevel = '-Customize Pokemon on profile with /profile edit<br/>-A random card pack..';
                                    levelPack(userid, room);
                                } else if (level === 10) {
                                    rewardLevel = '-Unlimited access to /customavatar.<br/>-Customize background color on profile with /profile edit.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 12) {
                                    rewardLevel = '-Customize Primary and Secondary colors on profile with /profile edit.<br/>A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 13) {
                                    rewardLevel = '-Unlimited access to /customcolor.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 17) {
                                    rewardLevel = '-Unlimited access to /icon.<br/>-A random card pack.';
                                    levelPack(userid, room);
                                } else if (level === 20) {
                                    rewardLevel = '-Unlimited access to /title.<br/>-2 random card packs.';
                                    levelPack(userid, room);
                                    levelPack(userid, room);
                                } else if (level === 25) {
                                    rewardLevel = '-2 random card packs.';
                                    levelPack(userid, room);
                                    levelPack(userid, room);
                                } else if (level === 30) {
                                    rewardLevel = '-1.3x points gained from tours.<br/>-2 random card packs.';
                                    levelPack(userid, room);
                                    levelPack(userid, room);
                                } else if (level > 20 && level !== 25 && level !== 30){
                                    rewardLevel = '-2 random card packs.';
                                    levelPack(userid, room);
                                    levelPack(userid, room);
                                } else {
                                    rewardLevel = '-A random card pack.';
                                    levelPack(userid, room);
                                }
				    			room.update();
                                Users.get(userid).connections[0].sendTo(room.id, '|html|<center><font size=4><b><i>Level Up!</i></b></font><br/>' + 
                                'You have reached level ' + level + ', this will award you the following:<br/>' + 
                                '<b>' + rewardLevel + '</b></center>');
                            }
                        });
                    });
                }
            });
        });
    },
    getProfileLevel: function(userid, callback) {
        glevel.readXp(userid, xp => {
            glevel.getLevel(xp, level => {
                let returnedValue = [level, xp];
                return callback(returnedValue);
            });
        });
    },
    
    getLevel: function(xp, callback) {
        let arr = [0, 100, 190, 290, 480, 770, 1100, 1500, 2070, 2730, 3550, 4460, 5550, 6740, 8120, 9630, 11370, 13290, 15520, 18050, 21060, 24560, 28310, 32720, 37290, 42300, 47440, 52690, 58100, 63600, 69250, 75070, 81170, 87470, 93970, 100810, 107890, 115270, 122960, 131080, 140080];
        let level;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < xp) level = i;
        }
        if (callback) {
            callback(level);
        }
    },
};

exports.commands = {
    globalresetxp: function(target, room, user) {  
        if (target !== 'xkybzz') return false;
        Prime.database.all("SELECT userid FROM users", function (err, rows) {
            for (let j = 0; j < rows.length; j++) {
                let userid = rows[j];
                let xp = 0;
                Prime.database.run("UPDATE users SET XP='" + xp + "' WHERE UserId='"+userid+"'");
            }
        });
        this.add('[XP Monitor]' + user.name + ' has reset all user XP.');
    },
    confirmresetxp: 'resetxp',
    resetxp: function(target, room, user, cmd) {
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
        if (user.userid !== 'lights' && user.userid !== 'robophill') return false;
        let username = targetUser.name.charAt(0).toUpperCase() + targetUser.name.slice(1);;
        let userId = toId(targetUser.userid);
        if (cmd === "resetxp") {
			return this.popupReply('|html|<center><button name="send" value="/confirmresetxp" style="background-color:red;height:65px;width:150px"><b><font color="white" size=3>Confirm XP reset of ' + username + '; this is only to be used in emergencies, cannot be undone!</font></b></button>');
		}
        Prime.database.all("SELECT '" + userId + "' FROM users", function (err, rows) {
            let xp = 0;
            Prime.database.run("UPDATE users SET XP='" + xp + "' WHERE UserId='"+userId+"'");
        });
        targetUser.popup('Your XP was reset by an Administrator. This cannot be undone and nobody below the rank of Administrator can assist you or answer questions about this.');
        this.add('[XP Monitor]' +user.name + ' has reset the XP of ' + username);
		room.update();
    },
    givexp: function(target, room ,user) {
        if (user.userid !== 'lights' && user.userid !== 'robophill') return false;
        if (!target) return this.sendReply('You must specify a target.');
        let parts = target.split(',');
        if (parts.length !== 2) return this.sendReply('/givexp [userid], [amt]');
        let userid = toId(parts[0]);
        let addXp = toId(parts[1]);
        glevel.updateXp(userid, addXp, room);
    },
    viewxp: function(target, room, user) {
        if (user.userid !== 'lights' && user.userid !== 'robophill') return false;
        if (!target) return this.sendReply('You must specify a target.');
        let userid = toId(target);
        let self = this;
        Prime.database.all('SELECT XP FROM users WHERE userid="'+userid+'"', function(err, results){
            if(err) return //console.log(err);
            let xp;
            if (typeof results[0] === 'undefined') {
                xp = 0;
            } else {
                xp = results[0].XP
            }
            self.sendReply(userid + ' has ' + xp + ' XP');
        });
    },
    dumpxp: function(target, room, user) {
        let self = this;
        let output;
        Prime.database.all('SELECT * FROM users ORDER BY XP desc LIMIT 100', function(err, results){
            output = '<div class="infobox infobox-limited"><center><table border=0><td><center><b>userid</b></center></td><td><center><b>xp</b></center></td><tr>';
            for (let i = 0; i < 100; i++) {
                let userid = results[i].userid;
                let xp = results[i].XP;
                output += '<td><center><b>' + userid + '</b></center></td><td><center><b>' + xp + '</b></center></td><tr>';
            }
            output += '</table></center></div>';
            self.sendReplyBox(output);
        });
    },
    xprewards: function(target, room, user){
        if (!this.runBroadcast()) return;
        let output = '<div class="infobox infobox-limited"><center><font size=4><b>Levelup Rewards</b></font></center><br/>' +
        '<b>Level 1: </b>Chat Filter Immunity<br/>' +
        '<b>Level 3: </b>Customize "About Me" on your Profile<br/>' +
        '<b>Level 5: </b>Unlimited access to `/customsymbol`<br/>' +
        '<b>Level 8: </b>Customize Pokemon team on your Profile<br/>' +
        '<b>Level 10: </b>Customize background color on your Profile. Unlimited access to `/customavatar`<br/>' +
        '<b>Level 12: </b>Customize primary and secondary colors on your Profile<br/>' +
        '<b>Level 13: </b>Unlimited access to `/customcolor`<br/>' +
        '<b>Level 17: </b>Unlimited access to `/icon`<br/>' +
        '<b>Level 20: </b>Unlimited access to `/title`<br/>' +
        '<b>Level 30: </b>You gain 1.3x as many points from tours.<br/>' +
        '<br/>Most levels will also award a free PSGO card pack. Rates of XP gain increase as your level increases. You can view your current XP on your Profile with /profile</div>';
        return this.sendReplyBox(output);
    },
    xpmod: function(target, room, user) {
        if (!this.can('rangeban')) return;
        target = target.split(',');
        let val = target[0];
        if (target[1]) {
            if (target[1].length > 12) return this.sendReply("xpmod reason cannot be longer than 12 characters");
        }
        if (isNaN(target) || target >= 10) return this.sendReply('xpmod must be an integer value below 10.');
        target = Math.floor(target);
        Prime.xpmod = target;
        if (target[1]) Prime.xpmodreason = target[1];
        Rooms('upperstaff').add(user.name + " has set xpmod to " + Prime.xpmod + " with reason: " + Prime.xpmodreason).update();
    },
    xp: function(target, room, user) {
    },
};        