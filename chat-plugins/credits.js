'use strict';

const fs = require('fs');

Prime.readCredits = function (userid, callback) {
	if (!callback) return false;
	userid = toId(userid);
	Prime.database.all("SELECT * FROM users WHERE userid=$userid", {$userid: userid}, function (err, rows) {
		if (err) return console.log("readCredits: " + err);
		callback(((rows[0] && rows[0].credits) ? rows[0].credits : 0));
	});
};
Prime.writeCredits = function (userid, amount, callback) {
	userid = toId(userid);
	Prime.database.all("SELECT * FROM users WHERE userid=$userid", {$userid: userid}, function (err, rows) {
		if (rows.length < 1) {
			Prime.database.run("INSERT INTO users(userid, credits) VALUES ($userid, $amount)", {$userid: userid, $amount: amount}, function (err) {
				if (err) return console.log("writeCredits 1: " + err);
				if (callback) return callback();
			});
		} else {
			amount += rows[0].credits;
			Prime.database.run("UPDATE users SET credits=$amount WHERE userid=$userid", {$amount: amount, $userid: userid}, function (err) {
				if (err) return console.log("writeCredits 2: " + err);
				if (callback) return callback();
			});
		}
	});
};
function logTransaction(message) {
	if (!message) return false;
	fs.appendFile('logs/credit.log', '[' + new Date().toUTCString() + '] ' + message + '\n');
}

exports.commands = {
	creditlog: function (target, room, user) {
		if (room.id !== 'marketplace' && room.id !== 'marketplacestaff') return this.errorReply("Credits can only be given out in the Marketplace.");
		if (!this.can('modlog', null, room)) return false;
		if (!target) return this.sendReply("Usage: /creditlog [number] to view the last x lines OR /creditlog [text] to search for text.");
		let word = false;
		if (isNaN(Number(target))) word = true;
		let lines;
		try {
			lines = fs.readFileSync('logs/credit.log', 'utf8').split('\n').reverse();
		} catch (e) {
			return this.sendReply("The credit log is empty.");
		}
		let output = '';
		let count = 0;
		let regex = new RegExp(target.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "gi");

		if (word) {
			output += 'Displaying last 50 lines containing "' + target + '":\n';
			for (let line in lines) {
				if (count >= 50) break;
				if (!~lines[line].search(regex)) continue;
				output += lines[line] + '\n';
				count++;
			}
		} else {
			if (target > 100) target = 100;
			output = lines.slice(0, (lines.length > target ? target : lines.length));
			output.unshift("Displaying the last " + (lines.length > target ? target : lines.length) + " lines:");
			output = output.join('\n');
		}
		user.popup(output);
	},

	creditatm: function (target, room, user) {
		if (!target) target = user.name;
		if (!this.runBroadcast()) return;
		let userid = toId(target);
		if (userid.length < 1) return this.sendReply("/creditatm - Please specify a user.");
		if (userid.length > 19) return this.sendReply("/creditatm - [user] can't be longer than 19 characters.");

		Prime.readCredits(userid, cred => {
			this.sendReplyBox(Prime.nameColor(target, true) + " has " + cred + ((cred === 1) ? " credits." : " credits."));
			if (this.broadcasting) room.update();
		});
	},

	gcr: 'givecredits',
	givecredits: function (target, room, user) {
		if (room.id !== 'marketplace' && room.id !== 'marketplacestaff') return this.errorReply("Credits can only be given out in the Marketplace.");
		if (!this.can('modlog', null, room)) return false;
		if (!target) return this.sendReply("Usage: /givecredits [user], [amount]");
		let splitTarget = target.split(',');
		if (!splitTarget[1]) return this.sendReply("Usage: /givecredits [user], [amount]");
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/givecredits - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/givecredits - [user] can't be longer than 19 characters");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/givecredits - [amount] must be a number.");
		if (amount > 1000) return this.sendReply("/givecredits - You can't give more than 1000 credits at a time.");
		if (amount < 1) return this.sendReply("/givecredits - You can't give less than one credits.");

		Prime.writeCredits(targetUser, amount);
		this.sendReply(Chat.escapeHTML(targetUser) + " has received " + amount + ((amount === 1) ? " credits." : " credits."));
		logTransaction(user.name + " has given " + amount + ((amount === 1) ? " credit " : " credits ") + " to " + targetUser);
		Rooms.get('marketplace').add('|html|<span style="color:red;">' + (user.name + " has given " + amount + ((amount === 1) ? " credit " : " credits ") + " to " + targetUser + ".</span>"));
		if (Users.get(targetUser) && Users.get(targetUser).connected) {
			Users.get(targetUser).popup("|modal||html|" + Prime.nameColor(user.name, true) + " has given you " + amount + ((amount === 1) ? " credit " : " credits "));
		}
	},

	takecredits: function (target, room, user) {
		if (room.id !== 'marketplace' && room.id !== 'marketplacestaff') return this.errorReply("Credits can only be given out in the Marketplace.");
		if (!this.can('modlog', null, room)) return false;
		if (!target) return this.sendReply("Usage: /takecredits [user], [amount]");
		let splitTarget = target.split(',');
		if (!splitTarget[1]) return this.sendReply("Usage: /takecredits [user], [amount]");
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/takecredits - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/takecredits - [user] can't be longer than 19 characters");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/takecredits - [amount] must be a number.");
		if (amount > 1000) return this.sendReply("/takecredits - You can't take more than 1000 credits at a time.");
		if (amount < 1) return this.sendReply("/takecredits - You can't take less than one credit.");

		Prime.writeCredits(targetUser, -amount);
		this.sendReply("You removed " + amount + ((amount === 1) ? " credit " : " credits ") + " from " + Chat.escapeHTML(targetUser));
		logTransaction(user.name + " has taken " + amount + ((amount === 1) ? " credit " : " credits ") + " from " + targetUser);
		Rooms.get('marketplace').add('|html|<span style="color: red;">' + user.name + " has taken " + amount + ((amount === 1) ? " credit " : " credits ") + " from " + targetUser + ".</span>");
		if (Users.get(targetUser) && Users.get(targetUser).connected) {
			Users.get(targetUser).popup("|modal||html|" + Prime.nameColor(user.name, true) + " has taken " + amount + ((amount === 1) ? " credit " : " credits from you."));
		}
	},

	tcr: 'transfercredits',
	transfercredits: function (target, room, user) {
		if (!target) return this.sendReply("Usage: /transfercredits [user], [amount]");
		let splitTarget = target.split(',');
		for (let u in splitTarget) splitTarget[u] = splitTarget[u].trim();
		if (!splitTarget[1]) return this.sendReply("Usage: /transfercredits [user], [amount]");

		let targetUser = splitTarget[0];
		if (toId(targetUser).length < 1) return this.sendReply("/transfercredits - [user] may not be blank.");
		if (toId(targetUser).length > 19) return this.sendReply("/transfercredits - [user] can't be longer than 19 characters.");

		let amount = Math.round(Number(splitTarget[1]));
		if (isNaN(amount)) return this.sendReply("/transfercredits - [amount] must be a number.");
		if (amount > 1000) return this.sendReply("/transfercredits - You can't transfer more than 1000 credits at a time.");
		if (amount < 1) return this.sendReply("/transfercredits - You can't transfer less than one credit.");

		Prime.readCredits(user.userid, cred => {
			if (cred < amount) return this.sendReply("/transfercredits - You can't transfer more credits than you have.");
			Prime.writeCredits(user.userid, -amount, () => {
				Prime.writeCredits(targetUser, amount, () => {
					this.sendReply("You've sent " + amount + ((amount === 1) ? " credit " : " credits ") + " to " + targetUser);
					logTransaction(user.name + " has transfered " + amount + ((amount === 1) ? " credit " : " credits ") + " to " + targetUser);
					if (Users.getExact(targetUser) && Users.getExact(targetUser)) Users.getExact(targetUser).popup("|modal||html|" + Prime.nameColor(user.name, true) + " has sent you " + amount + ((amount === 1) ? " credit." : " credits."));
				});
			});
		});
	},

	credits: function (target, room, user) {
		if (!this.runBroadcast()) return;

		Prime.database.all("SELECT SUM(credits) FROM users;", (err, rows) => {
			if (err) return console.log("credits1: " + err);
			let totalCredits = rows[0]['SUM(credits)'];
			Prime.database.all("SELECT userid, SUM(credits) AS total FROM users GROUP BY credits HAVING TOTAL > 0;", (err, rows) => {
				if (err) return console.log("credits2: " + err);
				let userCount = rows.length;
				Prime.database.all("SELECT * FROM users ORDER BY credits DESC LIMIT 1;", (err, rows) => {
					if (err) return console.log("credits3: " + err);
					let richestUser = rows[0].userid;
					let richestUserCred = rows[0].credits;
					if (Users.getExact(richestUser)) richestUser = Users.getExact(richestUser).name;
					Prime.database.all("SELECT AVG(credits) FROM users WHERE credits > 0;", (err, rows) => {
						if (err) return console.log("credits4: " + err);
						let averageCredits = rows[0]['AVG(credits)'];

						this.sendReplyBox("The richest user is currently <b><font color=#24678d>" + Chat.escapeHTML(richestUser) + "</font></b> with <b><font color=#24678d>" +
							richestUserCred + "</font></b> credits.</font></b><br />There is a total of <b><font color=#24678d>" +
							userCount + "</font></b> users with at least one credits.<br /> The average user has " +
							"<b><font color=#24678d>" + Math.round(averageCredits) + "</font></b> credits.<br /> There is a total of <b><font color=#24678d>" +
							totalCredits + "</font></b> credits in the economy."
						);
						room.update();
					});
				});
			});
		});
	},

	luckiestusers: function (target, room, user) {
		if (!target) target = 10;
		target = Number(target);
		if (isNaN(target)) target = 10;
		if (!this.runBroadcast()) return;
		if (this.broadcasting && target > 10) target = 10; // limit to 10 while broadcasting
		if (target > 500) target = 500;

		let self = this;

		function showResults(rows) {
			let output = '<table border="1" cellspacing ="0" cellpadding="3"><tr><th>Rank</th><th>Name</th><th>Credits</th></tr>';
			let count = 1;
			for (let u in rows) {
				if (!rows[u].credits || rows[u].credits < 1) continue;
				let username;
				if (rows[u].name !== null) {
					username = rows[u].name;
				} else {
					username = rows[u].userid;
				}
				output += '<tr><td>' + count + '</td><td>' + Chat.escapeHTML(username) + '</td><td>' + rows[u].credits + '</td></tr>';
				count++;
			}
			self.sendReplyBox(output);
			room.update();
		}

		Prime.database.all("SELECT userid, credits, name FROM users ORDER BY credits DESC LIMIT $target;", {$target: target}, function (err, rows) {
			if (err) return console.log("richestuser: " + err);
			showResults(rows);
		});
	},

};