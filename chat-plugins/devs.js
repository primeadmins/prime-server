var fs = require('fs');

function loadDevs() {
        try {
                Users.devs = JSON.parse(fs.readFileSync('config/devs.json', 'utf8'));
        } catch (e) {
                Users.devs = {};
        }
}
if (!Users.devs) loadDevs();

function saveDevs() {
        fs.writeFileSync('config/devs.json', JSON.stringify(Users.devs));
}

exports.commands = {
        givedev: function (target, room, user) {
                if (!this.can('hotpatch')) return false;
                if (!target) return this.sendReply("/givedev user");
                if (Users.devs[toId(target)]) return this.sendReply(target + " is already set as a dev.");
                var targetUser = Users(target);

                if (!targetUser) return this.sendReply("User \"" + target + "\" not found.");
                if (!targetUser.connected) return this.sendReply(targetUser.name + " is not online.");
                if (!targetUser.registered) return this.sendReply(targetUser.name + " is not registered.");

                Users.devs[targetUser.userid] = 1;
                targetUser.popup("You have recieved Developer status from " + user.name);
                this.privateModCommand(user.name + " gave " + targetUser.name + " Dev status. This message should almost never appear so Lights is going to write something silly. Penis!");

                saveDevs();
        },

        removedev: function (target, room, user) {
                if (!this.can('hotpatch')) return false;
                if (!target) return this.sendReply("i dont feel like writing an error message just specify a user");
                if (!Users.devs[toID(target)]) return this.sendReply("hes not a dev, bro");

                delete Users.devs[toId(target)];
                saveDevs();
                this.privateModCommand(target + " lost dev status.");
        },
};
