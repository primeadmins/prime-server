/*'use strict'

let lvl = module.exports = getLvl;

let lvlrooms = lvl.rooms = Object.create(null);

//Possible lvl status states: Spectator, Awaiting Challenge, Challenging, Awaiting Challenge Confirmation, Challenged, In Battle, Winner, Loser

let lvlBuilder = (() => {
	function lvlBuilder() {
		//League v League Phases
		this.pendingLvl = false;
		this.isActive = false;
		this.isSetup = false;
		this.isRosterSetup = false;
		this.isBuildTour = false;
		this.isBattle = false;
		//League v League Settings
		this.teamCount = 5;
		this.isPrivate = true;
		this.format = 'monotype';
		this.speed = 'fast';
		//Challenging League
		this.L1Captain = '';
		this.L1Name = '';
		this.L1Roster = [];
		this.L1Ready = false;
		this.L1StandIn = false;
		//Challenged League
		this.L2Captain = '';
		this.L2Name = '';
		this.L2Roster = [];
		this.L2Ready = false;
		this.L2StandIn = false;
		//Battle Phase
		this.bracket = {};
		//Background Settings for Admins
		this.allowRenames = false;
	};
	lvlBuilder.prototype.makeLvlRoom = function(leagueName, targetLeagueName, room, callback) {
		let id = (leagueName + ' vs ' + targetLeagueName);
		if (Rooms.search(id)) return this.errorReply("This League v League is already in progress.");
		if (!Rooms.global.addChatRoom(id)) return this.errorReply("An error occurred while trying to create the room.");
		let targetRoom = Rooms.search(id);
		targetRoom.isPrivate = true;
		targetRoom.chatRoomData.isPrivate = true;
		Rooms.global.writeChatRoomData();
		return callback (targetRoom);
	};
	lvlBuilder.prototype.captainAutoJoin = function(captains, targetRoom){
		let c1 = captains[0];
		let c2 = captains[1];
		this.L1Roster.push(c1);
		this.L2Roster.push(c2);
		c1.joinRoom(targetRoom);
		c2.joinRoom(targetRoom);
	};
	lvlBuilder.prototype.startSetup = function(targetRoom) {
		targetRoom.add('|html|<center><font size=4><b>League v Leauge Setup</b></font></center><br/>' +
		'<center>League v League Size: ' + this.TeamCount + ' vs ' + this.TeamCount + '</center><br/>' +
		'<center>' + ((this.isPrivate === true) ? 'Private League v League' : 'Public League v League') + '</center><br/>' +
		'<center>Use /lvlsize [3-9] and /lvlvisibility [public/private/announced*] *can only be set by a Leader or Admin</center><br/>' +
		'<center>When both leagues agree on the settings, use /ready. To cancel the League v League, type /cancelmatch.</center>');
	};
	lvlBuilder.prototype.startRosterSelection = function(targetRoom) {
		targetRoom.add('|html|<center><font size=4><b>League v Leauge Roster Selection</b></font></center><br/>' +
		'<center>Slots left: ' + this.L1Name + ' ' + (this.teamCount - this.L1Roster.length) + ' | ' + this.L2Name + ' ' + (this.teamCount - this.L2Roster.length) + '</center><br/>' +
		'<center>Use /addroster [username] to add someone from your League. You can remove someone by using /removeroster [username] or use a stand-in with /standin [username] (Max 1)</center><br/>' +
		'<center>When both leagues have finalized their rosters, use /ready. To cancel the League v League, type /cancelmatch.</center>');
	};
	lvlBuilder.prototype.buildBrackets = function(callback) {
		let L1Roster = this.L1Roster;
		let L2Roster = this.L2Roster;
		let L1Name = this.L1Name;
		let L2Name = this.L2Name;
		let L1Captain = this.L1Captain;
		let L2Captain = this.L2Captain;
		//getting table names of each league
		let L1LeagueTable = false;
		league.getThisLeague(L1Captain, L1Name, L1LeagueTable => {
			league.getThisLeague(L2Captain, L2Name, L2LeagueTable => {
				//getting league member info of League 1's Roster
				let L1RosterData = [];
				for (let x = 0; x < this.teamCount; x++) {
					let L1Player = {};
					L1Player.name = this.L1Roster[x];
					let userType;
					if (L1Player.name !== this.L1StandIn) {
						Prime.leaguedatabase.all("SELECT Type FROM '"+L1LeagueTable+"' WHERE UserId='"+L1Player.name+"'", function (err, results) {
							userType = results[0].Type;
							return userType;
						});
					} else {
						userType = 0;
					}
					L1Player.type = userType;
					L1RosterData.push(L1Player);
				}
				//getting League member info of League 2's Roster
				let L2RosterData = [];
				for (let x = 0; x < this.teamCount; x++) {
					let L2Player = {};
					L2Player.name = this.L2Roster[x];
					let userType;
					if (L2Player.name !== this.L2StandIn) {
						Prime.leaguedatabase.all("SELECT Type FROM '"+L2LeagueTable+"' WHERE UserId='"+L2Player.name+"'", function (err, results) {
							userType = results[0].Type;
							return userType;
						});
					} else {
						userType = 0;
					}
					L2Player.type = userType;
					L2RosterData.push(L2Player);
				}
				//building the bracket lineup ordering for each league
				let L1Bracket = L1Roster;
				let L2Bracket = L2Roster;
				for (let y = 0; y < 2; y++) {
					var shiftedUser = L2Bracket.shift();
					L2Bracket.push(shiftedUser);
				}
				for (let z = 0; z < this.teamCount; z++) {
					let thisUserData = {foundData: false};
					let comparedUserData = {foundData: false};
					for (let b = 0; b < L1RosterData.length; b++) {
						if (L1Bracket[z] === L1RosterData[b].name) {
							thisUserData = L1RosterData[b];
						}
					}
					for (let c = 0; c < L2RosterData.length; c++) {
						if (L2Bracket[z] === L2RosterData[c].name) {
							comparedUserData = L2RosterData[c];
						}
					}
					if (L1Bracket[z].type !== 0 && L2Bracket[z].type !== 0) { 
						let effectiveness = Tools.getEffectiveness(thisUserData.type, comparedUserData.type);
						if (effectiveness !== 0) {
							effectiveness = Tools.getEffectiveness(comparedUserData.type, thisUserData.type);
							if (effectiveness !== 0) {
								let tryNext = z;
								let originalUserData = comparedUserData;
								do {
									if (tryNext < this.TeamCount) {
										tryNext++;
									} else {
										tryNext = 0;
									}
									for (let d = 0; d < L2RosterData.length; d++) {
										if (L2Bracket[tryNext] === L2RosterData[d].name) {
											comparedUserData = L2RosterData[d];
										}
										if (d === (L2RosterData.length - 1) && comparedUserData.name === originalUserData.name) {
											comparedUserData = false;
										}
									}
									if (comparedUserData) {
										let newEffectiveness = Tools.getEffectiveness(thisUserData.type, comparedUserData.type);
										if (newEffectiveness === 0) {
											newEffectiveness = Tools.getEffectiveness(comparedUserData.type, thisUserData.type);
											if (newEffectiveness === 0) {
												for (let e = 0; e < L1RosterData.length; e++) {
													if (L1Bracket[tryNext] === L1RosterData[e].name) {
														thisUserData = L1RosterData[e];
													}
													if (e === (L1RosterData.length - 1) && thisUserData.name === originalUserData.name) {
														thisUserData = false;
													}
												}
												if (thisUserData) {
													effectiveness = Tools.getEffectiveness(thisUserData.type, comparedUserData.type);
													if (effectiveness === 0) {
														effectiveness = Tools.getEffectiveness(comparedUserData.type, thisUserData.type);
														if (effectiveness === 0) {
															var shiftedUser = L2Bracket[tryNext].shift();
															L2Bracket.push(shiftedUser);
														}
													}
												}
											}
										}
									}
								} while (effectiveness !== 0 || tryNext !== z);
							}
						}
					}
				}
				let brackets = {};
				brackets.L1 = L1Bracket;
				brackets.L2 = L2Bracket;
				return callback(brackets);
			});
		});
	};
	//incomplete
	lvlBuilder.prototype.startBattlePhase = function(brackets, self) {
		this.bracket = brackets;
		let L1Roster = this.bracket.L1;
		let L2Roster = this.bracket.L2;
		for (let i = 0; i < (this.teamCount * 2); i++) {
			let thisUser;
			let matchedUser;
			if (i < this.teamCount) {
				thisUser = Users.get(toId(L1Roster[i]));
				matchedUser =  Users.get(toId(L2Roster[i]));
			} else if (i >= this.teamCount) {
				let j = (i - this.teamCount.length);
				thisUser = Users.get(toId(L2Roster[j]));
				matchedUser =  Users.get(toId(L1Roster[j]));
			} else {
				console.log('Error in startBattlePhase; could not add to user object on i = ' + i);
			}
			thisUser.canLvlBattle = (matchedUser);
			if (i < this.teamCount) {
				thisUser.lvlStatus = 'Challenging';
			} else {
				thisUser.lvlStatus = 'Awaiting Challenge';
			}
		}
	};
	
	//incomplete
	lvlBuilder.prototype.updateLvl = function(user, self) {
		let displayHeight;
		if (this.teamCount < 6) {
			displayHeight = '250px';
		} else if (this.teamCount < 8) {
			displayHeight = '300px';
		} else {
			displayHeight = '350px';
		}
		let displayStyle;
		let showChallengeBox = 0;
		if (user.lvlStatus === 'Spectator') {
			displayStyle = '<div style="position:absolute;left:0;right:0;max-height:'+displayHeight+';line-height:1em;border-bottom:2px white solid;border-top: 2px white solid;text-align: center;background-color:rgba(0, 0, 0, 0.4);">' +
			'<div style="display:flex;justify-content:center;align-items:center;width:98%;height:'+displayHeight+';">';
		} else {
			displayStyle = '';
			showChallengeBox = 1;
		}
		let tourOutput = '';
		
		
		
		
		
		
		
		
		
		
		
		if (user.canLvlBattle) {
			//for (let i in room.users) {
			let matchedUser = user.canLvlBattle;
		}
	},
	lvlBuilder.prototype.challenge = function(user, matchedUser, self) {
		if (!this.isBattle) {
			output.sendReply('The League v League Battle Phase hasn\'t started yet.');
			return;
		}
		if (user.canLvlBattle !== true || matchedUser.canLvlBattle !== true) return this.sendreply('Spectators cannot participate in League v Leagues');
		if (matchedUser.disqualifiedLvl === true) return self.sendReply('This user has been disqualified, you were given an automatic win.');
		let L1Bracket = this.bracket.L1;
		let L2Bracket = this.bracket.L2;
		let userLeague;
		let matchedUserLeague;
		if (L1Bracket.indexOf(user.userid) !== -1 ) {
			userLeague = 'L1';
		} else if (L2Bracket.indexOf(user.userid) !== -1) {
			userLeague = 'L2';
		} else {
			return self.sendReply('You are not on a Roster');
		if (L1Bracket.indexOf(matchedUser.userid) !== -1 ) {
			matchedUserLeague = 'L1';
		} else if (L2Bracket.indexOf(matchedUser.userid) !== -1) {
			matchedUserLeague = 'L2';
		} else {
			return self.sendReply('This user is not on a Roster');
		if (userLeague == matchedUserLeague) {
			return self.sendReply('This user is not on the opposing League\'s Roster.');
		}
		let confirmed1 = false;
		let confirmed2 = false;
		if (userLeague == 'L1'){
			for (let i = 0; i < this.teamCount; i++) {
				if (L1Bracket[i] == user.userid) {
					confirmed1 = true;
				}
			}
			for (let j = 0; j < this.teamCount; j++) {
				if (L2Bracket[j] == matchedUser.userid) {
					confirmed2 = true;
				}
			}
		}
		if (userLeague == 'L2'){
			for (let x = 0; x < this.teamCount; x++) {
				if (L2Bracket[x] == matchedUser.userid) {
					confirmed2 = true;
				}
			}
			for (let y = 0; y < this.teamCount; y++) {
				if (L1Bracket[y] == matchedUser.userid) {
					confirmed1 = true;
				}
			}
		}
		if (confirmed1 !== true || confirmed2 !== true) {
			self.sendReply('This user is not your matched user.');
		} else {
			user.lvlStatus = 'Awaiting Challenge Confirmation';
			matchedUser.lvlStatus = 'Challenged';
			matchedUser.pendingLvlBattle = true;
			this.updateLvl(user, self);
		}
	};
	//pulled from tournaments code
	lvlBuilder.prototype.finishChallenge = function(user, to, output, result) {
		let from = this.players[user.userid];
		if (!result) {
			this.generator.setUserBusy(from, false);
			this.generator.setUserBusy(to, false);

			this.isAvailableMatchesInvalidated = true;
			this.update();
			return;
		}

		let now = Date.now();
		this.pendingChallenges.set(from, {to: to, team: user.team});
		this.pendingChallenges.set(to, {from: from, team: user.team});
		from.sendRoom('|tournament|update|' + JSON.stringify({challenging: to.name}));
		to.sendRoom('|tournament|update|' + JSON.stringify({challenged: from.name}));

		this.isBracketInvalidated = true;
		this.update();
	};
	lvlBuilder.prototype.cancelChallenge = function(user, output) {
		if (!this.isTournamentStarted) {
			output.sendReply('|tournament|error|NotStarted');
			return;
		}

		if (!(user.userid in this.players)) {
			output.sendReply('|tournament|error|UserNotAdded');
			return;
		}

		let player = this.players[user.userid];
		let challenge = this.pendingChallenges.get(player);
		if (!challenge || challenge.from) return;

		this.generator.setUserBusy(player, false);
		this.generator.setUserBusy(challenge.to, false);
		this.pendingChallenges.set(player, null);
		this.pendingChallenges.set(challenge.to, null);
		user.sendTo(this.room, '|tournament|update|{"challenging":null}');
		challenge.to.sendRoom('|tournament|update|{"challenged":null}');

		this.isBracketInvalidated = true;
		this.isAvailableMatchesInvalidated = true;
		this.update();
	};
	lvlBuilder.prototype.acceptChallenge = function(user, output) {
		if (!this.isTournamentStarted) {
			output.sendReply('|tournament|error|NotStarted');
			return;
		}

		if (!(user.userid in this.players)) {
			output.sendReply('|tournament|error|UserNotAdded');
			return;
		}

		let player = this.players[user.userid];
		let challenge = this.pendingChallenges.get(player);
		if (!challenge || !challenge.from) return;

		user.prepBattle(this.format, 'tournament', user).then(result => this.finishAcceptChallenge(user, challenge, result));
	};
	lvlBuilder.prototype.finishAcceptChallenge = function(user, challenge, result) {
		if (!result) return;

		// Prevent battles between offline users from starting
		let from = Users.get(challenge.from.userid);
		if (!from || !from.connected || !user.connected) return;

		// Prevent double accepts and users that have been disqualified while between these two functions
		if (!this.pendingChallenges.get(challenge.from)) return;
		let player = this.players[user.userid];
		if (!this.pendingChallenges.get(player)) return;

		let room = Rooms.global.startBattle(from, user, this.format, challenge.team, user.team, {rated: this.isRated, tour: this});
		if (!room) return;

		this.pendingChallenges.set(challenge.from, null);
		this.pendingChallenges.set(player, null);
		from.sendTo(this.room, '|tournament|update|{"challenging":null}');
		user.sendTo(this.room, '|tournament|update|{"challenged":null}');

		this.inProgressMatches.set(challenge.from, {to: player, room: room});
		this.room.add('|tournament|battlestart|' + from.name + '|' + user.name + '|' + room.id).update();

		this.isBracketInvalidated = true;
		this.runAutoDisqualify(this.room);
		this.update();
	};
	return lvlBuilder;
})();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function currentLvl() {
  lvlBuilder.call(this); 
}
lvl.createLvl = function(leagueName){
	currentLvl.prototype = object.create(lvlBuilder.prototype);
	currentLvl.id = leagueName;
};
function getLvl(roomid, fallback) {
	if (roomid && roomid.id) return roomid;
	if (!roomid) roomid = 'default';
	if (!lvlrooms[roomid] && fallback) {
		return lvlrooms.global;
	}
	return lvlrooms[roomid];
}
lvl.get = getLvl;

lvl.search = function (name, fallback) {
	return getLvl(name) || getLvl(toId(name)) || (fallback ? lvlrooms.global : undefined);
};*/