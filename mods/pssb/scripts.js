'use strict';

exports.BattleScripts = {
	randomPrimeStaffBrosTeam: function (side) {
		let userid = toId(side.name);
		let team = [];
		// Hardcoded sets of the available Pokemon.
		let sets = {
			// Admins.
			'~Lights': {
				species: 'Victini', ability: 'Versatility', item: 'Choice Scarf', gender: 'M',
				moves: ['vcreate', 'uturn', 'boltstrike'],
				baseSignatureMove: 'bunnyhop', signatureMove: "Bunnyhop",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'~Robophill': {
				species: 'Manectric-Mega', ability: 'Grease Lightning', item: 'Life Orb', gender: 'M', shiny: true,
				moves: ['HP Ice', 'Thunderbolt', 'Flamethrower'],
				baseSignatureMove: 'thunderclap', signatureMove: "Thunderclap",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'~HoeenHero': {
				species: 'Ludicolo', ability: 'Swift Swim', item: 'Damp Rock', gender: 'M',
				moves: [['hydropump', 'scald'][this.random(2)], 'gigadrain', 'icebeam'],
				baseSignatureMove: 'Scripting', signatureMove: "Scripting",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'~Showdown Helper': {
				species: 'Celebi', ability: 'Moodyish', item: 'Leftovers',
				moves: ['psystrike', 'gigadrain', 'quiverdance',],
				baseSignatureMove: 'zenosmixtape', signatureMove: "Zeno\'s Mixtape",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			},
			'&Archeum': {
				species: 'Absol', ability: 'banishthelight', item: 'Focus Sash', gender: 'M',
				moves: ['nightslash', 'extremespeed', 'playrough'],
				baseSignatureMove: 'intothenight', signatureMove: "Into The Night",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: ' ',
			},
			'&BuffyVampireSlayer': {
				species: 'Arcanine', ability: 'The Chosen One', item: 'Life Orb', gender: 'F',
				moves: ['extremespeed', 'recover', 'precipiceblades'],
				baseSignatureMove: 'slaydatvamp', signatureMove: "Slay Dat Vamp",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'&RisaReina': {
				species: 'Umbreon', ability: 'Twilight Armor', item: 'Leftovers', gender: 'F', shiny: true,
				moves: ['cosmicpower', 'protect', 'wish'],
				baseSignatureMove: 'bitchslap', signatureMove: "Bitch Slap",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Careful',
			},
			'&Family Man TPSN': {
				species: 'Audino-Mega', ability: 'Projectile Spam', item: 'Leftovers', gender: 'M',
				moves: ['sonicboom', 'toxic', 'wish'],
				baseSignatureMove: 'downback', signatureMove: "Down-Back",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Bold',
			},
			'&Gryph': {
				species: 'Slaking', ability: 'Huge Scrappy Power', item: 'Sticky Barb', gender: 'M',
				moves: ['tackle', 'quickattack', 'fakeout'],
				baseSignatureMove: 'gryphassault', signatureMove: "Gryph Assault",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'*Botimus Prime': {
				species: 'Klang', ability: 'Truant', item: 'Life Orb', gender: 'M',
				moves: ['roaroftime', 'blastburn', 'geomancy'],
				baseSignatureMove: 'frenzyplant', signatureMove: "Frenzy Plant",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'@Flare Blitzle': {
				species: 'zubat', ability: 'Speed and Defense Boost', item: 'Eviolite', gender: 'F',
				moves: ['toxic', 'doubleteam', 'roost'],
				baseSignatureMove: 'batattack', signatureMove: "Bat Attack",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Bold',
			},
			'@ZenDH': {
				species: 'Surskit', ability: 'Vengeful', item: 'Focus Sash', gender: 'F',
				moves: ['bugbuzz', 'icebeam', 'hydropump'],
				baseSignatureMove: 'buggaboo', signatureMove: "Buggaboo",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			},
			'@Dream Eater Gengar': {
				species: 'Gengar-Mega', ability: 'Shadow Tag', item: 'Sitrus Berry', gender: 'M',
				moves: ['focusblast', 'hex', 'sludgewave'],
				baseSignatureMove: "sweetdreams", signatureMove: "Sweet Dreams",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			},
			'@LANDOSTAYS': {
				species: 'Landorus', ability: 'Landostays', item: 'Life orb',
				moves: ['quiverdance', 'sludgewave', 'focusblast',],
				baseSignatureMove: 'landoswrath', signatureMove: "Lando\'s Wrath",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			},
			'@opple': {
				species: 'Froslass', ability: 'Snopple', item: 'Focus Sash', gender: 'F',
				moves: ['icebeam', 'shadowball', 'willowisp'],
				baseSignatureMove: 'destiny\'sgrip', signatureMove: "Destiny's Grip",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			}, 
			'@RogueScizor': {
				species: 'Scizor-Mega', ability: 'Berzerk', item: 'Expert Belt', gender: 'M',
				moves: ['earthquake', 'stoneedge', 'uturn'],
				baseSignatureMove: 'titaniumchidori', signatureMove: "Titanium Chidori",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'@Stellarbuck': {
				species: 'Rufflet', ability: 'Ruffled Feathers', item: 'Eviolite', gender: 'F',
				moves: ['bravebird', 'protect', 'roost'],
				baseSignatureMove: 'clumsylanding', signatureMove: "Clumsy Landing",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'%Apex AMB64': {
				species: 'Dragonite', ability: 'BITCH I\'M A DRAGONITE', item: 'Choice Band', gender: 'M',
				moves: ['ironhead', 'earthquake', 'dragonclaw'],
				baseSignatureMove: 'byefelicia', signatureMove: "Bye Felicia!",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'%Areidis': {
				species: 'Zygarde', ability: 'Unbalanced Balance', item: 'Leftovers',
				moves: ['thousandarrows', 'extremespeed', 'recover'],
				baseSignatureMove: 'rebalancing', signatureMove: "Rebalancing",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'%CB Wolf': {
				species: 'Absol-Mega', ability: 'Battle Hymn', item: 'Life Orb', gender: 'M',
				moves: ['suckerpunch', 'sacredsword', 'fly'],
				baseSignatureMove: 'nightmarepunch', signatureMove: "Nightmare Punch",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'%Kre8noyz': {
				species: 'Starmie', ability: 'Riptide', item: 'Assault Vest',
				moves: ['scald', 'thunder', 'blizzard'],
				baseSignatureMove: 'mysticgem', signatureMove: "Mystic Gem",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Timid',
			},
			'%Zen Chocolate': {
				species: 'Charizard-Mega-Y', ability: 'Dragon Boogey', item: 'Shell Bell', gender: 'M',
				moves: ['fierydance', 'boomburst', 'oblivionwing'],
				baseSignatureMove: 'sacredblast', signatureMove: "Sacred Blast",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'%Astral E4 Groove': {
				species: 'Ampharos-Mega', ability: 'Dance Floor', item: 'Expert Belt', gender: 'M',
				moves: ['zapcannon', 'earthpower',  'focusblast'],
				baseSignatureMove: 'breakdance', signatureMove: "Breakdance",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'%lvludkip': {
				species: 'Infernape', ability: 'Where U At', item: 'Air Balloon', gender: 'M',
				moves: ['highjumpkick', 'vcreate', 'thousandarrows'],
				baseSignatureMove: 'infernkip', signatureMove: "Infernkip",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'%rabinov': {
				species: 'Scyther', ability: 'Dread Armour', item: 'Toxic Orb', gender: 'F',
				moves: ['megahorn', 'boltstrike', 'crabhammer', 'crosschop', 'bravebird', 'headsmash', 'flareblitz', 'roost'],
				baseSignatureMove: 'miasmicstrike', signatureMove: "Miasmic Strike",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'jolly',
			},
			'%zIcarus': {
				species: 'Lugia', ability: 'Mark of Immortality', item: 'Life Orb', gender: 'M',
				moves: ['recover', 'psystrike', 'lightofruin'],
				baseSignatureMove: 'blushingrush', signatureMove: "Blushing Rush",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Relaxed',
			},
			'+Echo Mudkipz': {
				species: 'Mudkip', ability: 'Slick Fins', item: 'Leftovers', gender: 'M', shiny: true,
				moves: ['recover', 'healbell', 'earthquake'],
				baseSignatureMove: 'destructokip', signatureMove: "Destructo Kip",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Impish',
			},
			'+Echo Shayd': {
				species: 'Beedrill-Mega', ability: 'Glass Cannon', item: 'Safety Googles', gender: 'M',
				moves: ['drillrun', 'poisonjab', 'ironhead'],
				baseSignatureMove: 'metalstinger', signatureMove: "Metal Stinger",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'+Inactive': {
				species: 'Ludicolo', ability: 'Swift Swim', item: 'Leftovers', gender: 'M',
				moves: ['raindance', 'surf', 'leechseed'],
				baseSignatureMove: 'inactivity', signatureMove: "Inactivity",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Modest',
			},
			'+Ithi': {
				species: 'Sharpedo-Mega', ability: 'Shark\'s Glory', item: 'Focus Sash', gender: 'M',
				moves: ['protect', 'crunch', 'waterfall'],
				baseSignatureMove: 'predatorofthesea', signatureMove: "Predator of the Sea",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'+Rynite': {
				species: 'Banette-Mega', ability: 'Surprise', item: 'Life Orb', gender: 'M',
				moves: ['shadowclaw', 'drainpunch', 'firepunch'],
				baseSignatureMove: '2spooky', signatureMove: "2Spooky",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'+Astral E4 Maleovex': {
				species: 'Bisharp', ability: 'Vengeance', item: 'Life Orb', gender: 'M',
				moves: ['ironhead', 'knockoff', 'brickbreak'],
				baseSignatureMove: 'suckerdance', signatureMove: "Sucker Dance",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'+AstralDERANDI': {
				species: 'Torterra', ability: 'Nature\'s Bounty', item: 'Leftovers', gender: 'M',
				moves: ['curse', 'woodhammer', 'earthquake'],
				baseSignatureMove: 'actaeonplee', signatureMove: "Actaeon Plee",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Adamant',
			},
			'+CelestialTater': {
				species: 'Armaldo', ability: 'Unburden', item: 'White Herb', gender: 'M',
				moves: ['stoneedge', 'drainpunch', 'xscissor'],
				baseSignatureMove: 'shellbreak', signatureMove: "Shell Break",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
			'+Prism Paul': {
				species: 'Slowbro', ability: 'So Dope', item: 'Leftovers', gender: 'M',
				moves: ['steameruption', 'thunderbolt', 'bugbuzz'],
				baseSignatureMove: 'omegablast', signatureMove: "Omega Blast",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Quiet',
			},
			'+Vsz': {
				species: 'Medicham-Mega', ability: 'The Memist', item: 'Life Orb', gender: 'M', shiny: true,
				moves: ['flareblitz', 'icepunch', 'bulletpunch'],
				baseSignatureMove: 'maximummemery', signatureMove: "Maximum Memery",
				evs: {hp: 252, atk:252, def: 252, spa: 252, spd: 252, spe: 252}, nature: 'Jolly',
			},
		};
		// Generate the team randomly.
		let pool = Tools.shuffle(Object.keys(sets));
		let levels = {'~':99, '&':98, '*':97, '@':97, '%':96, '$':95, '+':95, ' ': 94};
		for (let i = 0; i < 6; i++) {
			if (i === 1) {
				let monIds = pool.slice(0, 6).map(function (p) {
					return toId(p);
				});
				let monName;
				for (let mon in sets) {
					if (toId(mon) === userid) {
						monName = mon;
						break;
					}
				}
				if (monIds.indexOf(userid) === -1 && monName) {
					pool[2] = monName;
				}
			}
			let rank = pool[i].charAt(0);
			let set = sets[pool[i]];
			set.level = levels[rank];
			set.name = pool[i];
			if (!set.ivs) {
				set.ivs = {hp:31, atk:31, def:31, spa:31, spd:31, spe:31};
			} else {
				for (let iv in {hp:31, atk:31, def:31, spa:31, spd:31, spe:31}) {
					set.ivs[iv] = set.ivs[iv] ? set.ivs[iv] : 31;
				}
			}
			// Assuming the hardcoded set evs are all legal. LOLOLOLOLOL
			if (!set.evs) set.evs = {hp:84, atk:84, def:84, spa:84, spd:84, spe:84};
			set.moves = [this.sampleNoReplace(set.moves), this.sampleNoReplace(set.moves), this.sampleNoReplace(set.moves)].concat(set.signatureMove);
			team.push(set);
		}
		// Check for Illusion.
		/*if ((team[5].name === '+Illusio' && team[4].name === '+Kit Kasai') || (team[4].name === '+Illusio' && team[5].name === '+Kit Kasai')) {
			let temp = team[3];
			team[3] = team[5];
			team[5] = temp;
		} else if (team[5].name === '+Illusio' || team[5].name === '+Kit Kasai') {
			let temp = team[4];
			team[4] = team[5];
			team[5] = temp;
		}*/
		return team;
	},
	canMegaEvo: function (pokemon) {
		let altForme = pokemon.baseTemplate.otherFormes && this.getTemplate(pokemon.baseTemplate.otherFormes[0]);
		if (altForme && altForme.isMega && altForme.requiredMove && pokemon.moves.indexOf(toId(altForme.requiredMove)) >= 0) return altForme.species;
		let item = pokemon.getItem('');
		if (item.megaEvolves !== pokemon.baseTemplate.baseSpecies || item.megaStone === pokemon.species) return false;
		return item.megaStone;
	},
	runMegaEvo: function (pokemon) {
		let template = this.getTemplate(pokemon.canMegaEvo);
		let side = pokemon.side;

		// PokÃ©mon affected by Sky Drop cannot mega evolve. Enforce it here for now.
		let foeActive = side.foe.active;
		for (let i = 0; i < foeActive.length; i++) {
			if (foeActive[i].volatiles['skydrop'] && foeActive[i].volatiles['skydrop'].source === pokemon) {
				return false;
			}
		}

		pokemon.formeChange(template);
		pokemon.baseTemplate = template; // mega evolution is permanent
		pokemon.details = template.species + (pokemon.level === 100 ? '' : ', L' + pokemon.level) + (pokemon.gender === '' ? '' : ', ' + pokemon.gender) + (pokemon.set.shiny ? ', shiny' : '');
		this.add('detailschange', pokemon, pokemon.details);
		this.add('-mega', pokemon, template.baseSpecies, template.requiredItem);
		pokemon.setAbility(template.abilities['0']);
		pokemon.baseAbility = pokemon.ability;

		// Limit one mega evolution
		for (let i = 0; i < side.pokemon.length; i++) {
			side.pokemon[i].canMegaEvo = false;
		}
		return true;
	},
};