'use strict';

exports.BattleAbilities = {
   //lights
    "vesatility": {
        isNonstandard: true,
        onUpdate: function (pokemon) {
			if (pokemon.volatiles['attract']) {
				this.add('-activate', pokemon, 'ability: Versatility');
				pokemon.removeVolatile('attract');
				this.add('-end', pokemon, 'move: Attract', '[from] ability: Versatility');
			}
		    if (pokemon.volatiles['taunt']) {
				this.add('-activate', pokemon, 'ability: Versatility');
				pokemon.removeVolatile('taunt');
				// Taunt's volatile already sends the -end message when removed
			}
			if (pokemon.volatiles['embargo']) {
				this.add('-activate', pokemon, 'ability: Versatility');
				pokemon.removeVolatile('embargo');
			}
			if (pokemon.volatiles['disable']) {
				this.add('-activate', pokemon, 'ability: Versatility');
				pokemon.removeVolatile('disable');
			}
		},
		onImmunity: function (type, pokemon) {
			if (type === 'attract') {
				this.add('-immune', pokemon, '[msg]', '[from] ability: Versatility');
				return null;
			}
		},
		onTryHit: function (pokemon, target, move) {
			if (move.id === 'captivate' || move.id === 'taunt') {
				this.add('-immune', pokemon, '[msg]', '[from] ability: Versatility');
				return null;
			if (move && move.id in {attract:1, disable:1, encore:1, healblock:1, taunt:1, torment:1}) {
				this.add('-activate', this.effectData.target, 'ability: Versatility', '[of] ' + target);
				return null;
				let lockedmove = source.getVolatile('lockedmove');
				if (lockedmove) {
					// Outrage counter is reset
					if (source.volatiles['lockedmove'].duration === 2) {
						delete source.volatiles['lockedmove'];
					}
				}
		    	}
			}
		},
		id: "vesatility",
		name: "Versatility",
    },
    //Robophill
    "greaselightning": {
    	isNonstandard: true,
    	onStart: function(pokemon) {
    		this.useMove('electricterrain', pokemon);
    		let allPokemon = this.p1.pokemon.concat(this.p2.pokemon); 
    		for (let i = 0; i < allPokemon.length; i++) {
    			if(allPokemon[i].isActive && allPokemon[i].types.indexOf('Electric') > -1) {
    				this.boost({spa: 1, spe: 1}, allPokemon[i]);
    			}
    		}
    	},
    	id: "greaselightning",
    	name: "Grease Lightning",
    },
    
    //hoeenhero
    //swift swim
    
    //showdownhelper
    "moodyish": {
        isNonstandard: true,
        onResidualOrder: 26,
    	onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			let stats = [];
			let boost = {};
			for (let statPlus in pokemon.boosts) {
				if (pokemon.boosts[statPlus] < 6) {
					stats.push(statPlus);
				}
			}
			let randomStat = stats.length ? stats[this.random(stats.length)] : "";
			if (randomStat) boost[randomStat] = 1;

			this.boost(boost);
		},
		id: "moodyish",
		name: "Moodyish",
    },
    //archeum
    "banishthelight": {
    onModifyDamage: function (damage, source, target, move) {
			if (move.crit) {
				this.debug('Sniper boost');
				return this.chainModify(1.5);
			}
		},
		onModifyMove: function (move, target) {
			if (!target.hp) return;
			if (!move.heal && move.category !== "Status" && move && move.effectType === 'Move' && move.crit) {
				move.drain = [1, 2];
			}
		},
		id: "banishthelight",
		name: "Banish The Light",
	},
    //buffyvampireslayer
	"thechosenone": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.boost({atk: 1, def:1, spe:1, evasion: 1});
		},
		onImmunity: function (type) {
			if (type === 'Water') return false;
		},
		id: "thechosenone",
		name: "The Chosen One",
	},
    //RisaReina
	"twilightarmor": {
		isNonstandard: true,
		onStart: function (pokemon, source) {
			this.boost({atk: 2});
		},
		onCriticalHit: false,
		onTryHitPriority: 1,
		onTryHit: function (target, source, move) {
			if (target === source || move.hasBounced || !move.flags['reflectable']) {
				return;
			}
			let newMove = this.getMoveCopy(move.id);
			newMove.hasBounced = true;
			this.useMove(newMove, target, source);
			return null;
		},
		onAllyTryHitSide: function (target, source, move) {
			if (target.side === source.side || move.hasBounced || !move.flags['reflectable']) {
				return;
			}
			let newMove = this.getMoveCopy(move.id);
			newMove.hasBounced = true;
			this.useMove(newMove, target, source);
			return null;
		},
		effect: {
			duration: 1,
		},
		id: "twilightarmor",
		name: "Twilight Armor",
    },
    //Family Man TSPN
    "projectilespam": {
    	isNonstandard: true,
		onModifyPriority: function (priority, pokemon, target, move) {
			if (move && move.category === 'special') {
				return priority + 1;
			}
		},
		onPrepareHit: function (source, target, move) {
			if (move.id in {iceball: 1, rollout: 1}) return;
			if (move.category !== 'Status' && !move.selfdestruct && !move.multihit && !move.flags['charge'] && !move.spreadHit) {
				move.multihit = 5;
				source.addVolatile('parentalbond');
			}
		},
		onModifyMove: function (pokemon, target, move) {
			if (move === "sonicboom") {
				this.debug('Adding Projectile Spam flinch');
				if (!move.secondaries) move.secondaries = [];
				for (let i = 0; i < move.secondaries.length; i++) {
					if (move.secondaries[i].volatileStatus === 'flinch') return;
				}
				move.secondaries.push({
					chance: 10,
					volatileStatus: 'flinch',
				});
		   }
		},
		id: "projectilespam",
		name: "Projectile Spam",
	},
    //gryph
    "hugescrappypower": {
    	isNonstandard: true,
    	onModifyAtkPriority: 5,
		onModifyAtk: function (atk) {
			return this.chainModify(2);
		},
		onModifyMovePriority: -5,
		onModifyMove: function (move) {
			if (!move.ignoreImmunity) move.ignoreImmunity = {};
			if (move.ignoreImmunity !== true) {
				move.ignoreImmunity['Fighting'] = true;
				move.ignoreImmunity['Normal'] = true;
			}
		},
		id: "hugescrappypower",
		name: "Huge Scrappy Power",
    },
    //leostenbuck

    //flareblitzle
    //This Pokemon's Speed, Defense, and Sp. Def is raised 1 stage at the end of each full turn on the field.
    "speedanddefenseboost": {
    	isNonstandard: true,
    	onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			if (pokemon.activeTurns) {
				this.boost({def: 1, spe: 1});
			}
		},
		id: "speedanddefenseboost",
		name: "Speed and Defense Boost",
    },
   
    //Zen DH
    "vengeful": {
		onAfterDamage: function (damage, target, source, move) {
			if (!target.hp) return;
			if (move && move.effectType === 'Move' && move.typeMod > 0) {
				target.setBoost({spa: 6});
				this.add('-setboost', target, 'spa', 12, '[from] ability: Vengeful');
			}
		},
		id: "vengeful",
		name: "Vengeful"
    },
    //landostays
    "landostays": {
    	isNonstandard: true,
		onStart: function (pokemon, source) {
			this.boost({spa:1, spd: 1, spe: 1});
		},
	},
    //opple
	"snopple": {
		onStart: function (pokemon, source) {
			this.boost({spa: 1, spd: 1});
		},
		id: "snopple",
		name: "Snopple",
	},
	//rougescizor Berzerk - Mold Breaker, Adaptability
	"berzerk": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.add('-ability', pokemon, 'Berzerk');
		},
		stopAttackEvents: true,
		onModifyMove: function (move) {
			move.stab = 2;
		},
		id: "berzerk",
		name: "Berzerk",
	},
    //stellarbuck
    "ruffledfeathers": {
    	isNonstandard: true,
    	onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			if (pokemon.activeTurns) {
				this.boost({spe:1});
			}
		},
		id: "ruffledfeathers",
		name: "Ruffled Feathers",
    },
    //apexamb64
    "bitchimadragonite": {
    	isNonstandard: true,
    	onStart: function (pokemon, source) {
			this.setWeather('deltastream');
			this.useMove('dragondance', pokemon)
		},
		onAnySetWeather: function (target, source, weather) {
			if (this.getWeather().id === 'deltastream' && !(weather.id in {desolateland:1, primordialsea:1, deltastream:1})) return false;
		},
		onEnd: function (pokemon) {
			if (this.weatherData.source !== pokemon) return;
			for (let i = 0; i < this.sides.length; i++) {
				for (let j = 0; j < this.sides[i].active.length; j++) {
					let target = this.sides[i].active[j];
					if (target === pokemon) continue;
					if (target && target.hp && target.hasAbility('bitchimadragonite')) {
						this.weatherData.source = target;
						return;
					}
				}
			}
			this.clearWeather();
		},
		id: "bitchimadragonite",
		name: "BITCH I\'M A DRAGONITE",
    },
	//areidis
	"unbalancedbalance": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.add('-ability', pokemon, 'Unbalanced Balance');
		},
		stopAttackEvents: true,
		onAnyModifyBoost: function (boosts, target) {
			let source = this.effectData.target;
			if (source === target) return;
			if (source === this.activePokemon && target === this.activeTarget) {
				boosts['def'] = 0;
				boosts['spd'] = 0;
				boosts['evasion'] = 0;
			}
			if (target === this.activePokemon && source === this.activeTarget) {
				boosts['atk'] = 0;
				boosts['spa'] = 0;
				boosts['accuracy'] = 0;
			}
		},
		id:"unbalancedbalance",
		name: "Unbalanced Balance",
	},
	//cbwolf
	"battlehymn": {//Battle Hymn: non offensive moves are blocked, raises Atk by two stages for both him and his foe
		isNonstandard: true,
		onStart: function (pokemon) {
			let foeactive = pokemon.side.foe.active;
			let activated = false;
			for (let i = 0; i < foeactive.length; i++) {
				if (!foeactive[i] || !this.isAdjacent(foeactive[i], pokemon)) continue;
				if (!activated) {
					this.add('-ability', pokemon, 'Battle Hymn', 'boost');
					activated = true;
				}
				if (foeactive[i]) {
					this.boost({atk: 2}, foeactive[i], pokemon);
					this.useMove("taunt", pokemon);
					this.boost({atk: 2});
				}
			}
		},
		onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			let foeactive = pokemon.side.foe.active;
			let activated = false;
			for (let i = 0; i < foeactive.length; i++) {
				if (!foeactive[i] || !this.isAdjacent(foeactive[i], pokemon)) continue;
				if (!activated) this.useMove("taunt", pokemon, foeactive[i]);
			}
		},
		id: "battlehymn",
		name: "Battle Hymn",
	},
	//kre8noyz
	"riptide": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.useMove("riptide", pokemon);
		},
		onModifyMovePriority: -2,
		onModifyMove: function (move) {
			if (move.secondaries) {
				this.debug('doubling secondary chance');
				for (let i = 0; i < move.secondaries.length; i++) {
					move.secondaries[i].chance *= 2;
				}
			}
		},
		id: "riptide",
		name: "Riptide",
	},
	//Zen Chocolate
	"dragonboogey": {
		isNonstandard: true,
		onStart: function (source, pokemon) {
			for (let i = 0; i < this.queue.length; i++) {
				if (this.queue[i].choice === 'runPrimal' && this.queue[i].pokemon === source && source.template.speciesid === 'groudon') return;
				if (this.queue[i].choice !== 'runSwitch' && this.queue[i].choice !== 'runPrimal') break;
			}
			this.setWeather('sunnyday');
			this.useMove('quiverdance', source);
		},
		id: "dragonboogey",
		name: "Dragon Boogey",
	},
	//astrale4groove
	"dancefloor": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.useMove("electricterrain", pokemon);
		},
		onAnyAccuracy: function (accuracy, target, source, move) {
			if (move && (source === this.effectData.target || target === this.effectData.target)) {
				return true;
			}
			return accuracy;
		},
		id: "dancefloor",
		name: "Dance Floor",
	},
	//lvludkip
	"whereuat": {
		isNonstandard: true,
    	onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			if (pokemon.activeTurns) {
				this.boost({spe:1});			}
		},
		id: "whereuat",
		name: "whereuat",
	},
	//rabinov
	"dreadarmour": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.boost({def: 2, spd: 2});
		},
		onDamage: function (damage, target, source, effect) {
			if (effect.id === 'psn' || effect.id === 'tox' || effect.id === 'brn' || effect.id === 'par' || effect.id === 'slp' || effect.id === 'frz') {
				this.heal(target.maxhp / 8);
				return false;
			}
		},
		onModifyMove: function (move) {
			if (!move || !move.flags['contact']) return;
			if (!move.secondaries) {
				move.secondaries = [];
			}
			let randSecondaryRoll = this.random(7);
			if (randSecondaryRoll === 0) {
				move.secondaries.push({
					chance: 100,
					status: 'psn',
				});
			} else if (randSecondaryRoll === 1) {
				move.secondaries.push({
					chance: 100,
					status: 'tox',
				});
			} else if (randSecondaryRoll === 2) {
				move.secondaries.push({
					chance: 100,
					status: 'frz',
				});
			} else if (randSecondaryRoll === 3) {
				move.secondaries.push({
					chance: 100,
					status: 'par',
				});
			} else if (randSecondaryRoll === 4) {
				move.secondaries.push({
					chance: 100,
					status: 'brn',
				});
			} else if (randSecondaryRoll === 5) {
				move.secondaries.push({
					chance: 100,
					status: 'slp',
				});
			} else if (randSecondaryRoll === 6) {
				move.secondaries.push({
					chance: 100,
					volatileStatus: 'confusion',
				});
			}
		},
		id: "dreadarmour",
		name: "Dread Armour",
	},
	//zicarus
	"markofimmortality": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.boost({atk: 1});
		},
		onSwitchOut: function (pokemon) {
			pokemon.heal(pokemon.maxhp / 3);
		},
		id: "markofimmortality",
		name: "Mark Of Immortality",
	},
	//echomudkipz
	"slickfins": {//
		isNonstandard: true,
    	onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			if (pokemon.activeTurns) {
				this.boost({def: 1, spe:1});
			}
		},
		id: "slickfins",
		name: "Slick Fins",
	},
	//echoshayd
	"glasscannon": {
		isNonstandard: true,
		onStart: function (pokemon, source) {
			this.boost({atk:2, def: -1, spd: -1, spe: 1});
			this.useMove("magnetrise", pokemon)
		},
		onModifyMove: function (move) {
			move.stab = 2;
		},
		id: "glasscannon",
		name: "Glass Cannon",
	},
	//inactive swiftswim
	
	//ithi
	"sharksglory": {
	isNonstandard: true,
	onStart: function (pokemon, source) {
		this.boost({spe: 1});
	},
	onBasePowerPriority: 8,
		onBasePower: function (basePower, attacker, defender, move) {
			if (move.flags['bite']) {
				return this.chainModify(1.5);
			}
		},
		id: "sharksglory",
		name: "Shark\'s Glory"
	},
	//rynite 
	"surprise": {
		isNonstandard: true,
		onModifyPriority: function (priority, pokemon, target, move) {
			if (move && move.category === 'Physical' && move.category === 'Special' && move.category === 'Status') {
				return priority + 1;
			}
		},
		id: "surprise",
		name: "Surprise",
	},
	//Astral E4 Maleovex
	"vengeance": {
		isNonstandard: true,
		onAfterEachBoost: function (boost, target, source) {
			if (!source || target.side === source.side) {
				return;
			}
			let statsLowered = false;
			for (let i in boost) {
				if (boost[i] < 0) {
					statsLowered = true;
				}
			}
			if (statsLowered) {
				this.boost({atk:1, def: 1, spa: 1, spd: 1, spe: 1});
			}
		},
		id: "vengeance",
		name: "Vengeance",
	},
	//AstralDERANDI
	"naturesbounty": {//natures bounty- restores health 1/6 each turn and +1 speed each turn during sunlight
		isNonstandard: true,
		onResidualOrder: 26,
		onResidualSubOrder: 1,
		onResidual: function (pokemon) {
			if (this.isWeather(['sunnyday', 'desolateland'])) {
				this.boost({spe:1});
			}
			pokemon.heal(pokemon.maxhp / 6);
		},
		id: "naturesbounty",
		name: "Nature\'s Bounty",
	},
	//CelestialTater
	//unburden
	//Prism Paul
	"sodope": {
		isNonstandard: true,
		onPrepareHit: function (source, target, move) {
			let type = move.type;
			if (type && type !== '???' && source.getTypes().join() !== type) {
				if (!source.setType(type)) return;
				this.add('-start', source, 'typechange', type, '[from] Protean');
			}
		},
		onModifyMove: function (move) {
			move.stab = 2;
		},
		onDamage: function (damage, target, source, effect) {
			if (effect.effectType !== 'Move') {
				return false;
			}
		},
		id: "sodope",
		name: "So Dope",
	},
    //vsz
    "thememist": {
    	isNonstandard: true,
    	onModifyAtkPriority: 5,
		onModifyAtk: function (atk) {
			return this.chainModify(2);
		},
		onDamage: function (damage, target, source, effect) {
			if (effect.id === 'recoil' && this.activeMove.id !== 'struggle') return null;
		},
		onModifySpePriority: 5,
		onModifySpe: function (spe) {
			return this.chainModify(1.5);
		},
	    id: "thememist",
	    name: "The Memist",
    },
};