'use strict';

exports.BattleMovedex = {
	//Lights
	"bunnyhop": {
		isNonstandard: true,
		accuracy: 50,
		basePower: 80,
		category: "physical",
		id: "bunnyhop",
		name: "Bunnyhop",
		pp: 10,
		priority: 0,
		flags: {protect: 1, mirror:1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]zenheadbutt');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]zenheadbutt');
		},
		onHit: function (target, source, move) {
			this.add('c|~Lights|hophophophophophophophophophophophophophophop');
		},
		secondary: {
			chance: 100,
			self: {
				boosts: {
					accuracy: 1,
					evasion: 1,
					spe: 1,
				},
			},
		},
		target: "normal",
		type: "Psychic",
	},
	//robophill
	"thunderclap": {//Boomburst animation.
    	isNonstandard: true,
    	accuracy: true,
    	category: "Status",
		id: "thunderclap",
		name: "Thunderclap",
		pp: 20,
		priority: 0,
		flags: {protect: 1, reflectable: 1, mirror: 1},
		status: 'par',
		ignoreImmunity: true,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]boomburst');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]boomburst');
		},
		onHit: function (target, source, move) {
			this.add("c|~Robophill|I seem to have a case of the claps.");
		},
		secondary: {
			self: {
				boosts: {
					spa: 1,
					spe: 1,
					evasion: 1,
				},
			},
		},
		target: "normal",
		type: "Normal",
    },
	//Hoeen Hero
	"scripting": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "scripting",
		name: "Scripting",
		pp: 10,
		flags: {},
		priority: 0,
		weather: 'RainDance',
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]calmmind');
			this.attrLastMove('[anim]geomancy')
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]calmmind');
			this.attrLastMove('[anim]geomancy');
		},
		onHit: function (target, source, move) {
			this.add("raw|>>> let p=p2.pokemon.find(p => p.speciesid==='ludicolo'); battle.boost({spa:1,spe:1},p); battle.setWeather('raindance', p); for(let i in p1.pokemon) if(p1.pokemon[i].isActive) { p1.pokemon[i].setStatus('confusion'); break;}");
		},
		secondary: {
			volatileStatus: 'confusion',
			self: {
				boosts: {
					spa: 1,
					spe: 1,
				}
			}
		},
		target: "normal",
		type: "Water",
	},
	//Showdown Helper
	"zenosmixtape": {
		isNonstandard: true,
		accuracy: true,
		basePower: 20,
		category: "special",
		id: "zenosmixtape",
		name: "Zeno\'s Mixtape",
		pp: 0.625,
		priority: 0,
		flags: {protect: 1, mirror:1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]freezedry');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]freezedry');
		},
		onHit: function (target, source, move) {
			this.add('c|~Showdown Helper|');
		},
		secondary: {
			chance: 100,
			status: 'frz',
		},
		target: "normal",
		type: "Psychic",
	},
	//archeum
	"intothenight": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "intothenight",
		name: "Into The Night",
		pp: 0.625,
		priority: 2,
		flags: {snatch: 1},
		volatileStatus: 'focusenergy',
		effect: {
			onStart: function (pokemon) {
				this.add('-start', pokemon, 'move: Focus Energy');
			},
			onModifyMove: function (move) {
				move.critRatio += 2;
			},
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]focusenergy');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]focusenergy');
		},
		onHit: function (target, source, move) {
			this.add('c|&Archeum|I am the shephard of the lost. Prepare to meet your maker.');
		},
		secondary: {
			chance: 100,
			boosts: {
				atk: 2,
				def: -2,
				spd: -2,
				spe: 2,
			}
		},
		target: "self",
		type: "Dark",
	},
    
    //buffyvampireslayer 
    "slaydatvamp": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 100,
		category: "Physical",
		id: "slaydatvamp",
		name: "Slay Dat Vamp",
		pp: 10,
		priority: 0,
		flags: {protect: 1, mirror:1},
		ignoreImmunity: {'Ground': true},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]earthquake');
			this.attrLastMove('[anim]flamethrower');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]earthquake');
			this.attrLastMove('[anim]flamethrower');
		},
		onHit: function (target, source, move) {
			this.useMove('dragondance', source);
			this.add('c|&Buffy Vampire Slayer|I hope you dont mind me slaying you');
		},
		secondary: {
			chance: 10,
			status: 'brn',
		},
		target: "normal",
		type: "Fire",
	},
    //RisaReina
    "bitchslap": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 80,
		basePowerCallback: function (pokemon, target) {
			if (target.newlySwitched) {
				this.debug('Bitch Slap NOT boosted on a switch');
				return 80;
			}
			if (this.willMove(target)) {
				this.debug('Bitch Slap NOT boosted');
				return 80;
			}
			this.debug('Bitch Slap damage boost');
			return 160;
		},
		category: "Physical",
		id: "bitchslap",
		name: "Bitch Slap",
		pp: 10,
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]payback');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]payback');
		},
		onHit: function (target, source, move) {
			this.add('c|&RisaReina|BITCH IMMA FUCKIN\' WRECK YOU');
		},
		secondary: {
			boosts: {
				atk: -1,
				spa: -1,
			}
		},
		target: "normal",
		type: "Dark",
    },
    //familymantspn
    "downback": {
    	isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "downback",
		name: "Down-Back",
		pp: 10,
		priority: 6,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]protect');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]protect');
		},
		onHit: function (target, source, move) {
			this.add('c|&Family Man TSPN|Just gonna turtle in the corner here.');
		},
		boosts: {
			def: 1,
			spd: 1,
		},
		secondary: false,
		target: "self",
		type: "Normal",
    },
    
    //gryph
    "gryphassault": {
    	isNonstandard: true,
		accuracy: 80,
		basePower: 0,
		category: "Physical",
		id: "gryphassault",
		name: "Gryph Assault",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]outrage');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]outrage');
		},
		onHit: function (target, source, move) {
			this.add('c|&Gryph|LOOOOOOUD NOISES!');
		},
		secondary: {
			chance: 100,
			self: {
				volatileStatus: 'confusion',
			}
		},
		ohko: true,
		target: "normal",
		type: "Normal",
    },
    //Leo Stenbuck
	
    //botimus prime - Truant
    
    
    //Flare Blitzle
    "batattack": {
    	isNonstandard: true,
    	accuracy: 100,
    	basePower: 100,
    	category: "physical",
    	id: "batattack",
    	name: "Bat Attack",
    	pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		drain: [1, 2],
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]quickattack');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]quickattack');
		},
		onHit: function (target, source, move) {
			this.add('c|@Flare Blitzle|bAT ATTACK!!!');
		},
		secondary: false,
		target: "normal",
		type: "Poison",
    },
	//zendh
	"buggaboo": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 95,
		category: "Special",
		id: "buggaboo",
		name: "Buggaboo",
		pp: 10,
		priority: 1,
		onEffectiveness: function (typeMod, type, move) {
			return typeMod + this.getEffectiveness('water', type);
		},
		flags: {protect: 1, mirror: 1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]');
		},
		onHit: function (target, source, move) {
			this.add('c|@Zen DH|rip one of us......');
		},
		secondary: {
			chance: 50,
			onHit: function (target, source, move) {
				let statRoll = this.random(2);
				if (statRoll === 0) {
					self: {
						this.boost({atk: -1, def: -1, spa: -1, spd: -1, spe: -1}, source);
					}
				} else if (statRoll === 1) {
					this.boost({atk: -1, def: -1, spa: -1, spd: -1, spe: -1}, target);
				}
			}
		},
		target: "normal",
		type: "Bug",
	},
    //dreameatergengar
    "sweetdreams": {
    	isNonstandard: true,
		accuracy: 90,
		basePower: 0,
		category: "Status",
		id: "sweetdreams",
		name: "Sweet Dreams",
		pp: 10,
		priority: 0,
		flags: {reflectable: 1, protect: 1, mirror: 1},
		onPrepareHit: function (target, source) {
			this.attrLastMove('[still]');
			this.add('-anim', source, "Dark Void", target);
		},
		onHit: function (target, source) {
			let highcost = false;
			if (target.status !== 'slp') {
				if (!target.trySetStatus("slp", source)) return false;
				highcost = true;
			} else if (target.volatiles["nightmare"] && target.volatiles["sweetdreams"]) {
				return false;
			}
			this.directDamage(this.modify(source.maxhp, (highcost ? 0.5 : 0.25)), source, source);
			target.addVolatile("nightmare");
			target.addVolatile("sweetdreams", source);
		},
		effect: {
			onStart: function (pokemon) {
				if (pokemon.status !== 'slp') {
					return false;
				}
				this.add('-start', pokemon, 'Sweet Dreams');
			},
			onResidualOrder: 8,
			onResidual: function (pokemon) {
				let target = this.effectData.source.side.active[pokemon.volatiles['sweetdreams'].sourcePosition];
				if (!target || target.fainted || target.hp <= 0) {
					return;
				}
				let damage = this.damage(pokemon.maxhp / 8, pokemon, target);
				if (damage) {
					this.heal(damage, target, pokemon);
				}
			},
			onUpdate: function (pokemon) {
				if (pokemon.status !== 'slp') {
					pokemon.removeVolatile('sweetdreams');
					this.add('-end', pokemon, 'Sweet Dreams', '[silent]');
				}
			},
		},
		secondary: {
			chance: 100,
			self: {
				boosts: {
					def: -1, 
					spd: -1
				},
			},
		},
		target: "normal",
		type: "Ghost",
	},
	//landostays 
	"landoswrath": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 110,
		category: "Special",
		id: "landoswrath",
		name: "Lando\'s Wrath",
		pp: 10,
		flags: {protect:1, mirror:1},
		priority: 0,
		ignoreImmunity: {'Ground': true},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]earthpower');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]earthpower');
		},
		onHit: function (target, source, move) {
			this.add('c|@Landostays|You are about to see Jesus');
		},
		secondary: {
			chance: 20,
			boosts: {
				spd: -1,
				spe: -1,
			}
		},
		target: "normal",
		type: "Ground",
	},
	//opple revamping move
	"destinysgrip": {
		isNonstandard: true,
		accuracy: true,
		damage: 999,
		multihit: 2,
		category: "Status",
		id: "destinysgrip",
		isViable: true,
		name: "Destiny\'s Grip",
		pp: 5,
		priority: 0,
		flags: {authentic: 1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]destinybond');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]destinybond');
		},
		onHit: function (target, source, move) {
			this.useMove('spikes', source);
			this.useMove('spikes', source);
			this.useMove('spikes', source);
			this.useMove('stealthrock', source);
			this.useMove('stickyweb', source);
			this.useMove('toxicspikes', source);
			this.useMove('toxicspikes', source);
			this.add('c| Opple|How very Devious...')
		},
		selfdestruct: true,
		secondary: false,
		target: "normal",
		type: "steel",
	},
	//rougescizor 
	"titaniumchidori": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 80,
		category: "Physical",
		id: "titaniumchidori",
		name: "Titanium Chidori",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]bulletpunch');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]bulletpunch');
		},
		onHit: function (target, source, move) {
			this.add('c|@RougeScizor|/me flexes');
		},
		secondary: {
			chance: 100,
			self: {
				boosts: {
					atk: 2,
					spe: 1,
				}
			}
		},
		target: "normal",
		type: "Steel",
	},
	
	//Stellarbuck
	"clumsylanding": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 100,
		category: "Physical",
		id: "clumsylanding",
		name: "Clumsy Landing",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		drain: [1, 2],
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]earthquake');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]earthquake');
		},
		onHit: function (target, source, move) {
			this.add('c|@Stellarbuck|/html Well, that didn\'t work out too great.... <em class="mine"><img src="http://i.imgur.com/9PgUk4M.gif" title="llamanv" height="40" width="40" /></em>');
		},
		secondary: {
			chance: 100,
			volatileStatus: "confusion",
		},
		target: "normal",
		type: "Ground",
	},
	
	//Apex AMb64
	"byefelicia": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		damage: 999,
		multihit: 2,
		id: "byefelicia",
		name: "Bye Felicia!",
		pp: 10,
		flags: {protect: 1},
		priority: 1,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]explosion');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]explosion');
		},
		onHit: function (target, source, move) {
			this.useMove('spikes', source);
			this.useMove('spikes', source);
			this.useMove('spikes', source);
			this.useMove('stealthrock', source);
			this.useMove('stickyweb', source);
			this.useMove('toxicspikes', source);
			this.useMove('toxicspikes', source);
			this.add('c|%Apex AMB64|You must be trippin to think u can beat me')
		},
		selfdestruct: true,
		secondary: false,
		target: "normal",
		type: "Steel",
	},
	//areidis
	"rebalancing": {// Torment
		isNonstandard: true,
		accuracy: 100,
		basePower: 60,
		category: "Physical",
		id: "rebalancing",
		name: "Rebalancing",
		pp: 10,
		priority: 0,
		flags: {protect: 1, mirror: 1},
		multihit: 4,
		typeList: {
			"Fire": "Flamethrower",
			"Grass": "Leaf Storm",
			"Water": "Hydro Pump",
			"Dragon": "Dragon Pulse",
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]bulletseed');
		},
		onTryHit: function () {
			this.attrLastMove('[still]');
		},
		onPrepareHit: function (target, source, move) {
			if (!move.animatedhits) move.animatedhits = 0;
			move.animatedhits++;
			if (move.animatedhits > 4 || target.hp === 0) return;
			let rand = ~~(Object.keys(move.typeList).length * Math.random());
			let type = Object.keys(move.typeList)[rand];
			move.type = type;
			this.add('-anim', source, move.typeList[type], target);
		},
		onHit: function (target, source, move) {
			move.onPrepareHit.call(this, target, source, move);
			delete move.typeList[move.type];
			let targetBoosts = {};

			for (let i in target.boosts) {
				target.boosts[i] = -target.boosts[i];
			}
			target.setBoost(targetBoosts);
			this.add('-invertboost', target, '[from] move: Rebalancing');
			this.add('c|%Areidis|Just because Zygarde is revered as the being of balance does not mean that balance has to be in your favour.  Actually, \'balance\' is another thing subject to perspective like everything else in this world.  While I view Zygarde as a superior Dragon, you probably don\'t.  As you see, perspective.  Now, back to cheesecake and candy.');
		},
		secondary: {
			chance: 100,
			volatileStatus: 'confusion',
			volatileStatus: 'torment',
		},
		target: "normal",
		type: "???",
	},
	//cbwolf
	"nightmarepunch": {
		isNonstandard: true,
		accuracy: 95,
		basePower: 90,
		category: "Physical",
		id: "nightmarepunch",
		name: "Nightmare Punch",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		onEffectiveness: function (typeMod, type) {
			if (type === 'Fairy') return 1;
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]nightslash');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]nightslash');
		},
		onHit: function (target, source) {
			this.add('c|%CB Wolf|Dear fairies, go smell your flowers somewhere else and let the warriors do their job in peace. Well, "peace"...');
		},
		secondary: false,
		target: "normal",
		type: "Dark",
	},
	//kre8noyz
	"mysticgem": {
		isNonstandard: true,
		accuracy: 90,
		basePower: 90,
		category: "Special",
		id: "mysticgem",
		name: "Mystic Gem",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		drain: [1, 3],
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]psychic');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]psychic');
		},
		onHit: function (target, source) {
			if (source.template && source.template.num === 493) return false;
			this.add('-start', source, 'typechange', '[from] move: Mystic Gem', '[of] ' + target);
			source.types = target.getTypes(true);
			source.addedType = target.addedType;
			this.add('c|%Kre8noyz|sasque....TOMA!!!!');
		},
		secondary: {
			chance: 100,
			self: {
				boosts: {
					accuracy: 2,
					atk: -2,
					spa: -2
				}
			}
		},
		target: "normal",
		type: "Psychic",
	},
	//zenchocolate 
	"sacredblast": {
		isNonstandard: true,
		accuracy: 85,
		basePower: 100,
		category: "Special",
		id: "sacredblast",
		name: "Sacred Blast",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]fireblast');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]fireblast');
		},
		onHit: function (target, source, move) {
			this.add('c|%Zen Chocolate|/html </button><em class="mine"><img src="http://i.imgur.com/wp51rIg.png" title="feelsbrn" height="40" width="40" /></em>');
		},
		secondary: {
			chance: 50,
			status: 'brn',
		},
		target: "normal",
		type: "Fire",
	},
	//Astral E4 Groove
	"breakdance": {
		isNonstandard: true,
		accuracy: 80,
		basePower: 100,
		category: "Special",
		id: "breakdance",
		name: "Breakdance",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]quiverdance');
			this.attrLastMove('[anim]seedflare');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]quiverdance');
			this.attrLastMove('[anim]seedflare');
		},
		onHit: function (target, source, move) {
			this.add('c|%Astral E4 Groove|check this out dudes');
		},
		secondary: {
			chance: 100,
			self: {
				boosts: {
					spe: 1,
				}
			}
		},
		target: "normal",
		type: "Dragon"
	},
	//lvludkip 
	"infernkip": {
		isNonstandard: true,
		accuracy: 95,
		basePower: 110,
		category: "Physical",
		id: "infernkip",
		name: "Infernkip",
		pp: 10,
		priority: 0,
		flags: {protect: 1, mirror: 1, defrost: 1},
		thawsTarget: true,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]flareblitz');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]flareblitz');
		},
		onHit: function (target, source, move) {
			this.add('c|%lvludkip|Itadakimasu!');
		},
		secondary: {
			chance: 30,
			status: 'brn',
			self: {
				chance: 100,
				boosts: {
					atk: 1,
				}
				
			}
		},
		target: "normal",
		type: "Water",
	},
	//rabinov
	"miasmicstrike": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 50,
		category: "Physical",
		id: "miasmicstrike",
		name: "Miasmic Strike",
		pp: 10,
		priority: 0,
		flags: {contact: 1, protect: 1, mirror: 1},
		multihit: 3,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]bugbite');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]bugbite');
		},
		onHit: function (target, source, move) {
			this.add('c|+Rabinov|You\'ll never make it out alive!');
			target.clearBoosts();
			this.add('-clearboost', target);
		},
		secondary: {
			chance: 100,
			onHit: function (target, source, move) {
				let statusRoll = this.random(5);
				/*{
					chance: 100,
					volatileStatus: 'confusion',
				}, */
				if (statusRoll === 0) {
					target.trySetStatus('par', source);
				} else if (statusRoll === 1) {
					target.trySetStatus('brn', source);
				} else if (statusRoll === 2) {
					target.trySetStatus('frz', source);
				} else if (statusRoll === 3) {
					target.trySetStatus('slp', source);
				} else if (statusRoll === 4) {
					target.trySetStatus('psn', source);
				}
			},
		},
		target: "normal",
		type: "Bug",
	},
	//rabinov
	"dreadarmour": {
		isNonstandard: true,
		onStart: function (pokemon) {
			this.boost({def: 2, spd: 2});
		},
		onDamage: function (damage, target, source, effect) {
			if (effect.id === 'psn' || effect.id === 'tox' || effect.id === 'brn' || effect.id === 'par' || effect.id === 'slp' || effect.id === 'frz') {
				this.heal(target.maxhp / 8);
				return false;
			}
		},
		onModifyMove: function (move) {
			if (!move || !move.flags['contact']) return;
			if (!move.secondaries) {
				move.secondaries = [];
			}
			let randSecondaryRoll = this.random(7);
			if (randSecondaryRoll === 0) {
				move.secondaries.push({
					chance: 100,
					status: 'psn',
				});
			} else if (randSecondaryRoll === 1) {
				move.secondaries.push({
					chance: 100,
					status: 'tox',
				});
			} else if (randSecondaryRoll === 2) {
				move.secondaries.push({
					chance: 100,
					status: 'frz',
				});
			} else if (randSecondaryRoll === 3) {
				move.secondaries.push({
					chance: 100,
					status: 'par',
				});
			} else if (randSecondaryRoll === 4) {
				move.secondaries.push({
					chance: 100,
					status: 'brn',
				});
			} else if (randSecondaryRoll === 5) {
				move.secondaries.push({
					chance: 100,
					status: 'slp',
				});
			} else if (randSecondaryRoll === 6) {
				move.secondaries.push({
					chance: 100,
					volatileStatus: 'confusion',
				});
			}
		},
		id: "dreadarmour",
		name: "Dread Armour",
	},
	//zIcarus
	"blushingrush": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 120,
		category: "Physical",
		id: "blushingrush",
		name: "Blushing Rush",
		pp: 10,
		priority: 0,
		flags: {contact: 1, protect: 1, mirror: 1},
		recoil: [1, 5],
		self: {
			boosts: {
			atk: 1,
			spa: 1,
			}
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]flareblitz');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]flareblitz');
		},
		onHit: function (target, source, move) {
			this.add('c|%zIcarus|My light is as bright and brief as the setting sun!');
		},
		secondary: {
			chance: 50,
			status: "brn",
		},
		target: "normal",
		type: "Fire",
	},
	//echomudkipz
	"destructokip": {
		isNonstandard: true,
		accuracy: true,
		basePower: 0,
		damage: 'level',
		category: "Status",
		id: "destructokip",
		name: "Destructo Kip",
		pp: 5,
		flags: {protect: 1, reflectable: 1, mirror: 1},
		self: {
			status: 'tox',
			stallingMove: true,
			volatileStatus: 'protect',
			onPrepareHit: function (pokemon) {
				return !!this.willAct() && this.runEvent('StallMove', pokemon);
			},
			onHit: function (pokemon) {
				pokemon.addVolatile('stall');
				this.add('c|+Echo Mudkipz|The opponent shall feel the wrath of the kips army.');
			},
			effect: {
				duration: 1,
				onStart: function (target) {
					this.add('-singleturn', target, 'Destructo Kip');
				},
				onTryHitPriority: 3,
				onTryHit: function (target, source, move) {
					if (!move.flags['protect']) return;
					this.add('-activate', target, 'Destructo Kip');
					let lockedmove = source.getVolatile('lockedmove');
					if (lockedmove) {
						// Outrage counter is reset
						if (source.volatiles['lockedmove'].duration === 2) {
							delete source.volatiles['lockedmove'];
						}
					}
					return null;
				},
			},
		},
		secondary: {
			self: {
				chance: 100,
				boosts: {
					atk: 1,
					spd: 1,
				}
			}
		},
		target: "normal",
		type: "Steel",
	},
	//echo shayd
	"metalstinger": {
		isNonstandard: true,
		accuracy: 99,
		basePower: 50,
		category: "Physical",
		id: "metalstinger",
		name: "Metal Stinger",
		pp: 10,
		priority: 0,
		flags: {contact: 1, protect: 1, mirror: 1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]fellstinger');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]fellstinger');
		},
		onHit: function (target, pokemon) {
			pokemon.addVolatile('fellstinger');
		},
		effect: {
			duration: 1,
			onAfterMoveSecondarySelf: function (pokemon, target, move) {
				if (!target || target.fainted || target.hp <= 0) this.boost({atk:2}, pokemon, pokemon, move);
				pokemon.removeVolatile('fellstinger');
			},
		},
		secondary: false,
		target: "normal",
		type: "Steel",
	},
	//implemented
	
	//inactive
	"inactivity": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "inactivity",
		name: "Inactivity",
		pp: 10,
		priority: 0,
		flags: {snatch: 1},
		boosts: {
			spe: 2,
			evasion: 2,
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]agility');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]agility');
		},
		onHit: function (target, source, move) {
			this.add('c|+inactive|2quick4you');
		},
		secondary: false,
		target: "self",
		type: "Water",
	},
	//Ithi
	"predatorofthesea": {//predator of the sea: evasion and attack raised by 1
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "predatorofthesea",
		name: "Predator of the Sea",
		pp: 10,
		flags: {snatch: 1},
		priority: 0,
		boosts: {
			atk: 1,
			evasion: 1,
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]doubleteam');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]doubleteam');
		},
		onHit: function (target, source, move) {
			this.add('c|+ithi|Checkmate i\'m afraid');
		},
		secondary: false,
		target: "self",
		type: "Water",
	},
	//joshykun
	
	//rynite
	"2spooky": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "2spooky",
		name: "2Spooky",
		pp: 10,
		flags: {snatch: 1},
		priority: 0,
		boosts: {
			atk: 2,
			def: 2,
			spa: 2,
			spd: 2,
			spe: 2,
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]calmmind');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]calmmind');
		},
		onHit: function (target, source, move) {
			this.add('c|+Rynite|I\'M SENDING YOU TO THE GRAVEYARD');
		},
		secondary: false,
		target: "self",
		type: "Ghost",
	},
	//abut
	
	//alankh
	
	//arkter
	
	//Astral E4 Maleovex
	"suckerdance": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 80,
		category: "Physical",
		id: "suckerdance",
		name: "Sucker Dance",
		pp: 5,
		flags: {contact: 1, protect: 1, mirror: 1},
		priority: 1,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]suckerpunch');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]suckerpunch');
		},
		onTry: function (source, target) {
			this.boost({atk: 2}, source);
			let decision = this.willMove(target);
			if (!decision || decision.choice !== 'move' || (decision.move.category === 'Status' && decision.move.id !== 'mefirst') || target.volatiles.mustrecharge) {
				this.add('-fail', source);
				return null;
			}
		},
		secondary: false,
		target: "normal",
		type: "Dark"
	},
	//Astral Looking
	
	//Astral SG
	
	//AstralDERANDI
	"actaeonplee": {
		isNonstandard: true,
		accuracy: true,
		category: "Status",
		id: "actaeonplee",
		name: "Actaeon Plee",
		pp: 10,
		flags: {},
		priority: 0,
		weather: 'sunnyday',
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]synthesis');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]synthesis');
		},
		onHit: function (pokemon) {
			if (this.isWeather(['sunnyday', 'desolateland'])) {
				this.heal(this.modify(pokemon.maxhp, 0.667));
			} else if (this.isWeather(['raindance', 'primordialsea', 'sandstorm', 'hail'])) {
				this.heal(this.modify(pokemon.maxhp, 0.25));
			} else {
				this.heal(this.modify(pokemon.maxhp, 0.5));
			}
			this.add('c|+AstralDERANDI|nature help me out here will ya?')
		},
		secondary: false,
		target: "self",
		type: "Grass",
	},
	//Attribute
	
	//Azazel9000
	
	//CelestialTater
	"shellbreak": {
		isNonstandard: true,
		accuracy: true,
		basePower: 0,
		category: "Status",
		id: "shellbreak",
		name: "Shell Break",
		pp: 10,
		priority: 1,
		flags: {snatch: 1},
		boosts: {
			def: -1,
			spd: -1,
			atk: 2,
			spa: 2,
			spe: 2,
		},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]shellsmash');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]shellsmash');
		},
		onHit: function (target, source, move) {
			this.add('c|+CelestialTater|MUAHAHAHAHAA?');
		},
		secondary: false,
		target: "self",
		type: "Normal",
	},
	//Mighty Sciz
	
	//NidoKyng
	
	//Olympus Poke
	
	//Prism Paul
	"omegablast": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 80,
		category: "Special",
		pp: 10,
		priority: 0,
		flags: {protect: 1, mirror: 1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]psychoboost');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]psychoboost');
		},
		onHit: function (target, source, move) {
			let stats = [];
			for (let stat in target.boosts) {
				if (target.boosts[stat] < 6) {
					stats.push(stat);
				}
			}
			if (stats.length) {
				let randomStat = stats[this.random(stats.length)];
				let boost = {};
				boost[randomStat] = 1;
				this.boost(boost, source);
				this.add('c|@Paul Century|Dope');
			} else {
				return false;
			}
		},
		secondary: false,
		target: "normal",
		type: "Psychic",
	},
	//Tylacto
	
	//Admin Drew
	
	//Vsz
	"maximummemery": {
		isNonstandard: true,
		accuracy: 100,
		basePower: 140,
		category: "Physical",
		id: "maximummemery",
		name: "Maximum Memery",
		pp: 10,
		flags: {protect: 1, mirror: 1},
		drain: [1, 2],
		onEffectiveness: function (typeMod, type, move) {
			return typeMod + this.getEffectiveness('psychic', type);
		},
		priority: 0,
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]meteormash');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]meteormash');
		},
		onHit: function (target, source, move) {
			this.add('c| Nappa|VEGETA WHAT DOES THE SCOUTER SAY ABOUT HIS POWER LEVEL?');
			this.add('c| Vegeta|IT\'S OVER 9000!');
			this.add('c|+Vsz|/html </button><em class="mine"><img src="http://i.imgur.com/rgIivM2.gif" title="datboi" height="40" width="40" /></em> OH SHIT! WADDUP!');
		},
		secondary: {
			chance: 50,
			self: {
				boosts: {
					def: 1,
					spd: 1,
				}
			}
		},
		target: "normal",
		type: "Fighting",
	},
	//part of kre8noyz's ability
	"riptide":{
		isNonstandard: true,
		accuracy: true,
		basePower: 0,
		category: "Special",
		id: "riptide",
		name: "Riptide",
		pp: 10,
		priority: 0,
		flags: {protect: 1, reflectable: 1, mirror: 1, authentic: 1},
		onTryHit: function (target, source, move) {
			this.attrLastMove('[anim]defog');
		},
		onMoveFail: function (target, source, move) {
			this.attrLastMove('[anim]defog');
		},
		self: {
			onHit: function (pokemon) {
				if (pokemon.hp && pokemon.removeVolatile('leechseed')) {
					this.add('-end', pokemon, 'Leech Seed', '[from] move: Rapid Spin', '[of] ' + pokemon);
				}
				let sideConditions = {spikes:1, toxicspikes:1, stealthrock:1, stickyweb:1};
				for (let i in sideConditions) {
					if (pokemon.hp && pokemon.side.removeSideCondition(i)) {
						this.add('-sideend', pokemon.side, this.getEffect(i).name, '[from] move: Rapid Spin', '[of] ' + pokemon);
					}
				}
				if (pokemon.hp && pokemon.volatiles['partiallytrapped']) {
					pokemon.removeVolatile('partiallytrapped');
				}
			},
		},
		onHit: function (target, pokemon) {
			if (pokemon.status && !target.status && target.trySetStatus(pokemon.status)) {
				pokemon.cureStatus();
			} else {
				return false;
			}
			this.add('c| Pssb|This Move is for removing hazards and transfering statuses(This message is here to alert you about what this move\'s purpose is so you aren\'t confused)')
		},
		secondary: false,
		target: "normal",
		type: "Steel",
	},
};