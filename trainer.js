'use strict'

//For Lights' future project. Most of it will be module based, however this file will load the entire system and handle the loading of user teams for battles

let Trainer = module.exports;

Trainer.commands = require('./trainer/commands.js');

//load Area info

let areaData;
try {
    areaData = JSON.parse(fs.readFileSync('./trainer/data/areaData.json', 'utf8'));
} catch (e) {
    areaData = false;
}

Trainer.areas = {};
let area;
let pokeareas;
let pokes = Object.getOwnPropertyNames(areaData);
for (let i = 0; i < areaData.length; i++) {
    pokeareas = Object.getOwnPropertyNames(areaData[i]);
    if (pokeareas[0] === 'event') continue;
    for (let x in pokeareas) {
        area = pokeareas[x]
        Trainer.areas[area][pokes[i]] = {
            level: areaData[i][area].level,
            rarity: areaData[i][area].rarity
        };
    }
}

class teams {
    constructor() {
    
    }
}

Trainer.teams = new teams();

Trainer.loadTeam = function (userid, callback) {
    let team = [];
    let userTeam;
    let slot;
    loadPoke(userid, 0);
    
    function loadPoke(userid, count) {
        slot = 'p' + count;
        if (count > 6) {
            userTeam = Trainer.teams[userid] = team;
            return callback(true);
        }
        Prime.database.all("SELECT * FROM trainer WHERE userid=$userid AND slot=$slot", {$userid: userid, $slot: slot}, function (err, results) {
            if (err) console.log(err);
            if (!results || !results[0]) {
                if (team.length > 0) {
                    userTeam = Trainer.teams[userid] = team;
                    return callback(true);
                } else return callback(false);
            } else {
                team.push(results[0]);
                loadPoke(userid, count++);
            }
        });
    }
},