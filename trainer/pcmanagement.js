'use strict'

//This file will be used for handling the PC management in the Trainer Project

Trainer.loadPC = function(userid, callback) {
	Prime.database.all("SELECT * FROM trainer WHERE slot LIKE 's%' AND userid=$userid", {$userid: userid}, function(err, results) {
		if (err) {
			console.log(err);
		} else if (!results || !results[0]) {
			return callback(false);
		} else return callback(results);
	});
};