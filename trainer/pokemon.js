'use strict'

//This file will be used for editing and adjusting pokemon in the Trainer Project

//This includes XP, evolution, levelling up, teaching new moves, and adjusting EVs with items

function getAbility(poke) {
	let data = Tools.getTemplate(poke);
	data = data.abilities;
    let arr = Object.getOwnPropertyNames(data);
    let ab = [];
    for (let i in arr) {
        let name = arr[i];
        ab.push(data[arr]);
    }
    let rand = Math.floor(Math.random() * ab.length);
    poke = ab[rand];
    return poke;
}

function getMoves(poke, level) {
	let data = Tools.getTemplate(poke);
    data = data.learnset;
    let potentialMoves = [];
    let selectedMoves = [];
    let arr = Object.getOwnPropertyNames(data);
    for (let i in arr) {
        let move = arr[i];
        let learned = data[arr];
        for (let x in learned) {
            let gen = learned[x].charAt(0);
			//update for gen7 soon
            if (gen === 6) {
                let learnType = learned[x].charAt(1);
                if (learnType === 'L') {
                    let reqLvl = learned[x].slice(2);
                    reqLvl = parseInt(reqLvl);
                    if (level >= reqLvl) potentialMoves.push(move);
                }
            }
        }
    }
    let attempts = 0;
    do {
        let rand = Math.floor(Math.random() * (+potentialMoves.length - +selectedMoves.length));
        let move = potentialMoves[rand];
        if (selectedMoves.indexOf(move) === -1) selectedMoves.push(move);
        attempts++;
    } while (selectedMoves.length < 4 && attempts < 30);
    return selectedMoves;
}

Trainer.genPoke = function(area, wild) {
	let pokes = Trainer.areas[area];
	let pokeids = Object.getOwnPropertyNames(Trainer.areas[area]);
	let pokemon;
	let common = [];
	let uncommon = [];
	let rare = [];
	let ultrarare = [];
	for (let i = 0; i < pokes.length; i++) {
		pokemon = {
			id: pokeids[i],
			level: pokes[i].level,
			rarity: pokes[i].rarity
		};
		switch(pokemon.rarity) {
			case 'common':
				common.push(pokemon);
				break;
			case 'uncommon':
				uncommon.push(pokemon);
				break;
			case 'rare':
				rare.push(pokemon);
				break;
			case 'ultrarare':
				ultrarare.push(pokemon);
				break;
		}
	}
	let rarityArr = [common, uncommon, rare, ultrarare];
	let rand = Math.floor(Math.random() * 100);
	let rarity;
	if (rand <= 60) {
		rarity = 0;
	} else if (rand <= 85) {
		rarity = 1;
	} else if (rand <= 95) {
		rarity = 2;
	} else {
		rarity = 3;
	}
	if (rarityArr[rarity].length !== 0) {
		rarity = rarityArr[rarity];
	} else rarity = rarityArr[0];
	let length = rarity.length;
	rand = Math.floor(Math.random() * length);
	pokemon = rarity[rand];
//get level
	let level = pokemon.level;
	rand = Math.floor(Math.random() * 2);
	if (rand < 1) {
		rand = Math.floor(Math.random() * 3.5);
	} else rand = math.floor(Math.random() * (3.5 * -1));
	level = Math.floor(+level + +rand);
//get item
	let item = false;
	rand = Math.floor(Math.random() * 1000);
	if (rand > 998) item = 'luckyegg';
//get shiny
	let shiny = false;
	rand = Math.floor(Math.random() * 8192);
	if (rand === 666) shiny = true;
//get ivs
	let ivArr = [];
	for (let x = 0; x < 6; x++) {
		let iv = Math.floor(Math.random() * 31);
		ivArr.push(iv);
	}
//get nature
	let nature;
	let natureArr = ['Hardy', 'Lonely', 'Brave', 'Adamant', 'Naughty', 'Bold', 'Docile', 'Relaxed', 'Impish', 'Lax', 'Timid', 'Hasty', 'Serious', 'Jolly', 'Naive', 'Modest', 'Mild', 'Quiet', 'Bashful', 'Rash', 'Calm', 'Gentle', 'Sassy', 'Careful', 'Quirky'];
	rand = Math.floor(Math.random() * natureArr.length);
	nature = natureArr[rand];
//get ability
	let ability = getAbility(pokemon.id);
//get moves
	let moves = getMoves(pokemon.id, level);
//get gender
	let gender = getGender(pokemon.id);
//build pokemon object
	pokemon = {
		name: pokemon.name,
		id: pokemon.id,
		nickname: false,
		level: level,
		nature: nature,
		ability: ability,
		gender: gender,
		hpev: 0,
		atkev: 0,
		defev: 0,
		spaev: 0,
		spdev: 0,
		speev: 0,
		hpiv: ivArr[0],
		atkiv: ivArr[1],
		defiv: ivArr[2],
		spaiv: ivArr[3],
		spdiv: ivArr[4],
		speiv: ivArr[5],
		item: item,
		happy: 0,
		shiny: shiny,
		move1: move[0],
		move2: move[1],
		move3: move[2],
		move4: move[3],
		found: ((wild) ? area : area + ' Collection')
	};
	return pokemon;
};
	
	
	